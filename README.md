# diffTrace

This project is for instrumenting and collecting function call traces from any executable (using __ParLOT__ and other __Pintools__) and subsequently, do analysis on the collected traces:
- Decompression
- Filter
- Concept Lattice and pair-wise Jaccard Similarity Matrix
- Minimum Repetetive Representation (MRR)
- Visualizng diffMRR of any pair of traces



## CONTENTS
- [Pintools](#pintools)
- [Collect Traces with ParLOT](#parlot)
	* [Activate Parlot](#activate-parlot)
	* [Collect Traces](#collect-traces)
		+ [Single Application](#single-application)
			- [Decompression](#decompression)
			- [Trace Text View](#trace-text-view)
		+ [Benchmarking](#benchmarking)
- [Concept Lattice](#concept-lattice)
	* [Attributes](#attributes)
- [diffMRR](diffMRR)


## Pintools
Any tool that is built on top of [Pin](https://software.intel.com/en-us/articles/pin-a-dynamic-binary-instrumentation-tool) is called __Pintool__. There are a bunch of examples that tell us how we can use [Pin API](https://software.intel.com/sites/landingpage/pintool/docs/97619/Pin/html/) to instrument binaries at different granularites and get insight about the target code.
To find built-in Pintools,
```
cd parlot/source/tools/
```
and you can find all different examples. Within each folder, there is a `makefile`.

## ParLOT
ParLOT is an efficient tracing tool on top of Pin that collects all function call traces from main image or all images (including library function calls).

### Activate ParLOT

In order to collect traces from running any executable, you need to run command `source activate-parlot.sh <path-to-diffTrace>` from diffTrace folder:

`source activate-parlot.sh $HOME/diffTrace`

This script will make ParLoT shared objects and set required paths.

### Collect Traces
You can collect function call traces from either a [single executable](#single-application) or [benchmark of applications](#benchmarking).

#### Single Application

For collecting function call traces from the main image:

`pin –t $M_MUTATOR_PATH/DBGpin17mainimage.so -- <executable>` 

(e.g., `pin –t $M_MUTATOR_PATH/DBGpin17mainimage.so -- ./a.out`)	
(e.g., `mpirun -np 8 pin –t $M_MUTATOR_PATH/DBGpin17mainimage.so -- ./helloWorld`)

for collecting function call traces from all images (including libraries), just replace `$M_MUTATOR_PATH/DBGpin17mainimage.so` with `$A_MUTATOR_PATH/DBGpin17allimages.so`

This will generate a trace file for each process/thread. If you run a program on a single core with single process/thread, pin would generate a pair of .info and .0 files.

##### Decompression
For decompressing and reading individual traces, each trace(s) and the info file would be decompressed into a format called __texTrace__. This intermediate representation of traces helps with protyping ideas w.r.t. trace filteration and analysis. Later, filteration of trace entries would take place either before trace collection (i.e. modification of ParLoT with white/blacklist) or at decompression phase.
In order to decompress a set of trace files:

```
cd decompress2text
make
``` 

Lets say you put all the compressed traces of a single execution within a folder `<somewhere>/experiment1/trace`. Then just run:
`python trace2text.py <somewhere>/experiment1`
and it will generate and store the texTrace files in `<somewhere>/experiment1/texTrace`.

##### Trace Text View
To read a single trace in ASCII text format:
`python scripts/viewTrace.py <path-to-texTrace>/sample.0.txt`

#### Benchmarking
If you are planning to collect traces using ParLOT from a set of applications with different compile flags, inputs, # of processes or threads, etc. you can add this information to the file __apps.toml__ (in `$DIFF_HOME/scripts/traceCollection`). As an example:

```
[allApps.suite]
		path = "path/to/suite/root"
		compiler = "mpicc"
		run = "mpirun -np"
		[allApps.suite.noBug]
			description = "no bug"
			sources = ["ilcs.c"]
			exec = "nb"
			compileFlags = ["-lm","-fopenmp"]
			input = ["path/to/input/input1.txt","path/to/input/input1.txt"]
			runFlags = []
		[allApps.suite.bug1]
			description = "bug1"
			sources = ["ilcs-b1.c"]
			exec = "b1"
			compileFlags = ["-lm","-fopenmp"]
			input = ["path/to/input/input.txt","path/to/input/input1.txt"]
			runFlags = []
```

Now let's say you want to collect traces from execution of the __bug1__ MPI application from __suite__ with 8 processes. You should run the python script `genCollectTraceScripts.py` with an input string in form of `<suiteName>.<testName>.<image>.<#processes>.<#threads>.<input>`. For the mentioned example the passing argument is like __suite.bug1.m.8.x.x__  ((m)ain/(a)ll) where last two __X__ s are because the number of threads(decided by the app) or input(identical for all tests) are not of our interest.

Line below
`python $DIFF_HOME/scripts/genCollectTraceScripts.py suite.bug1.m.8.x.x`
 will generate and run a bash script to run the app, collect traces, store them in a folder and convert them to **texTrace**. The generated traceCollection script would be stored in **$DIFF_HOME/scripts/traceCollection/collectTrace** for reuse/modification. Also a folder with the name __suite.bug1.m.8.x.x__ would be generated under `$DIFF_HOME/data` with two subfolders 1) __trace__ where it stores actual compressed traces and 2) __texTrace__ where the text-form decompressed traces are stored for analysis.

Any other data including extracted attributes, concept lattices, matrices, MRRs and diffMRRs would be stored under the same folder in `$DIFF_HOME/data`

