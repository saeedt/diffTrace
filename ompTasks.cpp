#include <stdio.h>
#include <omp.h>

using namespace std;

int fib(int n)
{
    int a,b;
    if(n == 0 || n == 1)
        return n;
    else{
	#pragma omp task shared(a) firstprivate(n)
	a = fib(n-1);
	#pragma omp task shared(b) firstprivate(n)
        b = fib(n-2);
	#pragma omp taskwait
	return a+b;
    }

}

int main(){
  #pragma omp parallel
  {
    #pragma omp single
    {
    printf("Fib(40): %d\n",fib(40));
    } 
  }  
     
}

