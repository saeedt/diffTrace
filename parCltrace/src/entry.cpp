/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
 * Code: entry.cpp
 * Description: decompression of ParLOT traces
 */
#include "nlr.h"
// how to retireve Loops from dtab
//    dtab[did] = info / L_lid
//    rdtab[L_lid] = did
//    rdtab[info] = did
//    ltab[lid] = LoopBody
//    rltab[LoopBody] = lid


//Entry static Init
int Entry::distinctElemenets = 0;
int Entry::ldataLen = 0;
int Entry::origLen = 0;
int Entry::fdataLen = 0;
int Entry::maxLoopBody = 0;
int Entry::maxLC = 0 ;

map<int,string> Entry::dtab ;
map<string,int> Entry::rdtab ;
map<int,string> Entry::ltab ;
map<string,int> Entry::rltab ;

// parEntry static init
int parEntry::distinctElemenets = 0;
int parEntry::ldataLen = 0;
int parEntry::origLen = 0;
int parEntry::fdataLen = 0;
int parEntry::maxLoopBody = 0;
int parEntry::maxLC = 0 ;

map<int,string> parEntry::dtab ;
map<string,int> parEntry::rdtab ;
map<int,string> parEntry::ltab ;
map<string,int> parEntry::rltab ;


parEntry::parEntry(int tid, int eid){
  this->tid = tid;
  this->eid = eid;
  this->elements.clear();
  this->lc = 0;
}

parEntry::parEntry(){
  this->tid = -1;
  this->eid = -1;
  this->elements.clear();
  this->lc = 0;
}

parEntry::~parEntry(){}

void parEntry::addElement(string el){
  //printf("Element to add %s\n",el.c_str());
  int eli=parEntry::addToDtab(el);
//  printf("Element to add %s : %d\n",el.c_str(), eli );
	this->elements.push_back(eli);
}


vector<int> parEntry::getElements(){
  return this->elements;
}

void parEntry::setElements(vector<int> v){
  this->elements = v;
}


void parEntry::loadNewEntry(Entry & b){
  int oldLC = b.getLC();
  vector<int> oldElements = b.getElements();
  vector<int>::iterator vit;
  string t,oldLoopID,ts;
  vector<string> vs;
  int ti,li;
  for(vit = oldElements.begin(); vit != oldElements.end() ; vit++){
    t = b.retFromDtab(*vit);
    //check if t is loop
    if (t.rfind("L",0) == 0 && t.find("^") != string::npos){
      //t is loop
      vs = splitString(t,'^');
      oldLoopID = vs[0].substr(vs[0].find("L")+1);
      ts = b.retFromLtab(stoi(oldLoopID));
      li = addToLtab(ts);
      ti = addToDtab("L"+to_string(li)+"^"+vs[1]);
    }else{
      ti = addToDtab(t);
    }
    this->elements.push_back(ti);
  }
  this->lc = oldLC;
}


int parEntry::addToDtab(string s){
  //printf("INSIDE DTAB %s\n",s.c_str());
  int ret;
  //printf("INSIDE DTAB2\n");
  //int chk = this->rdtab.count(s);
  //int chk = this->rdtab.size();
  //printf("INSIDE DTAB 3333, size : %d\n",chk);
  //map<string,int>::const_iterator got = this->rdtab.find(s);
  //printf("INSIDE DTAB 4444, size : %d\n",chk);
  if (this->rdtab.count(s) > 0){
    // s exist
    ret = rdtab[s];
    return ret;
  }else{
    ret = this->dtab.size();
    dtab[ret] = s;
    rdtab[s] = ret;
    return ret;
  }
}

string parEntry::retFromDtab(int i){
  return this->dtab[i];
}

int parEntry::addToLtab(string s){
  int ret;
  if (this->rltab.count(s) > 0){
    // s exist
    ret = rltab[s];
    return ret;
  }else{
    ret = this->ltab.size();
    ltab[ret] = s;
    rltab[s] = ret;
    return ret;
  }
}

string parEntry::retFromLtab(int i){
  return this->ltab[i];
}



void parEntry::setLC(int lcc){
	this->lc = lcc;
}

int parEntry::getLC(){
	return this->lc;
}


string parEntry::toString(){
  string st = "";
  //st = "TID:"+to_string(this->tid)+", EID:"+to_string(this->eid);
	vector<int>::iterator vit;
	if (this->lc == 1){
    //printf("%s - %s \n",retFromDtab(this->elements[0]).c_str(),retFromDtab(this->elements[1]).c_str());
    assert(this->elements.size() == 1);
		return retFromDtab(this->elements[0]);
	} else{
		for (vit = (this->elements).begin();vit != (this->elements).end(); vit++){
      //tmp = retFromTable(*vit);
      st = st + retFromDtab(*vit);
      if (vit != (this->elements).end() - 1 ){
          st = st + " - ";
      }
		}
    return "L"+to_string(parEntry::addToLtab(st))+"^"+to_string(this->lc);
	}
}

void parEntry::fillLtab(){
  string st = "";
	vector<int>::iterator vit;
	if (this->lc != 1){
    //printf("%s - %s \n",retFromDtab(this->elements[0]).c_str(),retFromDtab(this->elements[1]).c_str());
    assert(this->elements.size() != 0);
    for (vit = (this->elements).begin();vit != (this->elements).end(); vit++){
      //tmp = retFromTable(*vit);
      st = st + retFromDtab(*vit);
      if (vit != (this->elements).end() - 1 ){
          st = st + " - ";
      }
		}
    addToLtab(st);
	}
	return ;
}

string parEntry::ltabToString(){
  string s = "";
  //s = s + "TID: " + to_string(this->tid)
  map<int,string>::iterator mit;
  for(mit = (this->ltab).begin() ; mit != (this->ltab).end() ; mit++){
    s = s + to_string(mit->first) + ':' + mit->second + "\n";
  }
  return s;
}


string parEntry::dtabToString(){
  string s = "";
  map<int,string>::iterator mit;
  for(mit = (this->dtab).begin() ; mit != (this->dtab).end() ; mit++){
    s = s + to_string(mit->first) + ':' + mit->second + "\n";
  }
  return s;
}

void parEntry::loadDtab(string s){
  vector<string> sp = splitString(s,':');
  this->dtab[stoi(sp[0])] = sp[1];
  this->rdtab[sp[1]]= stoi(sp[0]);
}

void parEntry::loadLtab(string s){
  vector<string> sp = splitString(s,':');
  this->ltab[stoi(sp[0])] = sp[1];
  this->rltab[sp[1]]= stoi(sp[0]);
}


void parEntry::setOrigLen(int l){
  this->origLen = l;
}
void parEntry::setFdataLen(int l){
  this->fdataLen = l;
}
void parEntry::setLdataLen(int l){
  this->ldataLen = l;
}
int parEntry::getOrigLen(){
  return this->origLen;
}
int parEntry::getFdataLen(){
  return this->fdataLen;
}
int parEntry::getLdataLen(){
  return this->ldataLen;
}
void parEntry::setDistinctElements(int d){
  this->distinctElemenets = d;
}
int parEntry::getDistinctElements(){
  return this->distinctElemenets;
}

string parEntry::statToString(){
  string s="";
  s = s + "distElements:"+to_string(this->distinctElemenets) + "\n";
  s = s + "origLen:"+to_string(this->origLen) + "\n";
  s = s + "fdataLen:"+to_string(this->fdataLen) + "\n";
  s = s + "ldataLen:"+to_string(this->ldataLen) + "\n";
  return s;
}

int parEntry::numOfLoops(){
  return this->ltab.size();
}

void parEntry::setMaxLoopBody(int lb){
  if (lb > this->maxLoopBody){
    this->maxLoopBody = lb;
  }
}

int parEntry::getMaxLoopBody(){
  return this->maxLoopBody;
}

void parEntry::setMaxLC(int lc){
  if (lc > this->maxLC){
    this->maxLC = lc;
  }
}

int parEntry::getMaxLC(){
  return this->maxLC;
}

void parEntry::clear(){
  this->elements.clear();
  this->lc = 0;
}

void parEntry::setLtab(map<int,string> ltab){
  this->ltab = ltab;
}
void parEntry::setRLtab(map<string,int> rltab){
  this->rltab = rltab;
}

void parEntry::setDtab(map<int,string> dtab){
  this->dtab = dtab;
}

void parEntry::setRDtab(map<string,int> rdtab){
  this->rdtab = rdtab;
}

map<int,string> parEntry::getDtab(){
  return this->dtab;
}
map<string,int> parEntry::getRDtab(){
  return this->rdtab;
}
map<int,string> parEntry::getLtab(){
  return this->ltab;
}
map<string,int> parEntry::getRLtab(){
  return this->rltab;
}

////////////////////////////////////////////////////////////////////////////
//  ENTRY
////////////////////////////////////////////////////////////////////////////


Entry::Entry(){
  this->elements.clear();
  this->lc = 0;
}

Entry::~Entry(){}

void Entry::addElement(string el){
  //printf("Element to add %s\n",el.c_str());
  int eli=Entry::addToDtab(el);
//  printf("Element to add %s : %d\n",el.c_str(), eli );
	this->elements.push_back(eli);
}

vector<int> Entry::getElements(){
  return this->elements;
}

void Entry::setElements(vector<int> v){
  this->elements = v;
}

int Entry::addToDtab(string s){
  //printf("INSIDE DTAB %s\n",s.c_str());
  int ret;
  //printf("INSIDE DTAB2\n");
  //int chk = this->rdtab.count(s);
  //int chk = this->rdtab.size();
  //printf("INSIDE DTAB 3333, size : %d\n",chk);
  //map<string,int>::const_iterator got = this->rdtab.find(s);
  //printf("INSIDE DTAB 4444, size : %d\n",chk);
  if (this->rdtab.count(s) > 0){
    // s exist
    ret = rdtab[s];
    return ret;
  }else{
    ret = this->dtab.size();
    dtab[ret] = s;
    rdtab[s] = ret;
    return ret;
  }
}

string Entry::retFromDtab(int i){
  return this->dtab[i];
}

int Entry::addToLtab(string s){
  int ret;
  if (this->rltab.count(s) > 0){
    // s exist
    ret = rltab[s];
    return ret;
  }else{
    ret = this->ltab.size();
    ltab[ret] = s;
    rltab[s] = ret;
    return ret;
  }
}

string Entry::retFromLtab(int i){
  return this->ltab[i];
}



void Entry::setLC(int lcc){
	this->lc = lcc;
}

int Entry::getLC(){
	return this->lc;
}

int Entry::getElementLen(){
  //printf("inside getELEMENT %d \n", this->elements.size());
  return this->elements.size();
}

bool Entry::operator==(const Entry& b){
  if (this->lc != b.lc){
    return false;
  }
  else if (this->elements.size() != b.elements.size()){
    return false;
  } else{
    for(unsigned int i = 0 ; i < this->elements.size() ; i++){
      if (this->elements[i] != b.elements[i]){
        return false;
      }
    }
    return true;
  }
}

void Entry::incLC(){
	this->lc++;
}

string Entry::toString(){
	string s = "";
  string st = "";
	vector<int>::iterator vit;
	if (this->lc == 1){
    //printf("%s - %s \n",retFromDtab(this->elements[0]).c_str(),retFromDtab(this->elements[1]).c_str());
    assert(this->getElementLen() == 1);
		return retFromDtab(this->elements[0]);
	} else{
		for (vit = (this->elements).begin();vit != (this->elements).end(); vit++){
      //tmp = retFromTable(*vit);
      st = st + retFromDtab(*vit);
      if (vit != (this->elements).end() - 1 ){
          st = st + " - ";
      }
		}
    return "L"+to_string(Entry::addToLtab(st))+"^"+to_string(this->lc);
	}
}


void Entry::fillLtab(){
  string st = "";
	vector<int>::iterator vit;
	if (this->lc != 1){
    //printf("%s - %s \n",retFromDtab(this->elements[0]).c_str(),retFromDtab(this->elements[1]).c_str());
    assert(this->getElementLen() != 0);
    for (vit = (this->elements).begin();vit != (this->elements).end(); vit++){
      //tmp = retFromTable(*vit);
      st = st + retFromDtab(*vit);
      if (vit != (this->elements).end() - 1 ){
          st = st + " - ";
      }
		}
    Entry::addToLtab(st);
	}
	return ;
}


string Entry::ltabToString(){
  string s = "";
  map<int,string>::iterator mit;
  for(mit = (this->ltab).begin() ; mit != (this->ltab).end() ; mit++){
    s = s + to_string(mit->first) + ':' + mit->second + "\n";
  }
  return s;
}


string Entry::dtabToString(){
  string s = "";
  map<int,string>::iterator mit;
  for(mit = (this->dtab).begin() ; mit != (this->dtab).end() ; mit++){
    s = s + to_string(mit->first) + ':' + mit->second + "\n";
  }
  return s;
}

void Entry::loadDtab(string s){
  vector<string> sp = splitString(s,':');
  this->dtab[stoi(sp[0])] = sp[1];
  this->rdtab[sp[1]]= stoi(sp[0]);
}

void Entry::loadLtab(string s){
  vector<string> sp = splitString(s,':');
  this->ltab[stoi(sp[0])] = sp[1];
  this->rltab[sp[1]]= stoi(sp[0]);
}


void Entry::setOrigLen(int l){
  this->origLen = l;
}
void Entry::setFdataLen(int l){
  this->fdataLen = l;
}
void Entry::setLdataLen(int l){
  this->ldataLen = l;
}
int Entry::getOrigLen(){
  return this->origLen;
}
int Entry::getFdataLen(){
  return this->fdataLen;
}
int Entry::getLdataLen(){
  return this->ldataLen;
}
void Entry::setDistinctElements(int d){
  this->distinctElemenets = d;
}
int Entry::getDistinctElements(){
  return this->distinctElemenets;
}

string Entry::statToString(){
  string s="";
  s = s + "distElements:"+to_string(this->distinctElemenets) + "\n";
  s = s + "origLen:"+to_string(this->origLen) + "\n";
  s = s + "fdataLen:"+to_string(this->fdataLen) + "\n";
  s = s + "ldataLen:"+to_string(this->ldataLen) + "\n";
  return s;
}

int Entry::numOfLoops(){
  return this->ltab.size();
}

void Entry::setMaxLoopBody(int lb){
  if (lb > this->maxLoopBody){
    this->maxLoopBody = lb;
  }
}

int Entry::getMaxLoopBody(){
  return this->maxLoopBody;
}

void Entry::setMaxLC(int lc){
  if (lc > this->maxLC){
    this->maxLC = lc;
  }
}

int Entry::getMaxLC(){
  return this->maxLC;
}

void Entry::clear(){
  this->elements.clear();
  this->lc = 0;
}

void Entry::setLtab(map<int,string> ltab){
  this->ltab = ltab;
}
void Entry::setRLtab(map<string,int> rltab){
  this->rltab = rltab;
}

void Entry::setDtab(map<int,string> dtab){
  this->dtab = dtab;
}

void Entry::setRDtab(map<string,int> rdtab){
  this->rdtab = rdtab;
}

map<int,string> Entry::getDtab(){
  return this->dtab;
}
map<string,int> Entry::getRDtab(){
  return this->rdtab;
}
map<int,string> Entry::getLtab(){
  return this->ltab;
}
map<string,int> Entry::getRLtab(){
  return this->rltab;
}
