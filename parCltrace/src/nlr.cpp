/** nlr.cpp
 * Implementing NLR algorithm (nested loop detection)
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
 */
#include "nlr.h"

VecStack<Entry> vent;
int K1;


//Entry serialNLR(vector<Entry> seq, int k){
void parNLR(vector<Entry> &seq, int k, int tid,vector<Entry> &out ){
  VecStack<Entry> serialVent;
  vector<Entry> rett;
  vector<Entry>::iterator vit;
  vector<Entry>::iterator vit2;
  //K1=k;

  for(vit = seq.begin() ; vit != seq.end() ; vit++){
    serialVent.push(*vit);
    //printf("\t before reduce\n");
    serialReduce(serialVent,k);
    //printf("\t After reduce\n");
    if (serialVent.len() > SMAX){
      // FLUSH N elements from bottom of the stack
      rett = serialVent.pop_nbottom(NFLUSH);
      for (vit2 = rett.begin() ; vit2 != rett.end() ; vit2++){
        out.push_back(*vit2);
      }
      rett.clear();
    }
  }
  while (! serialVent.isEmpty()){
    //ret = serialVent.peek();
    out.push_back(serialVent.pop_bottom());
  }
  return ;
}


Entry nlr(vector<Entry> seq, int k,string outFile){
  int counter=0;
  int cnt = 0;
  Entry ret;
  vector<Entry> rett;
  vector<Entry>::iterator vit;
  vector<Entry>::iterator vit2;
  vent.clear();
  K1=k;

  //write to file
  ofstream fo(outFile);
  if (!fo.is_open()){
    printf("NLR:I/O Error\nUnable to open/write %s\n",(outFile).c_str());
  }
  printf("NLR - Input Length :%lu\n",seq.size());
  //printf("\t before first loop NLR()\n");
  for(vit = seq.begin() ; vit != seq.end() ; vit++){
    if (cnt % 10000 == 0){
      printf("Inserting element %d/%lu\n",cnt,seq.size());
    }
    vent.push(*vit);
    //printf("\t before reduce\n");
    reduce();
    if (vent.len() > SMAX){
      ret = vent.peek();
      // FLUSH N elements from bottom of the stack
      rett = vent.pop_nbottom(NFLUSH);
      for (vit2 = rett.begin() ; vit2 != rett.end() ; vit2++){
        fo << (*vit2).toString() << endl;
        counter++;
      }
      rett.clear();
    }
    else{
      ret = vent.peek();
    }
    cnt++;
  }
  while (! vent.isEmpty()){
    ret = vent.peek();
    fo << (vent.pop_bottom()).toString() << endl;
    counter++;
  }
  fo.close();
  ret.setLdataLen(counter);
  return ret;
}


void reduce(){
  // global maxLC

  // assert stack is not empty
  //printf("\t\t Within reduce\n");

  Entry tmp;
  int i,b;
  for(i = 2; i < 3*K1 ; i++){
    //printf("\t\t K LOOP i=%d\n",i);
    if(i%3 ==0){
      b = i/3 ;
      //printf("\t\t\tbefore triplet\n");
      //printf("\t\t current Stack: %s\n",vent.toString().c_str());
      //Entry tmp = triplet(vent.peek_range(3*b,2*b+1),vent.peek_range(2*b,b+1),vent.peek_range(b,1));
      vector<Entry> u = vent.peek_range(3*b,2*b+1);
      vector<Entry> v = vent.peek_range(2*b,b+1) ;
      vector<Entry> w = vent.peek_range(b,1);
      tmp.clear();
      triplet(u,v,w,tmp);
      //printf("\t\t\tafter triplet\n");
      if (tmp.getLC() != 0){
        vent.pop_ntop(i);
        vent.push(tmp);
        reduce();
        return;
      }
    }
    //printf("\t\t After first if\n");
    if (i > vent.len()){
      //printf("\t\t Here?\n");
      return;
    } else if (i <= K1+1){
      //printf("\t\t or Here?\n");
      //printf("\t\t current Stack: %s\n",vent.toString().c_str());
      if (vent.peek_n(i).getLC() != 1 && follows(vent.peek_n(i),vent.peek_range(i-1,1))){
        //printf("\t\t or even Here?\n");
        vent.pop_ntop(i-1);
        Entry tmp1 = vent.pop();
        tmp1.incLC();
        tmp1.setMaxLC(tmp1.getLC());
        vent.push(tmp1);
        reduce();
        return;
      }
    }
  }
}

bool follows(Entry u, vector<Entry> v ){
  // assert len(u.elements) == 1
  // elements of u == [x for x in elements of v]
  //printf("\t\t\t\tIN FOLLOWS\n");
  vector<int> velemenets;
  vector<int> vtmp;
  vector<int>::iterator vit;
  vector<Entry>::iterator vec_it;
  //printf("\tbefore first for\n");
  for (vec_it = v.begin() ; vec_it != v.end() ; vec_it++){
    //printf("\t\tbefore second for %s\n",(*vec_it).toString().c_str());
    vtmp = (*vec_it).getElements();
    //printf("\t\tbefore second for %s , len: %d\n",(*vec_it).toString().c_str(),vtmp.size());
    for (vit = vtmp.begin() ; vit != vtmp.end() ; vit++){
      //printf("\t\t\twithin second for %d\n",*vit);
      velemenets.push_back(*vit);
    }
  }
  //printf("\tafter fors\n");
  if (velemenets.size() != (unsigned int)u.getElementLen()){
    return false;
  }
  for (int i = 0 ; i < u.getElementLen() ; i++){
    if (u.getElements()[i] != velemenets[i]){
      return false;
    }
  }
  return true;
}

void triplet(vector<Entry> &u,vector<Entry> &v,vector<Entry> &w, Entry& ret){
  //printf("\t\t\t\twithin triplet\n");
  ret.setLC(0);
  unsigned int i;
  vector<int> newEntryElements;
  //<int>::iterator vit;
  string s,tmp;
  //typename vector<Entry>::iterator vit;
  //printf("\t\t\t\tBefore check\n");
  if (! (u.size() == v.size() && u.size() == w.size() )){
    return;
  } else{
    for (i = 0 ; i < u.size() ; i++){
      if (! (u[i] == v[i] && u[i] == w[i] ) ){
        return ;
      }
    }
  }
  //printf("\t\t\t\tAfter check\n");
  ret.setLC(3);
  ret.setMaxLC(3);
  ret.setMaxLoopBody(u.size());
  s = "";
  for (i = 0 ; i < u.size() ; i++){
    // add element.toString to new entry elements (consequently to dtab)
    tmp = u[i].toString();
    ret.addElement(tmp);
    // add element.toString to S to be added to ltab
    s = s + u[i].toString();
    if (i < u.size() -1 ){
      s = s + " - ";
    }
  }
  ret.addToLtab(s);
  return;
}




//void serialReduce(VecStack<Entry> &vent2){
void serialReduce(VecStack<Entry> &vent2, int k){
  // global maxLC
  // assert stack is not empty
  //printf("\t\t Within reduce\n");

  Entry tmp,tmp1;
  int i,b;
  //K1=10;

  for(i = 2; i < 3*k ; i++){
    //printf("\t\t K LOOP i=%d\n",i);
    if(i%3 ==0){
      b = i/3 ;
      vector<Entry> u = vent2.peek_range(3*b,2*b+1);
      vector<Entry> v = vent2.peek_range(2*b,b+1) ;
      vector<Entry> w = vent2.peek_range(b,1);
      tmp.clear();
      triplet(u,v,w,tmp);
      //printf("\t\t\tafter triplet\n");
      if (tmp.getLC() != 0){
        //printf("\ttriplet\n");
        vent2.pop_ntop(i);
        vent2.push(tmp);
        serialReduce(vent2,k);
        tmp.clear();
        return;
      }
    }
    //printf("\t\t After first if\n");
    if (i > vent2.len()){
      //printf("\t\t Here?\n");
      return;
    } else if (i <= k+1){
      if (vent2.peek_n(2).getLC() != 1 && vent2.peek().getElements() == vent2.peek_n(2).getElements() ){
        tmp = vent2.pop();
        tmp1 = vent2.pop();
        for(int jj=0; jj<tmp.getLC(); jj++){
          tmp1.incLC();
        }
        tmp1.setMaxLC(tmp1.getLC());
        vent2.push(tmp1);
        serialReduce(vent2,k);
        return;
      }
      if (vent2.peek_n(i).getLC() != 1 && follows(vent2.peek_n(i),vent2.peek_range(i-1,1))){
        vent2.pop_ntop(i-1);
        tmp1 = vent2.pop();
        tmp1.incLC();
        tmp1.setMaxLC(tmp1.getLC());
        vent2.push(tmp1);
        serialReduce(vent2,k);
        return;
      }

    }
  }
}


//void parReduce(VecStack<Entry> &vent2){}
