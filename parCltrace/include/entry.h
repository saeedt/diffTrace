/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
 * Code: entry.h
 * Description: decompression of ParLOT traces
 */
#ifndef ENTRY_H
#define ENTRY_H


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <dirent.h>
#include <unistd.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iterator>
#include <regex>
#include <assert.h>
#include <omp.h>
#include "util.h"


using namespace std;

class Entry{
  vector<int> elements;
  int lc;
  static int distinctElemenets;
  static int origLen;
  static int ldataLen;
  static int fdataLen;
  static int maxLC;
  static int maxLoopBody;
  static map<int,string> dtab;
  static map<string,int> rdtab;
  static map<int,string> ltab;
  static map<string,int> rltab;
  #pragma omp threadprivate(origLen,distinctElemenets,ldataLen,fdataLen,maxLC,maxLoopBody,dtab,rdtab,ltab,rltab)
public:
  //! Constructor
  Entry();

  //! Destructor
  ~Entry();

  //! Add Element
	/*!
	  Add element to the vector of current entry
	*/
  void addElement(string el);

  void setElements(vector<int> els);

  vector<int> getElements();

  //! Set loop coint to lc
  void setLC(int lc);

  //! Increment loop count by one
  void incLC();

  //! Return string representation of the entry
  string toString();

  //! Overloading == operator for entries
  bool operator==(const Entry& b);

  //! Return loop count of current entry
  int getLC();

  //! Return size of elements in current entry
  int getElementLen();

  //! add new entry (element) to ltab-rltab
  int addToDtab(string s);
  int addToLtab(string s);

  void fillLtab();

  //! return ltab elements
  string retFromDtab(int i);
  string retFromLtab(int i);

  string ltabToString();
  string dtabToString();
  void loadLtab(string s);
  void loadDtab(string s);

  void setLtab(map<int,string> ltab);
  void setRLtab(map<string,int> rltab);
  void setDtab(map<int,string> dtab);
  void setRDtab(map<string,int> rdtab);

  map<int,string> getDtab();
  map<string,int> getRDtab();
  map<int,string> getLtab();
  map<string,int> getRLtab();

  void setOrigLen(int l);
  void setFdataLen(int l);
  void setLdataLen(int l);
  int getOrigLen();
  int getFdataLen();
  int getLdataLen();
  void setDistinctElements(int d);
  int getDistinctElements();
  string statToString();
  int numOfLoops();
  void setMaxLoopBody(int lb);
  int getMaxLoopBody();
  void setMaxLC(int lc);
  int getMaxLC();

  // Return the body of the loop in string format (if the entry is a loop)
  string LBtoString();

  void clear();
};


class parEntry{
  int tid;
  int eid;
  vector<int> elements;
  int lc;
  static int distinctElemenets;
  static int origLen;
  static int ldataLen;
  static int fdataLen;
  static int maxLC;
  static int maxLoopBody;
  static map<int,string> dtab;
  static map<string,int> rdtab;
  static map<int,string> ltab;
  static map<string,int> rltab;
public:
  //! Constructor
  parEntry(int tid,int eid);

  parEntry();

  //! Destructor
  ~parEntry();

  void addElement(string el);

  void setElements(vector<int> els);

  vector<int> getElements();

  void loadNewEntry(Entry & b);

  int addToDtab(string s);

  string retFromDtab(int i);

  int addToLtab(string s);

  string retFromLtab(int i);

  void setLC(int lcc);

  int getLC();

  string toString();

  string ltabToString();
  string dtabToString();

  void fillLtab();

  void loadLtab(string s);
  void loadDtab(string s);

  void setLtab(map<int,string> ltab);
  void setRLtab(map<string,int> rltab);
  void setDtab(map<int,string> dtab);
  void setRDtab(map<string,int> rdtab);

  map<int,string> getDtab();
  map<string,int> getRDtab();
  map<int,string> getLtab();
  map<string,int> getRLtab();

  void setOrigLen(int l);
  void setFdataLen(int l);
  void setLdataLen(int l);
  int getOrigLen();
  int getFdataLen();
  int getLdataLen();
  void setDistinctElements(int d);
  int getDistinctElements();
  string statToString();
  int numOfLoops();
  void setMaxLoopBody(int lb);
  int getMaxLoopBody();
  void setMaxLC(int lc);
  int getMaxLC();

  // Return the body of the loop in string format (if the entry is a loop)
  string LBtoString();

  void clear();

};

#endif
