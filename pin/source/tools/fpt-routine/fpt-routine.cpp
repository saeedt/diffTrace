/*BEGIN_LEGAL 
Intel Open Source License 

Copyright (c) 2002-2018 Intel Corporation. All rights reserved.
 
Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are
met:

Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.  Redistributions
in binary form must reproduce the above copyright notice, this list of
conditions and the following disclaimer in the documentation and/or
other materials provided with the distribution.  Neither the name of
the Intel Corporation nor the names of its contributors may be used to
endorse or promote products derived from this software without
specific prior written permission.
 
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE INTEL OR
ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
END_LEGAL */

/*
 * Modified by Saeed Taheri, School of Computing, University of Utah, for FP Debugging
 * February 2018
 */
#include <iomanip>
#include <iostream>

#include "pin.H"
#include "instlib.H"

using namespace INSTLIB;

// Contains knobs to filter out things to instrument
FILTER filter;

ofstream outFile;


//ofstream out;
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "filter.out", "specify output file name");

INT32 Usage()
{
    cerr <<
        "This pin tool demonstrates use of FILTER to identify instrumentation points\n"
        "This pin tool instruments and analyze all instructions and their corresponding images and routines"
        "\n";

    cerr << KNOB_BASE::StringKnobSummary() << endl;
    return -1;
}

VOID capture(CHAR * imgName, CHAR * rtnName)
{
    outFile << "000::" << imgName << "::" << rtnName << "::000" << endl ;
}

VOID Routine(RTN rtn, VOID * val){
    const string img = IMG_Name(SEC_Img(RTN_Sec(rtn)));
    static string previmg = " ";
    static int imnum = 0;
    if (previmg == img) {
        if (imnum == 1) {
            //fprintf(info, "%s\n", rtnname);
        }
    } else {
        imnum++;
        if (imnum == 1) {
            previmg = img;
            //const char* imgname = img.c_str();
            //fprintf(info, "+\n%s\n%s\n", imgname, rtnname);
        }
    } 
    if (imnum == 1){
        RTN_Open(rtn);
        RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)capture, IARG_ADDRINT, img.c_str(), IARG_ADDRINT, RTN_Name(rtn).c_str(), IARG_END);
        RTN_Close(rtn);
    }
}

VOID Trace(TRACE trace, VOID * val)
{
    if (!filter.SelectTrace(trace))
        return;

    for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl))
    {
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins))
        {
            RTN rtn = TRACE_Rtn(trace);
            if (RTN_Valid(rtn))
            {
                IMG img = SEC_Img(RTN_Sec(rtn));
                if (IMG_Valid(img)) {
                    string dis = INS_Disassemble(ins);
                    char * tmp2 = new char[dis.length() + 1];
                    strcpy(tmp2,dis.c_str());
                    INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)capture, IARG_ADDRINT, IMG_Name(img).c_str(), IARG_ADDRINT, RTN_Name(rtn).c_str(), IARG_ADDRINT, tmp2,  IARG_END);
                }
            }
        }
    }
}


// argc, argv are the entire command line, including pin -t <toolname> -- ...
int main(int argc, char * argv[])
{
    if( PIN_Init(argc,argv) )
    {
        return Usage();
    }
    outFile.open(KnobOutputFile.Value().c_str());

    //TRACE_AddInstrumentFunction(Trace, 0);
    RTN_AddInstrumentFunction(Routine, 0);
    
    filter.Activate();
    
    // Start the program, never returns
    PIN_StartProgram();
    return 0;
}
