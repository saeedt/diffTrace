/*
Pin tool for generating compressed call traces of multithreaded programs.

This tool captures only the functions from the main image.  It corrects
the trace by inserting missing returns and uses an incremental
implementation of the "LZa3 | ZE" compressiong algorithm to compress the
trace information on-the-fly before emitting it.

Copyright (c) 2016, Texas State University. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted for academic, research, experimental, or personal use provided
that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Texas State University nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

For all other uses, please contact the Office for Commercialization and Industry
Relations at Texas State University <http://www.txstate.edu/ocir/>.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Authors: Sindhu Devale and Martin Burtscher
*/


#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
//#include <iterator>
#include "pin.H"
#include "portability.H"
//#include <memory>
//#include <tr1/memory>
//#include "cpptoml.h"
#include <bitset>

using namespace std;

#define version 17

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long uint8;


static const uint2 maxstacksize = 4096;

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "out.out", "specify output file name");

ofstream outFile;
//ofstream outFile2;
//static FILE* info = NULL;

//static string configPath = "$HOME/diffTrace/soruce/tools/fpt-bitsets/insets.toml";
static const string arr[] = {"FMA","SSE_DATA","SSE_ARITH","SSE_COMP_LOGIC","FP_DATA","FP_ARITH","FP_COMP","FP_CONTROL"};
static vector<string> insetKeys (arr, arr + sizeof(arr) / sizeof(arr[0]) );
//static vector<string> insetKeys = {"FMA","SSE_DATA","SSE_ARITH","SSE_COMP_LOGIC","FP_DATA","FP_ARITH","FP_COMP","FP_CONTROL"};
static vector<string>::iterator v_str_it;
static vector<string>::iterator v_str_it2;
static vector<string> seenRTN;

static map< string , vector <string> > instructionSets;
static map<string,vector<string>>::iterator m_str_it;


#define Q(x) #x
#define QUOTE(x) Q(x)
#define errorif(cond, msg) if (cond) {fprintf(stderr, "ERROR: %s\n", msg); exit(-1);}
#define BITSETSIZE 8

/* ********************************************************************* */
/* Hold instruction FP information for a single procedure                */
/* ********************************************************************* */
typedef struct RtnFPInfo
{
    string _name;
    string _image;
    RTN _rtn;
    UINT64 _rtnCount;
    int _insBitSet[BITSETSIZE] {0,0,0,0,0,0,0,0};
    struct RtnFPInfo * _next;
} RTN_FPINFO;


// Linked List of FP Info for each routine
RTN_FPINFO * RtnList = 0;


VOID insetsInit(){
  //string fma[] = {"VFMADD132PDy","VFMADD132PSy","VFMADD132PDx","VFMADD132PSx","VFMADD132SD","VFMADD132SS","VFMADD213PDy","VFMADD213PSy","VFMADD213PDx","VFMADD213PSx","VFMADD213SD","VFMADD213SS","VFMADD231PDy","VFMADD231PSy","VFMADD231PDx","VFMADD231PSx","VFMADD231SD","VFMADD231SS"};
  string fma[] = {"vfmadd132pdy", "vfmadd132psy", "vfmadd132pdx", "vfmadd132psx", "vfmadd132sd", "vfmadd132ss", "vfmadd213pdy", "vfmadd213psy", "vfmadd213pdx", "vfmadd213psx", "vfmadd213sd", "vfmadd213ss", "vfmadd231pdy", "vfmadd231psy", "vfmadd231pdx", "vfmadd231psx", "vfmadd231sd", "vfmadd231ss"};
  vector<string> tmp1 (fma, fma + sizeof(fma) / sizeof(fma[0]) );
  instructionSets["FMA"] = tmp1 ;

  //string sse_data[] = {"MOVSS","MOVAPS","MOVUPS","MOVLPS","MOVHPS","MOVLHPS","MOVHLPS","MOVMSKPS","SHUFPS", "UNPCKHPS", "UNPCKLPS", "CVTSI2SS", "CVTSS2SI", "CVTTSS2SI","CVTPI2PS", "CVTPS2PI", "CVTTPS2PI"};
  string sse_data[] = {"movss", "movaps", "movups", "movlps", "movhps", "movlhps", "movhlps", "movmskps", "shufps", "unpckhps", "unpcklps", "cvtsi2ss", "cvtss2si", "cvttss2si", "cvtpi2ps", "cvtps2pi", "cvttps2pi"};
  vector<string> tmp2 (sse_data, sse_data + sizeof(sse_data) / sizeof(sse_data[0]) );
  instructionSets["SSE_DATA"] = tmp2 ;

  //string sse_arith[] ={"ADDSS","SUBSS","MULSS","DIVSS","RCPSS","SQRTSS","MAXSS","MINSS","RSQRTSS","ADDPS","SUBPS","MULPS","DIVPS","RCPPS","SQRTPS","MAXPS","MINPS","RSQRTPS"} ;
  string sse_arith[] ={"addss", "subss", "mulss", "divss", "rcpss", "sqrtss", "maxss", "minss", "rsqrtss", "addps", "subps", "mulps", "divps", "rcpps", "sqrtps", "maxps", "minps", "rsqrtps"};
  vector<string> tmp3 (sse_arith, sse_arith + sizeof(sse_arith) / sizeof(sse_arith[0]) );
  instructionSets["SSE_ARITH"] = tmp3 ;

  //string sse_comp_logic[] = {"CMPSS","COMISS","UCOMISS","ANDPS","ORPS","XORPS","ANDNPS"};
  string sse_comp_logic[] = {"cmpss", "comiss", "ucomiss", "andps", "orps", "xorps", "andnps"};
  vector<string> tmp4 (sse_comp_logic, sse_comp_logic + sizeof(sse_comp_logic) / sizeof(sse_comp_logic[0]) );
  instructionSets["SSE_COMP_LOGIC"] = tmp4 ;

  //string fp_data[] = {"FBLD","FBSTP","FCMOVB","FCMOVBE","FCMOVE","FCMOVNB","FCMOVNBE","FCMOVNE","FCMOVNU","FCMOVU","FILD","FIST","FISTP","FLD","FST","FSTP","FXCH","FLD1","FLDL2E","FLDL2T","FLDLG2","FLDLN2","FLDPI","FLDZ"};
  string fp_data[] = {"fbld", "fbstp", "fcmovb", "fcmovbe", "fcmove", "fcmovnb", "fcmovnbe", "fcmovne", "fcmovnu", "fcmovu", "fild", "fist", "fistp", "fld", "fst", "fstp", "fxch", "fld1", "fldl2e", "fldl2t", "fldlg2", "fldln2", "fldpi", "fldz"};
  vector<string> tmp5 (fp_data,fp_data  + sizeof(fp_data) / sizeof(fp_data[0]) );
  instructionSets["FP_DATA"] = tmp5 ;

  //string fp_arith[] = {"FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","F2XM1","FCOS","FPATAN","FPTAN","FSIN","FSINCOS","FYL2X","FYL2XP1"};
  string fp_arith[] = {"fabs", "fadd", "faddp", "fchs", "fdiv", "fdivp", "fdivr", "fdivrp", "fiadd", "fidiv", "fidivr", "fimul", "fisub", "fisubr", "fmul", "fmulp", "fprem", "fprem1", "frndint", "fscale", "fsqrt", "fsub", "fsubp", "fsubr", "fsubrp", "fxtract", "f2xm1", "fcos", "fpatan", "fptan", "fsin", "fsincos", "fyl2x", "fyl2xp1"};
  vector<string> tmp6 (fp_arith,fp_arith  + sizeof(fp_arith) / sizeof(fp_arith[0]) );
  instructionSets["FP_ARITH"] = tmp6 ;

  //string fp_comp[] = {"FCOM","FCOMI","FCOMIP","FCOMP","FCOMPP","FICOM","FICOMP","FTST","FUCOM","FUCOMI","FUCOMIP","FUCOMP","FUCOMPP","FXAM"};
  string fp_comp[] = {"fcom", "fcomi", "fcomip", "fcomp", "fcompp", "ficom", "ficomp", "ftst", "fucom", "fucomi", "fucomip", "fucomp", "fucompp", "fxam"};
  vector<string> tmp7 (fp_comp,fp_comp  + sizeof(fp_comp) / sizeof(fp_comp[0]) );
  instructionSets["FP_COMP"] = tmp7 ;

  //string fp_control[] = {"FCLEX","FDECSTP","FFREE","FINCSTP","FINIT","FLDCW","FLDENV","FNCLEX","FNINIT","FNOP","FNSAVE","FNSTCW","FNSTENV","FNSTSW","FRSTOR","FSAVE","FSTCW","FSTENV","FSTSW","FWAIT","WAIT"};
  string fp_control[] = {"fclex", "fdecstp", "ffree", "fincstp", "finit", "fldcw", "fldenv", "fnclex", "fninit", "fnop", "fnsave", "fnstcw", "fnstenv", "fnstsw", "frstor", "fsave", "fstcw", "fstenv", "fstsw", "fwait", "wait"};
  vector<string> tmp8 (fp_control,fp_control  + sizeof(fp_control) / sizeof(fp_control[0]) );
  instructionSets["FP_CONTROL"] = tmp8 ;

  // instructionSets["SSE_ARITH"] = ["ADDSS","SUBSS","MULSS","DIVSS","RCPSS","SQRTSS","MAXSS","MINSS","RSQRTSS","ADDPS","SUBPS","MULPS","DIVPS","RCPPS","SQRTPS","MAXPS","MINPS","RSQRTPS"];
  // instructionSets["SSE_COMP_LOGIC"] = ["CMPSS","COMISS","UCOMISS","ANDPS","ORPS","XORPS","ANDNPS"];
  // instructionSets["FP_DATA"] = ["FBLD","FBSTP","FCMOVB","FCMOVBE","FCMOVE","FCMOVNB","FCMOVNBE","FCMOVNE","FCMOVNU","FCMOVU","FILD","FIST","FISTP","FLD","FST","FSTP","FXCH","FLD1","FLDL2E","FLDL2T","FLDLG2","FLDLN2","FLDPI","FLDZ"];
  // instructionSets["FP_ARITH"] = ["FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","F2XM1","FCOS","FPATAN","FPTAN","FSIN","FSINCOS","FYL2X","FYL2XP1"];
  // instructionSets["FP_COMP"] = ["FCOM","FCOMI","FCOMIP","FCOMP","FCOMPP","FICOM","FICOMP","FTST","FUCOM","FUCOMI","FUCOMIP","FUCOMP","FUCOMPP","FXAM"];
  // instructionSets["FP_CONTROL"] = ["FCLEX","FDECSTP","FFREE","FINCSTP","FINIT","FLDCW","FLDENV","FNCLEX","FNINIT","FNOP","FNSAVE","FNSTCW","FNSTENV","FNSTSW","FRSTOR","FSAVE","FSTCW","FSTENV","FSTSW","FWAIT","WAIT"];
}
/* ===================================================================== */
/* Read configuration file for instruction sets                          */
/* ===================================================================== */
// VOID readConfig(){
//   auto config = cpptoml::parse_file(configPath);
//   auto vals;
//   vector<string> instructions;
//   for (sit = insetKeys.begin() ; sit != insetKeys.end() ; sit++){
//     vals = config->get_array_of<std::string>(sit);
//     instructions.clear()
//     for (const auto& val : *vals)
//     {
//       instructions.push_back(val);
//     }
//     instructionSets[sit]=instructions;
//   }
// }

string strFirstToken(const string &text, char sep) {
	size_t start = 0, end = 0;
  if ( (end = text.find(sep, start)) != string::npos ){
    return text.substr(start, end - start);
  }else{
    return text;
  }
}

/* ===================================================================== */
/* Truncate strings                                                      */
/* ===================================================================== */
const char * StripString(const char * str,char delim, bool first)
{
  const char * token;
  if (first){
    token = strchr(str,delim);
  }else{
    token = strrchr(str,delim);
  }
  if (token)
    return token+1;
  else
    return str;
}

/* ===================================================================== */
/* This function is called before every routine is executed              */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL enterRoutine(UINT64 * counter)
{
    (*counter)++;
}

/* ===================================================================== */
/* This function is called after every routine is executed              */
/* ===================================================================== */
VOID exitRoutine(UINT64 * counter)
{
    (*counter)++;
}


/* ===================================================================== */
/* This function is called before every instruction is executed          */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL bitsetGen(INT * bs, CHAR * instName)
{
  int i = 0;
  string tmp ;
  for (v_str_it = insetKeys.begin() ; v_str_it != insetKeys.end() ; v_str_it++){
    for (v_str_it2 = instructionSets[*v_str_it].begin() ; v_str_it2 != instructionSets[*v_str_it].end() ; v_str_it2++){
      tmp = *v_str_it2;
      //outFile2 << tmp << "::" << instName << endl;
      if (strcmp(instName,tmp.c_str()) == 0){
        bs[i]++;
      }
    }
    i++;
    //if (find(instructionSets[*v_str_it].begin(),instructionSets[*v_str_it].end(),str(instName)) != instructionSets[*v_str_it].end()){
    //}
  }
}

/* ===================================================================== */
/* Pin calls this function every time a new routine is executed          */
/* ===================================================================== */
VOID Routine(RTN rtn, VOID *v)
{
  //From ParLOT DBGMain
  static string previmg = " ";
  static int imnum = 0;

  // Allocate a counter for this routine
  RTN_FPINFO * rfp = new RTN_FPINFO;

  // The RTN goes away when the image is unloaded, so save it now
  // because we need it in the fini
  rfp->_name = RTN_Name(rtn);
  rfp->_image = StripString(IMG_Name(SEC_Img(RTN_Sec(rtn))).c_str(),'/',false);
  //rfp->_insBitSet = {false,false,false,false,false,false,false,false};
  rfp->_rtnCount = 0;
  //rfp->_flag = false;

  // Add to list of routines
  rfp->_next = RtnList;
  RtnList = rfp;

  // Aux variables for instruction disassebmly
  char * inst_chr;
  string inst_str;


  const string img = IMG_Name(SEC_Img(RTN_Sec(rtn)));
  if (previmg != img) {
    imnum++;
    if (imnum == 1) {
      previmg = img;
    }
  }

  if (imnum == 1) {
    RTN_Open(rtn);
    // Insert a call at the entry point of a routine to increment the call count
    RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)enterRoutine,IARG_FAST_ANALYSIS_CALL, IARG_PTR, &(rfp->_rtnCount), IARG_END);
    if (find(seenRTN.begin(), seenRTN.end(), RTN_Name(rtn)) == seenRTN.end()){
      // For each instruction of the routine
      for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins))
      {
          // String operations on instruction disassebmly
          inst_str = strFirstToken(INS_Disassemble(ins),' ');
          inst_chr = new char[inst_str.length() + 1];
          strcpy(inst_chr,inst_str.c_str());

          // Call this function every time an instruction of a routine execute
          INS_InsertCall(ins, IPOINT_BEFORE, (AFUNPTR)bitsetGen,IARG_FAST_ANALYSIS_CALL, IARG_PTR, &(rfp->_insBitSet),IARG_ADDRINT, inst_chr, IARG_END);
      }
      seenRTN.push_back(RTN_Name(rtn));
    }
    //RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)exitRoutine, IARG_PTR, &(rfp->_flag),IARG_PTR, &(rfp->_insBitSet), IARG_END);
    RTN_Close(rtn);
  }
}

// This function is called when the application exits
// It prints the name and count for each procedure
VOID Fini(INT32 code, VOID *v)
{
  outFile << setw(60) << "Procedure" << endl
    << setw(12) << "Calls" << " "
    << setw(5) << "FMA_"  << " "
    << setw(5) << "SSED" << " "
    << setw(5) << "SSEA" << " "
    << setw(5) << "SSEC" << " "
    << setw(5) << "FP_D" << " "
    << setw(5) << "FP_A" << " "
    << setw(5) << "FP_C" << " "
    << setw(5) << "FP_N" << endl;

  for (RTN_FPINFO * rc = RtnList; rc; rc = rc->_next)
  {
    if (rc -> _rtnCount > 0){
      outFile << setw(60) << rc->_name << endl
        //<< setw(18) << hex << rc->_address << dec <<" "
        << setw(12) << rc->_rtnCount << " "
        << setw(5) << rc->_insBitSet[0] << " "
        << setw(5) << rc->_insBitSet[1] << " "
        << setw(5) << rc->_insBitSet[2] << " "
        << setw(5) << rc->_insBitSet[3] << " "
        << setw(5) << rc->_insBitSet[4] << " "
        << setw(5) << rc->_insBitSet[5] << " "
        << setw(5) << rc->_insBitSet[6] << " "
        << setw(5) << rc->_insBitSet[7] << endl;
    }
  }
}

int main(int argc, char* argv[])
{
  outFile.open(KnobOutputFile.Value().c_str());
  //outFile2.open((KnobOutputFile.Value()+"2").c_str());
  //info = fopen((KnobOutputFile.Value()+".info").c_str(), "wt");
  //spvals.open((KnobOutputFile.Value()+".spvals").c_str());
  //readConfig();
  insetsInit();
  PIN_InitSymbols();
  errorif(PIN_Init(argc, argv), "pin failed to initialize");

  RTN_AddInstrumentFunction(Routine, 0);

  // Register Fini to be called when the application exits
  PIN_AddFiniFunction(Fini, 0);

  PIN_StartProgram();
  return 0;
}

// /* ===================================================================== */
// /* Callback function when entering a routine                             */
// /* ===================================================================== */
// VOID PIN_FAST_ANALYSIS_CALL enter(UINT32 x, ADDRINT spval)
// {
//   mit = calls.find(x);
//   if (mit != calls.end()){
//     calls[x]++;
//   }else{
//     calls[x]=1;
//   }
//   spvals << x << "[" << calls[x] << "]" << " > " << spval << endl;
// }
//
// VOID PIN_FAST_ANALYSIS_CALL leave(UINT32 x, ADDRINT spval)
// {
//   mit = returns.find(x);
//   if (mit != returns.end()){
//     returns[x]++;
//   }else{
//     returns[x]=1;
//   }
//   spvals << x << "[" << returns[x] << "]" << " < " << spval << endl;
// }
// /* ===================================================================== */
// /* Add instrumentation to routines                                       */
// /* ===================================================================== */
// VOID Routine(RTN rtn, VOID* v)
// {
//   static UINT32 id = 1;
//   static string previmg = " ";
//   static int imnum = 0;
//
//   errorif(id >= 65535, "too many functions");
//   const char* rtnname = RTN_Name(rtn).c_str();
//   const string img = IMG_Name(SEC_Img(RTN_Sec(rtn)));
//   if (previmg == img) {
//     if (imnum == 1) {
//       fprintf(info, "%s\n", rtnname);
//     }
//   } else {
//     imnum++;
//     if (imnum == 1) {
//       previmg = img;
//       const char* imgname = img.c_str();
//       fprintf(info, "+\n%s\n%s\n", imgname, rtnname);
//     }
//   }
//
//   if (imnum == 1) {
//     RTN_Open(rtn);
//     RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)enter, IARG_FAST_ANALYSIS_CALL, IARG_UINT32, id, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
//     RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)leave, IARG_FAST_ANALYSIS_CALL, IARG_UINT32, id, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
//     RTN_Close(rtn);
//     id++;
//   }
// }
//
// VOID Fini(INT32 code, VOID *v)
// {
//     // Write to a file since cout and cerr maybe closed by the application
//     outFile << "Calls" << endl;
//     for(mit = calls.begin() ; mit != calls.end() ; mit++){
//       outFile << mit->first << ":" << mit->second << endl;
//     }
//     outFile << "Returns" << endl;
//     for(mit = returns.begin() ; mit != returns.end() ; mit++){
//       outFile << mit->first << ":" << mit->second << endl;
//     }
//     outFile.close();
//     spvals.close();
//
// }
