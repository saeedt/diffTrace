/*
Pin tool for generating compressed call traces of multithreaded programs.

This tool captures only the functions from the main image.  It corrects
the trace by inserting missing returns and uses an incremental
implementation of the "LZa3 | ZE" compressiong algorithm to compress the
trace information on-the-fly before emitting it.

Copyright (c) 2016, Texas State University. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted for academic, research, experimental, or personal use provided
that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Texas State University nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

For all other uses, please contact the Office for Commercialization and Industry
Relations at Texas State University <http://www.txstate.edu/ocir/>.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Authors: Sindhu Devale and Martin Burtscher
*/


#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include "pin.H"
#include "portability.H"
#include "cpptoml.h"

#define version 17

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long uint8;


static const uint2 maxstacksize = 4096;

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "out.out", "specify output file name");

ofstream outFile;
ofstream spvals;
static FILE* info = NULL;

static map<UINT32,int> calls;
static map<UINT32,int> returns;
static map<UINT32,int>::iterator mit;

static map<string,vector<string>> instructionSets;

#define Q(x) #x
#define QUOTE(x) Q(x)
#define errorif(cond, msg) if (cond) {fprintf(stderr, "ERROR: %s\n", msg); exit(-1);}

VOID readConfig(){
  
}



/* ===================================================================== */
/* Callback function when entering a routine                             */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL enter(UINT32 x, ADDRINT spval)
{
  mit = calls.find(x);
  if (mit != calls.end()){
    calls[x]++;
  }else{
    calls[x]=1;
  }
  spvals << x << "[" << calls[x] << "]" << " > " << spval << endl;
}

VOID PIN_FAST_ANALYSIS_CALL leave(UINT32 x, ADDRINT spval)
{
  mit = returns.find(x);
  if (mit != returns.end()){
    returns[x]++;
  }else{
    returns[x]=1;
  }
  spvals << x << "[" << returns[x] << "]" << " < " << spval << endl;
}
/* ===================================================================== */
/* Add instrumentation to routines                                       */
/* ===================================================================== */
VOID Routine(RTN rtn, VOID* v)
{
  static UINT32 id = 1;
  static string previmg = " ";
  static int imnum = 0;

  errorif(id >= 65535, "too many functions");
  const char* rtnname = RTN_Name(rtn).c_str();
  const string img = IMG_Name(SEC_Img(RTN_Sec(rtn)));
  if (previmg == img) {
    if (imnum == 1) {
      fprintf(info, "%s\n", rtnname);
    }
  } else {
    imnum++;
    if (imnum == 1) {
      previmg = img;
      const char* imgname = img.c_str();
      fprintf(info, "+\n%s\n%s\n", imgname, rtnname);
    }
  }

  if (imnum == 1) {
    RTN_Open(rtn);
    RTN_InsertCall(rtn, IPOINT_BEFORE, (AFUNPTR)enter, IARG_FAST_ANALYSIS_CALL, IARG_UINT32, id, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
    RTN_InsertCall(rtn, IPOINT_AFTER, (AFUNPTR)leave, IARG_FAST_ANALYSIS_CALL, IARG_UINT32, id, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
    RTN_Close(rtn);
    id++;
  }
}

VOID Fini(INT32 code, VOID *v)
{
    // Write to a file since cout and cerr maybe closed by the application
    outFile << "Calls" << endl;
    for(mit = calls.begin() ; mit != calls.end() ; mit++){
      outFile << mit->first << ":" << mit->second << endl;
    }
    outFile << "Returns" << endl;
    for(mit = returns.begin() ; mit != returns.end() ; mit++){
      outFile << mit->first << ":" << mit->second << endl;
    }
    outFile.close();
    spvals.close();

}

int main(int argc, char* argv[])
{
  outFile.open(KnobOutputFile.Value().c_str());
  info = fopen((KnobOutputFile.Value()+".info").c_str(), "wt");
  spvals.open((KnobOutputFile.Value()+".spvals").c_str());
  PIN_InitSymbols();
  errorif(PIN_Init(argc, argv), "pin failed to initialize");

  RTN_AddInstrumentFunction(Routine, 0);

  // Register Fini to be called when the application exits
  PIN_AddFiniFunction(Fini, 0);

  PIN_StartProgram();
  return 0;
}
