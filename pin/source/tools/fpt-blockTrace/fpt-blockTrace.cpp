/*
* FPT_staticBlock by Saeed Taheri, University of Utah 2019
*/

#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "pin.H"
#include "portability.H"
#include <bitset>
#include "instlib.H"

using namespace INSTLIB;
using namespace std;


/* ********************************************************************* */
/* Definitions                                                           */
/* ********************************************************************* */
typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long uint8;

static const uint1 maxthreads = 64;
static const uint2 maxstacksize = 4096;
static const uint1 hsizelg2 = 12;
static const uint2 hsize = 1 << hsizelg2;
static const uint1 psizelg2 = 16;
static const uint4 psize = 1 << psizelg2;
static const uint2 psizem1 = psize - 1;
static const uint2 csize = 256;  //small fwrite increments

static const string arr[] = {"BAS_DATA","BAS_ARITH","BAS_BIT","BAS_CON","BAS_OTH","SSE_DATA","SSE_ARITH","SSE_OTHER","SSE2_DATA","SSE2_ARITH","SSE2_OTHER","FP_DATA","FP_ARITH","FP_OTHER","AVX","AVX2","FMA"};
static vector<string> insetKeys (arr, arr + sizeof(arr) / sizeof(arr[0]) );
static vector<string>::iterator v_str_it;
static vector<string>::iterator v_str_it2;
static vector<string> seenRTN;

static map< string , vector <string> > instructionSets;
static map<string,vector<string>>::iterator m_str_it;

static string fnameprefix;
static ofstream outFile;
static ofstream infoFile;
static ofstream insFile;

#define Q(x) #x
#define QUOTE(x) Q(x)
#define errorif(cond, msg) if (cond) {fprintf(stderr, "ERROR: %s\n", msg); exit(-1);}
#define BITSETSIZE 17

// Contains knobs to filter out things to instrument
FILTER filter;
KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "out.out", "specify output file name");


/* ********************************************************************* */
/* ParLOT compression: Thread data structure                             */
/* ********************************************************************* */

static struct ThreadData {
  bool iscount;  // is most recent value in tbuf a count
  uint2 ppos;  // current position in pbuf
  uint2 lpos;  // last similar position in pbuf
  uint2 hash;  // hash of last few values
  uint1 tpos;  // current position in tbuf
  uint4 cpos;  // current position in cbuf
  uint2 tos;  // top of stack
  FILE* file;
  uint2 tbuf[4];  // temporary buffer for intermediate values
  uint1 cbuf[csize];  // compressed byte buffer
  uint2 hbuf[hsize];  // hash table of last similar position
  uint2 pbuf[psize];  // cyclic pattern buffer
  uint2 stack[maxstacksize];  // stack holding function IDs
  uint8 sp[maxstacksize];  // SP addresses
} td[maxthreads];


/* ********************************************************************* */
/* Hold instruction FP information for a single block                    */
/* ********************************************************************* */
typedef struct blkFP
{
  UINT32 _tid;
  UINT32 _bid;
  UINT32 _eid;
  string _rtnName;
  string _imgName;
  UINT64 _blkCount;
  int _insBitSet[BITSETSIZE] {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
  struct blkFP * _next;
} BLK_FP;

// Linked List of FP Info for each routine
BLK_FP * blkList = 0;


///////////////////////////////////////////////////////////////////////////////////////////////////


VOID insetsInit(){
  string basic_data[] = {"bswap", "cbw", "cdq", "cdqe", "cmova", "cmovae", "cmovb", "cmovbe", "cmovc", "cmove", "cmovg", "cmovge", "cmovl", "comvle", "cmovna", "cmovnae", "cmovnb", "cmovnbe", "cmovnc", "cmovne", "cmovng", "cmovnge", "cmovnl", "cmovnle", "cmovno", "cmovnp", "cmovns", "cmovnz", "cmovo", "cmovp", "cmovpe", "cmovpo", "cmovs", "cmovz", "cmpxchg", "cmpxchg8b", "cqo", "cqo", "cwd", "cwde", "mov", "movabs", "movabs", "movsx", "movzx", "pop", "popa", "popad", "push", "pusha", "pushad", "xadd", "xchg", "xchg"};
  vector<string> tmp_1 (basic_data, basic_data + sizeof(basic_data) / sizeof(basic_data[0]) );
  instructionSets["BAS_DATA"] = tmp_1 ;

  string basic_arith[] = {"adc", "add", "cmp", "dec", "div", "idiv", "imul", "inc", "mul", "neg", "sbb", "sub","aaa","aad","aam","aas","daa", "das"};
  vector<string> tmp_2 (basic_arith, basic_arith + sizeof(basic_arith) / sizeof(basic_arith[0]) );
  instructionSets["BAS_ARITH"] = tmp_2 ;

  string basic_bitop[] = {"rcl", "rcr", "rol", "ror", "sal", "sar", "shl", "shld", "shr", "shrd", "bsf", "bsr", "bt", "btc", "btr", "bts", "seta", "setae", "setb", "setbe", "setc", "sete", "setg", "setge", "setl", "setle", "setna", "setnae", "setnb", "setnbe", "setnc", "setne", "setng", "setnge", "setnl", "setnle", "setno", "setnp", "setns", "setnz", "seto", "setp", "setpe", "setpo", "sets", "setz", "test","and","or","xor","not"};
  vector<string> tmp_3 (basic_bitop, basic_bitop + sizeof(basic_bitop) / sizeof(basic_bitop[0]) );
  instructionSets["BAS_BIT"] = tmp_3 ;

  string basic_control[] = {"bound", "call", "enter", "int", "into", "iret", "ja", "jae", "jb", "jbe", "jc", "jcxz", "je", "jecxz", "jg", "jge", "jl", "jle", "jmp", "jnae", "jnb", "jnbe", "jnc", "jne", "jng", "jnge", "jnl", "jnle", "jno", "jnp", "jns", "jnz", "jo", "jp", "jpe", "jpo", "js", "jz", "call", "leave", "loop", "loope", "loopne", "loopnz", "loopz", "ret"} ;
  vector<string> tmp_4 (basic_control, basic_control + sizeof(basic_control) / sizeof(basic_control[0]) );
  instructionSets["BAS_CON"] = tmp_4 ;

  string basic_other[] = {"cmps", "cmpsb", "cmpsd", "cmpsw", "lods", "lodsb", "lodsd", "lodsw", "movs", "movsb", "movsd", "movsw", "rep", "repne", "repnz", "repe", "repz", "scas", "scasb", "scasd", "scasw", "stos", "stosb", "stosd", "stosw", "in", "ins", "insb", "insd", "insw", "out", "outs", "outsb", "outsd", "outsw", "clc", "cld", "cli", "cmc", "lahf", "popf", "popfl", "pushf", "pushfl", "sahf", "stc", "std", "sti", "lds", "les", "lfs", "lgs", "lss", "cpuid", "lea", "nop", "ud2", "xlat", "xlatb"};
  vector<string> tmp_5 (basic_other, basic_other + sizeof(basic_other) / sizeof(basic_other[0]) );
  instructionSets["BAS_OTH"] = tmp_5 ;

  //string sse_data[] = {"MOVSS","MOVAPS","MOVUPS","MOVLPS","MOVHPS","MOVLHPS","MOVHLPS","MOVMSKPS","SHUFPS", "UNPCKHPS", "UNPCKLPS", "CVTSI2SS", "CVTSS2SI", "CVTTSS2SI","CVTPI2PS", "CVTPS2PI", "CVTTPS2PI"};
  string sse_data[] = {"movaps","movhlps","movhps","movlhps","movlps","movmskps","movss","movups"};
  vector<string> tmp (sse_data, sse_data + sizeof(sse_data) / sizeof(sse_data[0]) );
  instructionSets["SSE_DATA"] = tmp ;

  //string sse_arith[] ={"ADDSS","SUBSS","MULSS","DIVSS","RCPSS","SQRTSS","MAXSS","MINSS","RSQRTSS","ADDPS","SUBPS","MULPS","DIVPS","RCPPS","SQRTPS","MAXPS","MINPS","RSQRTPS"} ;
  string sse_arith[] ={"addps","addss","divps","divss","maxps","maxss","minps","minss","mulps","mulss","rcpps","rcpss","rsqrtps","rsqrtss","sqrtps","sqrtss","subps","subss"};
  vector<string> tmp1 (sse_arith, sse_arith + sizeof(sse_arith) / sizeof(sse_arith[0]) );
  instructionSets["SSE_ARITH"] = tmp1 ;

  //string sse_comp_logic[] = {"CMPSS","COMISS","UCOMISS","ANDPS","ORPS","XORPS","ANDNPS"};
  string sse_other[] = {"cmpps","cmpss","comiss","ucomiss","andnps","andps","orps","xorps","shufps","unpckhps","unpcklps","cvtpi2ps","cvtps2pi","cvtsi2ss","cvtss2si","cvttps2pi","cvttss2si","ldmxcsr","stmxcsr","maskmovq","movntps","movntq","prefetchnta","prefetcht0","prefetcht1","prefetcht2","sfence"};
  vector<string> tmp2 (sse_other, sse_other + sizeof(sse_other) / sizeof(sse_other[0]) );
  instructionSets["SSE_OTHER"] = tmp2 ;

  string sse2_Data[] = {"movapd","movhpd","movlpd","movmskpd","movsd","movupd"};
  vector<string> tmp3 (sse2_Data,sse2_Data  + sizeof(sse2_Data) / sizeof(sse2_Data[0]) );
  instructionSets["SSE2_DATA"] = tmp3 ;

  string sse2_Arith[] = {"addpd","addsd","divpd","divsd","maxpd","maxsd","minpd","minsd","mulpd","mulsd","sqrtpd","sqrtsd","subpd","subsd"};
  vector<string> tmp4 (sse2_Arith,sse2_Arith  + sizeof(sse2_Arith) / sizeof(sse2_Arith[0]) );
  instructionSets["SSE2_ARITH"] = tmp4 ;

  string sse2_Other[] = {"andnpd","andpd","orpd","xorpd","cmppd","cmpsd","comisd","ucomisd","shufpd","unpckhpd","unpcklpd","cvtdq2pd","cvtpd2dq","cvtpd2pi","cvtpd2ps","cvtpi2pd","cvtps2pd","cvtsd2si","cvtsd2ss","cvtsi2sd","cvtss2sd","cvttpd2dq","cvttpd2pi","cvttsd2si","cvtdq2ps","cvtps2dq","cvttps2dq","movdq2q","movdqa","movdqu","movq2dq","paddq","pmuludq","pshufd","pshufhw","pshuflw","pslldq","psrldq","psubq","punpckhqdq","punpcklqdq","lflush","ence","maskmovdqu","mfence","movntdq","movnti","movntpd","pause"};
  vector<string> tmp5 (sse2_Other,sse2_Other  + sizeof(sse2_Other) / sizeof(sse2_Other[0]) );
  instructionSets["SSE2_OTHER"] = tmp5 ;

  //string fp_data[] = {"FBLD","FBSTP","FCMOVB","FCMOVBE","FCMOVE","FCMOVNB","FCMOVNBE","FCMOVNE","FCMOVNU","FCMOVU","FILD","FIST","FISTP","FLD","FST","FSTP","FXCH","FLD1","FLDL2E","FLDL2T","FLDLG2","FLDLN2","FLDPI","FLDZ"};
  string fp_data[] = {"insertps","fbld", "fbstp", "fcmovb", "fcmovbe", "fcmove", "fcmovnb", "fcmovnbe", "fcmovne", "fcmovnu", "fcmovu", "fild", "fist", "fistp", "fld", "fst", "fstp", "fxch", "fld1", "fldl2e", "fldl2t", "fldlg2", "fldln2", "fldpi", "fldz"};
  vector<string> tmp6 (fp_data,fp_data  + sizeof(fp_data) / sizeof(fp_data[0]) );
  instructionSets["FP_DATA"] = tmp6 ;

  //string fp_arith[] = {"FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","F2XM1","FCOS","FPATAN","FPTAN","FSIN","FSINCOS","FYL2X","FYL2XP1"};
  string fp_arith[] = {"fabs", "fadd", "faddp", "fchs", "fdiv", "fdivp", "fdivr", "fdivrp", "fiadd", "fidiv", "fidivr", "fimul", "fisub", "fisubr", "fmul", "fmulp", "fprem", "fprem1", "frndint", "fscale", "fsqrt", "fsub", "fsubp", "fsubr", "fsubrp", "fxtract", "f2xm1", "fcos", "fpatan", "fptan", "fsin", "fsincos", "fyl2x", "fyl2xp1"};
  vector<string> tmp7 (fp_arith,fp_arith  + sizeof(fp_arith) / sizeof(fp_arith[0]) );
  instructionSets["FP_ARITH"] = tmp7 ;

  //string fp_comp[] = {"FCOM","FCOMI","FCOMIP","FCOMP","FCOMPP","FICOM","FICOMP","FTST","FUCOM","FUCOMI","FUCOMIP","FUCOMP","FUCOMPP","FXAM","FCLEX","FDECSTP","FFREE","FINCSTP","FINIT","FLDCW","FLDENV","FNCLEX","FNINIT","FNOP","FNSAVE","FNSTCW","FNSTENV","FNSTSW","FRSTOR","FSAVE","FSTCW","FSTENV","FSTSW","FWAIT","WAIT"};
  string fp_comp[] = {"fclex", "fdecstp", "ffree", "fincstp", "finit", "fldcw", "fldenv", "fnclex", "fninit", "fnop", "fnsave", "fnstcw", "fnstenv", "fnstsw", "frstor", "fsave", "fstcw", "fstenv", "fstsw", "fwait", "wait","fcom", "fcomi", "fcomip", "fcomp", "fcompp", "ficom", "ficomp", "ftst", "fucom", "fucomi", "fucomip", "fucomp", "fucompp", "fxam","fxrstor","fxsave"};
  vector<string> tmp8 (fp_comp,fp_comp  + sizeof(fp_comp) / sizeof(fp_comp[0]) );
  instructionSets["FP_OTHER"] = tmp8 ;

  string avx[] = {"vaddpd","vaddps","vaddsd ","vaddss","vaddsubpd","vaddsubps","vandnpd","vandnps","vandpd","vandps","vblendpd","vblendps","vblendvpd","vblendvps","vcmpeq_ospd","vcmpeq_uqpd","vcmpeq_uspd","vcmpeqpd","vcmpfalse_ospd","vcmpfalsepd","vcmpge_oqpd","vcmpgepd","vcmpgt_oqpd","vcmpgtpd","vcmple_oqpd","vcmplepd","vcmplt_oqpd","vcmpltpd","vcmpneq_oqpd","vcmpneq_ospd","vcmpneq_uspd ","vcmpneqpd","vcmpnge_uqpd","vcmpngepd","vcmpngt_uqpd ","vcmpngtpd","vcmpnle_uqpd","vcmpnlepd","vcmpnlt_uqpd","vcmpnltpd","vcmpord_spd","vcmpordpd","vcmppd","vcmptrue_uspd ","vcmptruepd","vcmpunord_spd","vcmpunordpd","vcmpeq_osps","vcmpeq_uqps","vcmpeq_usps","vcmpeqps","vcmpfalse_osps","vcmpfalseps","vcmpge_oqps","vcmpgeps","vcmpgt_oqps","vcmpgtps","vcmple_oqps","vcmpleps","vcmplt_oqps","vcmpltps","vcmpneq_oqps","vcmpneq_osps","vcmpneq_usps","vcmpneqps","vcmpnge_uqps","vcmpngeps","vcmpngt_uqps","vcmpngtps","vphminposuw","vphsubsw","vphsubw","vphsubd","vpinsrq","vpinsrb","vpinsrw","vpinsrd","vpinsrw","vpmaddubsw","vpmaddwd","vpmaxsw","vpmaxsb","vpmaxsd","vpmaxub","vpmaxud","vpmaxuw","vpminsb","vpminsd","vpminsw","vpminub","vpminud","vpminuw","vpmovmskb","vpmovsxbd","vpmovsxbq","vpmovsxbw","vpmovsxdq","vpmovsxwd","vpmovsxwq","vpmovzxbd","vpmovzxbq","vpmovzxbw","vpmovzxdq","vpmovzxwd","vpmovzxwq","vpmuldq","vpmulhrsw","vpmulhuw","vpmulhw","vpmulld","vpmullw","vpmuludq","vpor","vpshufb","vpshufd","vpshufhw","vpshuflw","vpsignw","vpsignb","vpsignd","vpslldq","vpsllq","vpsllw","vpslld","vpsraw","vpsrad","vpsrlw","vpsrld","vpsrlq","vpsubq","vpsubw","vpsubb","vpsubd","vpsubsw","vpsubsb","vpsubusb","vpsubusw","vptest","vpunpckhbw","vpunpckhdq","vpunpckhqdq","vpunpckhwd","vpunpcklwd","vpunpcklqdq","vpunpckldq","vpunpcklbw","vpxor","vrcpps","vrcpss","vroundpd","vroundps","vroundsd","vroundss","vrsqrtps","vrsqrtss","vshufpd","vshufps","vsqrtpd","vsqrtps","vsqrtsd","vsqrtss","vstmxcsr","vsubpd","vsubps","vsubsd","vsubss","vucomisd","vucomiss","vunpckhpd","vunpckhps","vunpcklpd","vunpcklps","vbroadcastss","vbroadcastsd","vbroadcastf128","vextractf128","vinsertf128","vmaskmovps","vmaskmovpd","vperm2f128","vpermilpd","vpermilps","vtestps","vtestpd","vzeroall","vzeroupper","vxorpd","vxorps ","vpclmulqdq","vbroadcastss","vbroadcastsd","vbroadcastf128","vinsertf128","vextractf128","vmaskmovps","vmaskmovpd","vpermilps","vpermilpd","vperm2f128","vzeroall","vzeroupper"};
  vector<string> tmp9 (avx,avx  + sizeof(avx) / sizeof(avx[0]) );
  instructionSets["AVX"] = tmp9 ;

  string avx2[] = {"vmovntdqa","vmpsadbw","vpabsw","vpabsb","vpabsd","vpackssdw","vpacksswb","vpackusdw","vpackuswb","vpaddq","vpaddw","vpaddb","vpaddd","vpaddsw","vpaddsb","vpaddusw","vpaddusb","vpalignr","vpand","vpandn","vpavgw","vpavgb","vpblendvb","vpblendw","vpcmpeqq","vpcmpeqw","vpcmpeqb","vpcmpeqd","vpcmpgtq","vpcmpgtw","vpcmpgtb","vpcmpgtd","vphaddsw","vphaddw","vphaddd","vphsubsw","vphsubw","vphsubd","vpmaddubsw","vpmaddwd","vpmaxsw","vpmaxsb","vpmaxsd","vpmaxub","vpmaxud","vpmaxuw","vpminsb","vpminsd","vpminsw","vpminub","vpminud","vpminuw","vpmovmskb","vpmovsxbd","vpmovsxbq","vpmovsxbw","vpmovsxdq","vpmovsxwd","vpmovsxwq","vpmovzxbd","vpmovzxbq","vpmovzxbw","vpmovzxdq","vpmovzxwd","vpmovzxwq","vpmuldq","vpmulhrsw","vpmulhuw","vpmulhw","vpmulld","vpmullw","vpmuludq","vpor","vpsadbw","vpshufb","vpshufd","vpshufhw","vpshuflw","vpsignw","vpsignb","vpsignd","vpslldq","vpsllq","vpsllw","vpslld","vpsraw","vpsrad","vpsrldq","vpsrlq","vpsrlw","vpsrld","vpsubq","vpsubw","vpsubb","vpsubd","vpsubsw","vpsubsb","vpsubusw","vpsubusb","vpunpckhbw","vpunpckhdq","vpunpckhqdq","vpunpckhwd","vpunpcklbw","vpunpckldq","vpunpcklqdq","vpunpcklwd","vpxor","vbroadcastsd","vbroadcastss","vextracti128","vgatherdpd","vgatherdps","vgatherqpd","vgatherqps","vinserti128","vpblendd","vpbroadcastq","vpbroadcastw","vpbroadcastb","vpbroadcastd","vbroadcasti128","vperm2i128","vpermd","vpermpd","vpermps","vpermq","vpgatherdd","vpgatherdq","vpgatherqd","vpgatherqq","vpmaskmovq","vpmaskmovd","vpsllvq","vpsllvd","vpsravd","vpsrlvq","vpsrlvd"};
  vector<string> tmp10 (avx2,avx2  + sizeof(avx2) / sizeof(avx2[0]) );
  instructionSets["AVX2"] = tmp10 ;

  string fma[] = {"vfmadd132pdy", "vfmadd132psy", "vfmadd132pdx", "vfmadd132psx", "vfmadd132sd", "vfmadd132ss", "vfmadd213pdy", "vfmadd213psy", "vfmadd213pdx", "vfmadd213psx", "vfmadd213sd", "vfmadd213ss", "vfmadd231pdy", "vfmadd231psy", "vfmadd231pdx", "vfmadd231psx", "vfmadd231sd", "vfmadd231ss"};
  vector<string> tmp11 (fma, fma + sizeof(fma) / sizeof(fma[0]) );
  instructionSets["FMA"] = tmp11 ;
}

/* ===================================================================== */
/* First token of the string                                             */
/* ===================================================================== */
string strFirstToken(const string &text, char sep) {
	size_t start = 0, end = 0;
  if ( (end = text.find(sep, start)) != string::npos ){
    return text.substr(start, end - start);
  }else{
    return text;
  }
}

/* ===================================================================== */
/* Truncate strings                                                      */
/* ===================================================================== */
const char * StripString(const char * str,char delim, bool first){
  const char * token;
  if (first){
    token = strchr(str,delim);
  }else{
    token = strrchr(str,delim);
  }
  if (token)
    return token+1;
  else
    return str;
}

/* ===================================================================== */
/* ParLOT compression: compress the data                                 */
/* ===================================================================== */
static void write(struct ThreadData* const d, const uint2 value)
{
  if (d->tpos == 4) {
    d->tpos = 0;

    uint1* buf = (uint1*)(d->tbuf);
    const uint2 pos = d->cpos;
    d->cpos++;
    uint1 bitpat = 0;
    for (uint1 b = 0; b < 8; b++) {
      const uint1 val = buf[b];
      if (val != 0) {
        bitpat |= 1 << b;
        d->cbuf[d->cpos] = val;
        d->cpos++;
      }
    }
    d->cbuf[pos] = bitpat;

    if (d->cpos > csize - 9) {
      fwrite(d->cbuf, 1, d->cpos, d->file);
      d->cpos = 0;
    }
  }

  d->tbuf[d->tpos] = value;
  d->tpos++;
}

/* ===================================================================== */
/* ParLOT compression: outputs data the data                             */
/* ===================================================================== */
static void output(const THREADID tid, const uint2 value)
{
  struct ThreadData* d = &td[tid];

  if ((d->iscount) && (d->pbuf[d->lpos] == value) && (d->tbuf[d->tpos - 1] < (uint2)65535)) {
    d->lpos = (d->lpos + 1) & psizem1;
    d->tbuf[d->tpos - 1]++;
  } else {
    if (d->iscount) {
      write(d, value);
      d->iscount = false;
    } else {
      d->lpos = d->hbuf[d->hash];
      d->iscount = (d->pbuf[(d->lpos - 3) & psizem1] == d->pbuf[(d->ppos - 3) & psizem1]) &&
                   (d->pbuf[(d->lpos - 2) & psizem1] == d->pbuf[(d->ppos - 2) & psizem1]) &&
                   (d->pbuf[(d->lpos - 1) & psizem1] == d->pbuf[(d->ppos - 1) & psizem1]);
      if (d->iscount) {
        if (d->pbuf[d->lpos] == value) {
          d->lpos = (d->lpos + 1) & psizem1;
          write(d, 1);
        } else {
          write(d, 0);
          write(d, value);
          d->iscount = false;
        }
      } else {
        write(d, value);
      }
    }
  }

  d->hbuf[d->hash] = d->ppos;
  d->hash = ((d->hash << (hsizelg2 / 3)) ^ value) % hsize;
  d->pbuf[d->ppos] = value;
  d->ppos = (d->ppos + 1) & psizem1;
}


/* ===================================================================== */
/* ParLOT compression: Enter the bbl (originally was routine)            */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL enter(THREADID tid, UINT32 x, ADDRINT spval)
{
  uint2 ts = td[tid].tos;
  while ((ts > 1) && (td[tid].sp[ts - 1] <= spval)) {
    ts--;
    output(tid, 0);
  }

  td[tid].sp[ts] = spval;
  td[tid].stack[ts++] = x;
  errorif(ts >= maxstacksize, "stack overflow");
  output(tid, x);
  td[tid].tos = ts;
}

/* ===================================================================== */
/* ParLOT compression: leave the bbl (originally was routine)            */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL leave(THREADID tid, UINT32 x, ADDRINT spval)
{
  uint2 ts = td[tid].tos;
  while ((ts > 1) && (td[tid].sp[ts - 1] <= spval) && (td[tid].stack[ts - 1] != x)) {
    ts--;
    output(tid, 0);
  }
  if ((ts > 1) && (td[tid].sp[ts - 1] <= spval) && (td[tid].stack[ts - 1] == x)) {
    ts--;
    output(tid, 0);
  }
  td[tid].tos = ts;
}

/* ===================================================================== */
/* This function is called before every BBL is executed              */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL docount(UINT64 * counter){
    (*counter)++;
}

/* ===================================================================== */
/* Truncate STRING                                                       */
/* ===================================================================== */
string truncateString(string text, string sep) {
	//printf("Split String...%s\n",text.c_str() );
	vector<string> tokens;
  string ret;
	size_t start = 0, end = 0;
	if ((end = text.find(sep, start)) != string::npos) {
		ret = "_"+(text.substr(end+1));
		//start = end + 1;
	}
  else{
    ret = text;
  }
	//tokens.push_back(text.substr(start));
	return ret;
}


/* ===================================================================== */
/* Trace Instrumentation - Extracting info about BBLs within the trace   */
/*                         only once, and count how many times each BBL  */
/*                         is executed.                                  */
/* ===================================================================== */
VOID Trace(TRACE trace, VOID * val){
  // Trace Entry ID
  static UINT32 eid = 1;
  // Trace ID
  static UINT32 ttid = 0;
  // Aux variables for instruction disassebmly
  char * inst_chr;
  string inst_str;
  string tmp ;
  int i;
  //int statuss;
  //bool isFP = false;
  string lastInstruction = "";
  string infbitset = "";
  vector<string> tempInstruction;
  vector<string>::iterator vit;


  if (!filter.SelectTrace(trace))
    return;
  UINT32 bbid = 0;
  for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)){
    BLK_FP * blkfp = new BLK_FP;
    blkfp->_bid = bbid;
    blkfp->_tid = ttid;
    blkfp->_eid = eid;
    blkfp->_next = blkList;
    blkfp->_blkCount = 0;
    blkList = blkfp;
    RTN rtn = TRACE_Rtn(trace);

    if (RTN_Valid(rtn)){

      blkfp->_rtnName = PIN_UndecorateSymbolName(truncateString((RTN_Name(rtn)),"_Z").c_str(),UNDECORATION_NAME_ONLY );
      //blkfp->_rtnName = RTN_Name(rtn);
      IMG img = SEC_Img(RTN_Sec(rtn));
      if (IMG_Valid(img)){
        blkfp->_imgName = StripString(IMG_Name(img).c_str(),'/',false);

        // Block Header
        //infoFile <<eid << ":" << blkfp->_imgName << ":" << blkfp->_rtnName << ":" << bbid;
        //infoFile <<eid << "|" << blkfp->_imgName << "|" << blkfp->_rtnName  << "|" << bbid << "#" << ttid;
        //insFile  << "+" << eid << "|" << blkfp->_imgName << "|" << blkfp->_rtnName  << "|" << bbid << "#" << ttid;
        infoFile <<eid << "|" << blkfp->_imgName << "|" << blkfp->_rtnName << "\\l" ;
        insFile  << "+" << eid << "|" << blkfp->_imgName << "|" << blkfp->_rtnName << endl ;
        lastInstruction = "";
        infbitset = "";
        tempInstruction.clear();
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)){

          // String operations on instruction disassebmly
          inst_str = strFirstToken(INS_Disassemble(ins),' ');
          inst_chr = new char[inst_str.length() + 1];
          strcpy(inst_chr,inst_str.c_str());
          lastInstruction = inst_str;
          // Write Block Body
          //infoFile << INS_Disassemble(ins) << endl;
          tempInstruction.push_back(INS_Disassemble(ins));
          // Set the FPbitset of current block
          i = 0;
          for (v_str_it = insetKeys.begin() ; v_str_it != insetKeys.end() ; v_str_it++){
            for (v_str_it2 = instructionSets[*v_str_it].begin() ; v_str_it2 != instructionSets[*v_str_it].end() ; v_str_it2++){
              tmp = *v_str_it2;
              //outFile2 << tmp << "::" << instName << endl;
              if (strcmp(inst_chr,tmp.c_str()) == 0){
                //isFP = true;
                blkfp->_insBitSet[i]++;
              }
            }
            i++;
          }
        }
        for(int jj = 0 ; jj < BITSETSIZE ; jj++){
          infbitset = infbitset + decstr(blkfp->_insBitSet[jj]) + ".";
        }
        //infoFile << "|" << lastInstruction << "|" << infbitset << endl;
        insFile << "|" << lastInstruction << "|" << infbitset << endl;
        for(vit = tempInstruction.begin(); vit != tempInstruction.end() ; vit++){
          insFile << (*vit) << endl;
          infoFile << (*vit) << "\\l";
        }
        infoFile << endl;
        //if (isFP){
          BBL_InsertCall(bbl, IPOINT_ANYWHERE, (AFUNPTR)docount,IARG_FAST_ANALYSIS_CALL,IARG_PTR, &(blkfp->_blkCount), IARG_END);
          BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)enter, IARG_FAST_ANALYSIS_CALL, IARG_THREAD_ID, IARG_UINT32, eid, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
          BBL_InsertCall(bbl, IPOINT_ANYWHERE, (AFUNPTR)leave, IARG_FAST_ANALYSIS_CALL, IARG_THREAD_ID, IARG_UINT32, eid, IARG_REG_VALUE, REG_STACK_PTR, IARG_END);
        //  isFP = false;
      //  }
        bbid++;
        eid++;
      }
    }
  }
  ttid++;
}


VOID ThreadStart(THREADID tid, CONTEXT* ctxt, INT32 flags, VOID* v)
{
  errorif(tid >= maxthreads, "too many threads");
  string filename = fnameprefix + decstr(tid);

  td[tid].iscount = false;
  td[tid].ppos = 0;
  td[tid].lpos = 0;
  td[tid].hash = 0;
  td[tid].tpos = 0;
  td[tid].cpos = 0;
  td[tid].tos = 1;
  td[tid].file = fopen(filename.c_str(), "wb");
  errorif(td[tid].file == NULL, "could not open output file");
  td[tid].stack[0] = 0;
  td[tid].sp[0] = 0;
  memset(td[tid].hbuf, 0, hsize * sizeof(td[tid].hbuf[0]));
  memset(td[tid].pbuf, 0, psize * sizeof(td[tid].pbuf[0]));
}

VOID ThreadFini(THREADID tid, const CONTEXT* ctxt, INT32 code, VOID* v)
{
  struct ThreadData* d = &td[tid];

  if (d->tpos > 0) {
    uint1* buf = (uint1*)(d->tbuf);
    const uint2 pos = d->cpos;
    d->cpos++;
    uint1 bitpat = 0;
    for (uint1 b = 0; b < d->tpos * 2; b++) {
      const uint1 val = buf[b];
      if (val != 0) {
        bitpat |= 1 << b;
        d->cbuf[d->cpos] = val;
        d->cpos++;
      }
    }
    d->cbuf[pos] = bitpat;
  }
  d->cbuf[d->cpos] = d->tpos;
  d->cpos++;

  fwrite(d->cbuf, 1, d->cpos, d->file);
  fclose(d->file);
  d->file = NULL;
}


VOID Fini2(INT32 code, VOID *v)
{
  int sum = 0;
  string infbitset = "";
  for (BLK_FP * bc = blkList; bc; bc = bc->_next)
  {
    sum = 0;
    if (bc -> _blkCount > 0){
      for ( int k = 0 ; k < BITSETSIZE ; k++){
        sum += bc->_insBitSet[k];
      }
      if (sum != 0){
        infbitset = "";
        outFile << bc->_rtnName << "|" << bc->_imgName << "|" << bc->_eid << "|" << bc->_blkCount << "|";
        for(int jj = 0 ; jj < BITSETSIZE ; jj++){
          infbitset = infbitset + decstr(bc->_insBitSet[jj]) + ".";
        }
        outFile << infbitset << endl;
      }
    }
  }
  outFile.close();
  infoFile.close();
}

VOID Fini(INT32 code, VOID *v)
{
  int sum = 0;
  outFile << setw(80) << "Func" << " "
    << setw(15) << "Img" << " "
    << setw(5) << "TID" << " "
    << setw(3) << "BID" << " "
    << setw(10) << "BCNT" << " "
    << setw(3) << "SSD" << " "
    << setw(3) << "SSA" << " "
    << setw(3) << "SSO" << " "
    << setw(3) << "S2D" << " "
    << setw(3) << "S2A" << " "
    << setw(3) << "S2O" << " "
    << setw(3) << "FPD" << " "
    << setw(3) << "FPA" << " "
    << setw(3) << "FPO" << " "
    << setw(3) << "AVX" << " "
    << setw(3) << "AV2"  << " "
    << setw(3) << "FMA" << endl;

  for (BLK_FP * bc = blkList; bc; bc = bc->_next)
  {
    sum = 0;
    if (bc -> _blkCount > 0){
      for ( int k = 0 ; k < BITSETSIZE ; k++){
        sum += bc->_insBitSet[k];
      }
      if (sum != 0){
        outFile << setw(80) << bc->_rtnName << " "
          << setw(15) << bc->_imgName << " "
          << setw(5) << bc->_tid << " "
          << setw(3) << bc->_bid << " "
          << setw(10) << bc->_blkCount << " "
          << setw(3) << bc->_insBitSet[0] << " "
          << setw(3) << bc->_insBitSet[1] << " "
          << setw(3) << bc->_insBitSet[2] << " "
          << setw(3) << bc->_insBitSet[3] << " "
          << setw(3) << bc->_insBitSet[4] << " "
          << setw(3) << bc->_insBitSet[5] << " "
          << setw(3) << bc->_insBitSet[6] << " "
          << setw(3) << bc->_insBitSet[7] << " "
          << setw(3) << bc->_insBitSet[8] << " "
          << setw(3) << bc->_insBitSet[9] << " "
          << setw(3) << bc->_insBitSet[10] << " "
          << setw(3) << bc->_insBitSet[11] << endl;
      }
    }
  }
  outFile.close();
  infoFile.close();
}


int main(int argc, char* argv[])
{
  insetsInit();
  PIN_InitSymbols();
  errorif(PIN_Init(argc, argv), "pin failed to initialize");
  outFile.open((KnobOutputFile.Value()+".out").c_str());
  infoFile.open((KnobOutputFile.Value()+".info").c_str());
  insFile.open((KnobOutputFile.Value()+".ins").c_str());
  fnameprefix = KnobOutputFile.Value()+".";
  //trc.open((KnobOutputFile.Value()+".trc").c_str());

  PIN_AddThreadStartFunction(ThreadStart, 0);
  PIN_AddThreadFiniFunction(ThreadFini, 0);

  TRACE_AddInstrumentFunction(Trace, 0);

  filter.Activate();

  // Register Fini to be called when the application exits
  //PIN_AddFiniFunction(Fini, 0);
  PIN_AddFiniFunction(Fini2, 0);

  PIN_StartProgram();
  return 0;
}
