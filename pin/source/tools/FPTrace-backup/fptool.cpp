/*
 * Description: Pin tool for FP debugging purposes.
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
*/


#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <unistd.h>
#include "pin.H"
#include "portability.H"

#define version 17


static FILE* outFile = NULL;
static string outFileName;
#define errorif(cond, msg) if (cond) {fprintf(stderr, "ERROR: %s\n", msg); exit(-1);}


VOID Routine(RTN rtn, VOID* v)
{
  const char* rtnname = RTN_Name(rtn).c_str();
  const string img = IMG_Name(SEC_Img(RTN_Sec(rtn)));
  fprintf (outFile,"%s::%s\n", img.c_str(),rtnname);
  RTN_Open(rtn);
  for (INS ins = RTN_InsHead(rtn); INS_Valid(ins); ins = INS_Next(ins)){
    fprintf (outFile,"-%s\n", INS_Disassemble(ins).c_str());
  }
  RTN_Close(rtn);
}



int main(int argc, char* argv[])
{
  printf("FPTrace, University of Utah");
  
  outFileName = "instructions.txt";
  outFile = fopen(outFileName.c_str(), "wt");

  errorif(outFile == NULL, "could not open instName file");

  PIN_InitSymbols();
  errorif(PIN_Init(argc, argv), "pin failed to initialize");
  RTN_AddInstrumentFunction(Routine, 0);

  PIN_StartProgram();
  return 0;
}
