/*
* FPT_staticBlock by Saeed Taheri, University of Utah 2019
*/

#include <cstdlib>
#include <cstdio>
#include <string.h>
#include <string>
#include <unistd.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include "pin.H"
#include "portability.H"
#include <bitset>
#include "instlib.H"

using namespace INSTLIB;
using namespace std;

// Contains knobs to filter out things to instrument
FILTER filter;

typedef unsigned char uint1;
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long uint8;

static const uint2 maxstacksize = 4096;

KNOB<string> KnobOutputFile(KNOB_MODE_WRITEONCE, "pintool",
    "o", "out", "specify output file name");

ofstream outFile;
ofstream info;
ofstream trc;

static const string arr[] = {"FMA","SSE_DATA","SSE_ARITH","SSE_COMP_LOGIC","FP_DATA","FP_ARITH","FP_COMP","FP_CONTROL"};
static vector<string> insetKeys (arr, arr + sizeof(arr) / sizeof(arr[0]) );
static vector<string>::iterator v_str_it;
static vector<string>::iterator v_str_it2;
static vector<string> seenRTN;

static map< string , vector <string> > instructionSets;
static map<string,vector<string>>::iterator m_str_it;


#define Q(x) #x
#define QUOTE(x) Q(x)
#define errorif(cond, msg) if (cond) {fprintf(stderr, "ERROR: %s\n", msg); exit(-1);}
#define BITSETSIZE 8

/* ********************************************************************* */
/* Hold instruction FP information for a single procedure                */
/* ********************************************************************* */
typedef struct blkFP
{
  UINT32 _tid;
  UINT32 _bid;
  string _rtnName;
  string _imgName;
  UINT64 _blkCount;
  int _insBitSet[BITSETSIZE] {0,0,0,0,0,0,0,0};
  struct blkFP * _next;
} BLK_FP;


// Linked List of FP Info for each routine
BLK_FP * blkList = 0;

VOID insetsInit(){
  //string fma[] = {"VFMADD132PDy","VFMADD132PSy","VFMADD132PDx","VFMADD132PSx","VFMADD132SD","VFMADD132SS","VFMADD213PDy","VFMADD213PSy","VFMADD213PDx","VFMADD213PSx","VFMADD213SD","VFMADD213SS","VFMADD231PDy","VFMADD231PSy","VFMADD231PDx","VFMADD231PSx","VFMADD231SD","VFMADD231SS"};
  string fma[] = {"vfmadd132pdy", "vfmadd132psy", "vfmadd132pdx", "vfmadd132psx", "vfmadd132sd", "vfmadd132ss", "vfmadd213pdy", "vfmadd213psy", "vfmadd213pdx", "vfmadd213psx", "vfmadd213sd", "vfmadd213ss", "vfmadd231pdy", "vfmadd231psy", "vfmadd231pdx", "vfmadd231psx", "vfmadd231sd", "vfmadd231ss"};
  vector<string> tmp1 (fma, fma + sizeof(fma) / sizeof(fma[0]) );
  instructionSets["FMA"] = tmp1 ;

  //string sse_data[] = {"MOVSS","MOVAPS","MOVUPS","MOVLPS","MOVHPS","MOVLHPS","MOVHLPS","MOVMSKPS","SHUFPS", "UNPCKHPS", "UNPCKLPS", "CVTSI2SS", "CVTSS2SI", "CVTTSS2SI","CVTPI2PS", "CVTPS2PI", "CVTTPS2PI"};
  string sse_data[] = {"movss", "movaps", "movups", "movlps", "movhps", "movlhps", "movhlps", "movmskps", "shufps", "unpckhps", "unpcklps", "cvtsi2ss", "cvtss2si", "cvttss2si", "cvtpi2ps", "cvtps2pi", "cvttps2pi"};
  vector<string> tmp2 (sse_data, sse_data + sizeof(sse_data) / sizeof(sse_data[0]) );
  instructionSets["SSE_DATA"] = tmp2 ;

  //string sse_arith[] ={"ADDSS","SUBSS","MULSS","DIVSS","RCPSS","SQRTSS","MAXSS","MINSS","RSQRTSS","ADDPS","SUBPS","MULPS","DIVPS","RCPPS","SQRTPS","MAXPS","MINPS","RSQRTPS"} ;
  string sse_arith[] ={"addss", "subss", "mulss", "divss", "rcpss", "sqrtss", "maxss", "minss", "rsqrtss", "addps", "subps", "mulps", "divps", "rcpps", "sqrtps", "maxps", "minps", "rsqrtps"};
  vector<string> tmp3 (sse_arith, sse_arith + sizeof(sse_arith) / sizeof(sse_arith[0]) );
  instructionSets["SSE_ARITH"] = tmp3 ;

  //string sse_comp_logic[] = {"CMPSS","COMISS","UCOMISS","ANDPS","ORPS","XORPS","ANDNPS"};
  string sse_comp_logic[] = {"cmpss", "comiss", "ucomiss", "andps", "orps", "xorps", "andnps"};
  vector<string> tmp4 (sse_comp_logic, sse_comp_logic + sizeof(sse_comp_logic) / sizeof(sse_comp_logic[0]) );
  instructionSets["SSE_COMP_LOGIC"] = tmp4 ;

  //string fp_data[] = {"FBLD","FBSTP","FCMOVB","FCMOVBE","FCMOVE","FCMOVNB","FCMOVNBE","FCMOVNE","FCMOVNU","FCMOVU","FILD","FIST","FISTP","FLD","FST","FSTP","FXCH","FLD1","FLDL2E","FLDL2T","FLDLG2","FLDLN2","FLDPI","FLDZ"};
  string fp_data[] = {"fbld", "fbstp", "fcmovb", "fcmovbe", "fcmove", "fcmovnb", "fcmovnbe", "fcmovne", "fcmovnu", "fcmovu", "fild", "fist", "fistp", "fld", "fst", "fstp", "fxch", "fld1", "fldl2e", "fldl2t", "fldlg2", "fldln2", "fldpi", "fldz"};
  vector<string> tmp5 (fp_data,fp_data  + sizeof(fp_data) / sizeof(fp_data[0]) );
  instructionSets["FP_DATA"] = tmp5 ;

  //string fp_arith[] = {"FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","F2XM1","FCOS","FPATAN","FPTAN","FSIN","FSINCOS","FYL2X","FYL2XP1"};
  string fp_arith[] = {"fabs", "fadd", "faddp", "fchs", "fdiv", "fdivp", "fdivr", "fdivrp", "fiadd", "fidiv", "fidivr", "fimul", "fisub", "fisubr", "fmul", "fmulp", "fprem", "fprem1", "frndint", "fscale", "fsqrt", "fsub", "fsubp", "fsubr", "fsubrp", "fxtract", "f2xm1", "fcos", "fpatan", "fptan", "fsin", "fsincos", "fyl2x", "fyl2xp1"};
  vector<string> tmp6 (fp_arith,fp_arith  + sizeof(fp_arith) / sizeof(fp_arith[0]) );
  instructionSets["FP_ARITH"] = tmp6 ;

  //string fp_comp[] = {"FCOM","FCOMI","FCOMIP","FCOMP","FCOMPP","FICOM","FICOMP","FTST","FUCOM","FUCOMI","FUCOMIP","FUCOMP","FUCOMPP","FXAM"};
  string fp_comp[] = {"fcom", "fcomi", "fcomip", "fcomp", "fcompp", "ficom", "ficomp", "ftst", "fucom", "fucomi", "fucomip", "fucomp", "fucompp", "fxam"};
  vector<string> tmp7 (fp_comp,fp_comp  + sizeof(fp_comp) / sizeof(fp_comp[0]) );
  instructionSets["FP_COMP"] = tmp7 ;

  //string fp_control[] = {"FCLEX","FDECSTP","FFREE","FINCSTP","FINIT","FLDCW","FLDENV","FNCLEX","FNINIT","FNOP","FNSAVE","FNSTCW","FNSTENV","FNSTSW","FRSTOR","FSAVE","FSTCW","FSTENV","FSTSW","FWAIT","WAIT"};
  string fp_control[] = {"fclex", "fdecstp", "ffree", "fincstp", "finit", "fldcw", "fldenv", "fnclex", "fninit", "fnop", "fnsave", "fnstcw", "fnstenv", "fnstsw", "frstor", "fsave", "fstcw", "fstenv", "fstsw", "fwait", "wait"};
  vector<string> tmp8 (fp_control,fp_control  + sizeof(fp_control) / sizeof(fp_control[0]) );
  instructionSets["FP_CONTROL"] = tmp8 ;

}


string strFirstToken(const string &text, char sep) {
	size_t start = 0, end = 0;
  if ( (end = text.find(sep, start)) != string::npos ){
    return text.substr(start, end - start);
  }else{
    return text;
  }
}

/* ===================================================================== */
/* Truncate strings                                                      */
/* ===================================================================== */
const char * StripString(const char * str,char delim, bool first){
  const char * token;
  if (first){
    token = strchr(str,delim);
  }else{
    token = strrchr(str,delim);
  }
  if (token)
    return token+1;
  else
    return str;
}

/* ===================================================================== */
/* This function is called before every BBL is executed              */
/* ===================================================================== */
VOID PIN_FAST_ANALYSIS_CALL docount(UINT32 eid, UINT64 * counter){
  trc << eid << endl;
    (*counter)++;
}

/* ===================================================================== */
/* Trace Instrumentation - Extracting info about BBLs within the trace   */
/*                         only once, and count how many times each BBL  */
/*                         is executed.                                  */
/* ===================================================================== */
VOID Trace(TRACE trace, VOID * val){
  // Trace Entry ID
  static UINT32 eid = 0;
  // Trace ID
  static UINT32 ttid = 0;
  // Aux variables for instruction disassebmly
  char * inst_chr;
  string inst_str;
  string tmp ;
  int i;

  if (!filter.SelectTrace(trace))
    return;
  UINT32 bbid = 0;
  for (BBL bbl = TRACE_BblHead(trace); BBL_Valid(bbl); bbl = BBL_Next(bbl)){
    BLK_FP * blkfp = new BLK_FP;
    blkfp->_bid = bbid;
    blkfp->_tid = ttid;
    blkfp->_next = blkList;
    blkfp->_blkCount = 0;
    blkList = blkfp;
    RTN rtn = TRACE_Rtn(trace);
    if (RTN_Valid(rtn)){
      blkfp->_rtnName = RTN_Name(rtn);
      IMG img = SEC_Img(RTN_Sec(rtn));
      if (IMG_Valid(img)){
        blkfp->_imgName = StripString(IMG_Name(img).c_str(),'/',false);

        // Block Header
        info <<"+" <<eid << ":" << blkfp->_imgName << ":" << blkfp->_rtnName << ":" << ttid << ":" << bbid << endl;
        for (INS ins = BBL_InsHead(bbl); INS_Valid(ins); ins = INS_Next(ins)){

          // String operations on instruction disassebmly
          inst_str = strFirstToken(INS_Disassemble(ins),' ');
          inst_chr = new char[inst_str.length() + 1];
          strcpy(inst_chr,inst_str.c_str());

          // Write Block Body
          info << INS_Disassemble(ins) << endl;
          // Set the FPbitset of current block
          i = 0;
          for (v_str_it = insetKeys.begin() ; v_str_it != insetKeys.end() ; v_str_it++){
            for (v_str_it2 = instructionSets[*v_str_it].begin() ; v_str_it2 != instructionSets[*v_str_it].end() ; v_str_it2++){
              tmp = *v_str_it2;
              //outFile2 << tmp << "::" << instName << endl;
              if (strcmp(inst_chr,tmp.c_str()) == 0){
                blkfp->_insBitSet[i]++;
              }
            }
            i++;
          }
        }
        BBL_InsertCall(bbl, IPOINT_BEFORE, (AFUNPTR)docount,IARG_FAST_ANALYSIS_CALL,IARG_UINT32, eid, IARG_PTR, &(blkfp->_blkCount), IARG_END);
        bbid++;
        eid++;
      }
    }
  }
  ttid++;
}


VOID Fini(INT32 code, VOID *v)
{
  int sum = 0;
  outFile << setw(80) << "Func" << " "
    << setw(15) << "Img" << " "
    << setw(5) << "TID" << " "
    << setw(3) << "BID" << " "
    << setw(10) << "BCNT" << " "
    << setw(3) << "FMA"  << " "
    << setw(3) << "SSD" << " "
    << setw(3) << "SSA" << " "
    << setw(3) << "SSC" << " "
    << setw(3) << "FPD" << " "
    << setw(3) << "FPA" << " "
    << setw(3) << "FPC" << " "
    << setw(3) << "FPN" << endl;

  for (BLK_FP * bc = blkList; bc; bc = bc->_next)
  {
    sum = 0;
    if (bc -> _blkCount > 0){
      for ( int k = 0 ; k < 8 ; k++){
        sum += bc->_insBitSet[k];
      }
      if (sum != 0){
        outFile << setw(80) << bc->_rtnName << " "
          << setw(15) << bc->_imgName << " "
          << setw(5) << bc->_tid << " "
          << setw(3) << bc->_bid << " "
          << setw(10) << bc->_blkCount << " "
          << setw(3) << bc->_insBitSet[0] << " "
          << setw(3) << bc->_insBitSet[1] << " "
          << setw(3) << bc->_insBitSet[2] << " "
          << setw(3) << bc->_insBitSet[3] << " "
          << setw(3) << bc->_insBitSet[4] << " "
          << setw(3) << bc->_insBitSet[5] << " "
          << setw(3) << bc->_insBitSet[6] << " "
          << setw(3) << bc->_insBitSet[7] << endl;
      }
    }
  }
  outFile.close();
  trc.close();
  info.close();
}

int main(int argc, char* argv[])
{
  insetsInit();
  PIN_InitSymbols();
  errorif(PIN_Init(argc, argv), "pin failed to initialize");
  outFile.open((KnobOutputFile.Value()+".out").c_str());
  info.open((KnobOutputFile.Value()+".info").c_str());
  trc.open((KnobOutputFile.Value()+".trc").c_str());
  TRACE_AddInstrumentFunction(Trace, 0);

  filter.Activate();

  // Register Fini to be called when the application exits
  PIN_AddFiniFunction(Fini, 0);

  PIN_StartProgram();
  return 0;
}
