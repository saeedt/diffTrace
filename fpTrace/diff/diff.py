#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
# Code: diff.py
# Description: Generates diffTrace.dot and diffTrace.pdf of pairs of text(trace) files
# Input: Path with all target TXT files in it
# Output: Set of .dot and .pdf of diffTrace(i,j) within input path

import glob
import sys,subprocess
import diffCore as diff
import newVis as nvis
import mrrCore as mrr
import argparse
import toml
import cxxfilt

# To store configuration file entries
conf = {}

parser = argparse.ArgumentParser(description='Filter FPTrace files and generate diff dot file.')
parser.add_argument('file1',help='File f1 in diff(f1,f2)')
parser.add_argument('file2',help='File f2 in diff(f1,f2)')
parser.add_argument('--mode',required=True, choices = ['only_routines','specific_routine','general'],default='general',help='Routines_only: Only show routines (skip instructions), specific_routine: Only show instructions of particular routine (requires --routine option), General: Show everything. Default: general')
parser.add_argument('--out',default='f1_f2',help='Output file name. Default: out.dot, out.pdf')
parser.add_argument('--filt_branches', action='store_true',help='Filter out call or jump instructions e.g. jmp jz jnle etc.')
parser.add_argument('--include_addresses', action='store_true',help='Include address of instructions')
parser.add_argument('--include_instInputs', action='store_true',help='Include instruction inputs')
parser.add_argument('--instruction_set',required=True,choices = ['all','FMA','SSEarith','SSEall','FPall','FParith','Custome'],default='all',help='Only look at instructions from these instruction sets (insets.toml). Default: all')
parser.add_argument('--routine',type=open, nargs='+',help='Only look at instructions of this particular routine. Rotunes should be in form of functionName(int,float*)')
parser.add_argument('--configFile',required=True,help='Config file of set of instructions')



args = parser.parse_args()
print args

class entry:
	def __init__(self,i,line):
		self.id = i
		ll = line.split("::")
		self.address = ll[0]
		self.lib = ll[1]
		self.routine = ll[2]
		self.instruction = ll[3].partition(" ")[0]
		self.instruction_Inputs = ll[3].partition(" ")[2]

def genEntryObjects(lines):
	l = []
	for i in range(0,len(lines)):
		tmp = entry(i,lines[i])
		l.append(tmp)
	return l

def dmngl(s):
	ss = str(cxxfilt.demangle(s.partition("@")[0]))
	ret = ""
	for i in ss:
		if i == ",":
			ret = ret + "-"
		else:
			ret = ret + i
	return ret

def instFilters(args,entries):
	ret = []
	if args.filt_branches:
		for ent in entries:
			filts1 = [str(x).lower() for x in conf["insets"]["jumps"]["instructions"]]
			filts2 = [str(x).lower() for x in conf["insets"]["invoks"]["instructions"]]
			if ent.instruction not in filts1 and ent.instruction not in filts2:
				ret.append(ent)
	else:
		ret = entries

	nen = ret
	ret = []
	if args.instruction_set and args.instruction_set != "all":
		for ent in nen:
			if ent.instruction in conf["insets"][args.instruction_set]["instructions"]:
				ret.append(ent)
	else:
		ret = nen
	return ret


def viewFilters(args,entries):
	ret = []
	seenRtn = []
	for ent in entries:
		libRtn = "+"+dmngl(ent.routine)
		if libRtn not in seenRtn:
			ret.append(libRtn)
			seenRtn.append(libRtn)
		if args.include_addresses or args.include_instInputs:
			print "NEED TO ADD FUNCTIONALITY OF THIS"
			sys.exit(-1)
		else:
			ret.append(ent.instruction)
	return ret


def processArgs(args):
	ret = {}
	ret[1] = []
	ret[2] = []
	f1lines = open(args.file1).readlines()
	f2lines = open(args.file2).readlines()
	f1entries = genEntryObjects(f1lines)
	f2entries = genEntryObjects(f2lines)

	# Routine Only
	if args.mode == 'only_routines':
		for ent in f1entries:
			demangled = dmngl(ent.routine)
			tmp = (ent.lib).rpartition("/")[2]+"::"+demangled
			if tmp not in ret[1]:
				ret[1].append(tmp)
		for ent in f2entries:
			demangled = dmngl(ent.routine)
			tmp = (ent.lib).rpartition("/")[2]+"::"+demangled
			if tmp not in ret[2]:
				ret[2].append(tmp)
		return ret
	elif args.mode == "general":
		ret[1]=viewFilters(args,instFilters(args,f1entries))
		ret[2]=viewFilters(args,instFilters(args,f2entries))
		return ret
	elif args.mode == "specific_routine":
		if not args.routine:
			print "Error: You must specify the routine with --routine <functionName>(<arg1_type>*,<arg2_type>*,....)\n"
			sys.exit(-1)
		process = subprocess.Popen("$DIFF_ROOT/FPTrace/scripts/mangle.bash \" "+args.routine+"\"", stdout=subprocess.PIPE,shell=True)
		s, err = process.communicate()
		ret[1]=viewFilters(args,instFilters(args,[x for x in f1entries if x.routine == s]))
		ret[2]=viewFilters(args,instFilters(args,[x for x in f2entries if x.routine == s]))
		return ret



config_file=open(args.configFile)
conf=toml.loads(config_file.read())

print args
targets = processArgs(args)

for i in range(0,len(targets)):
	for j in range(i+1,len(targets)):
		#nm = lof[i].rpartition("/")[2].rpartition(".")[0]+"_"+lof[j].rpartition("/")[2].rpartition(".")[0]
		#print nm
		nm = args.out
		#fe = diff.lcs([x.strip() for x in open(lof[i],"r").readlines() if len(x) > 0],[x.strip() for x in open(lof[j],"r").readlines() if len(x) > 0])
		mrrt1 = mrr.toMRR(targets[1])
		mrrt2 = mrr.toMRR(targets[2])
		print mrrt1
		print mrrt2
		#input()
		#fe = diff.lcs(targets[1],targets[2])
		fe = diff.lcs(mrrt1,mrrt2)
		print fe
		dt = nvis.edit2dot(fe,nm,1)
		f = open(nm+".dot","w")
		f.write(dt)
		f.close()
		process = subprocess.Popen("dot -Tpdf "+nm+".dot -o "+nm+".pdf", stdout=subprocess.PIPE,shell=True)
		si, err = process.communicate()