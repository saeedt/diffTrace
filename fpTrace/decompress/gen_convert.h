/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: gen_convert.h
 * Description: Part of traceToText decompression application
 */
#ifndef GEN_CONVERT_H
#define GEN_CONVERT_H


#include <vector>
#include <set>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
//#include <fstream>
#include <sstream>
#include <iterator>
#include <dirent.h>
#include <unistd.h>
//#include <iostream>
#include <sstream>
#include <iostream>
#include <fstream>

#include "trc_decomp.h"
#include "gen_convert.h"



#include <iterator>


using namespace std;

//GLOBAL VARIABLES




class Concept;
class Trace;
template <class T> class Attribute;


 

/**
 * Converts Trace to Concept.
 * It takes the path of traces and after decompressing traces, it creates 
 * Objects and Attributes from traces to construct a Concept
 */
Concept* traceToConcept(string tracePath, string path, int outMode);

/**
 * Prints Attribute and Object tables.
 */
string ttab2string();




/**
 * Takes a string and returns a vector of strings which is the splitter based on char 'sep' as seperator
 */
vector<string> splitString(string text, char sep);

/**
 * Pairing each trace file with its corresponding info file.
 */
pair<string,string> infoPair(const string info,const string path);

/**
 * Returns a vector of all files within path with ext extension
 */
vector<string> listOfFiles(const string path,const char* ext);

/**
 * Returns a vector of all folders within path with ext extension
 */
vector<string> listOfFolders(const string path);

/**
 * Prints help message
 */
void printUsage(void);

/**
 * Extract TID from a extractor
 */
string tidExtractor(string);

/**
 * Summarize long range of ints (1,2,3,...,n) to (1-n)
 */
string setSummary(set<int>,int flag);

/**
 * Converts integer to string
 */
string intToString(int i);

/**
 * Add new trace to Trace map
 */
void addToTraceMap(Trace& trc);

/**
 * Add new attribute<string> to Attribute<string> map
 */
void addTostringAttrMap(Attribute<string>& attr);

/**
 * Add new attribute<int> to Attribute<int> map
 */
void addTointAttrMap(Attribute<int>& attr);








#endif





