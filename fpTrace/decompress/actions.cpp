/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: actions.cpp
 * Description: Part of traceToText decompression application
 */
#include "actions.h"

void actionM4(string _trace, string _info, string _outPath){
  static const uint2 maxstacksize = 4096;
	///////////////////////////////
	// Tokenizing trace and info
	///////////////////////////////
	string traceToken;
	string infoToken;

	traceToken = splitString(_trace, '/').back() ;
	infoToken = splitString(_info, '/').back() ;

	string infoPath = _outPath+"/"+infoToken+".txt";
	string tracePath = _outPath+"/"+traceToken+".txt";

	printf("tracePath : %s\n", tracePath.c_str());
	printf("infoPath : %s\n", infoPath.c_str());

  uint2 stack[maxstacksize], top = 1, max = 1;
  int sinfo[60];
  for (int i = 0 ; i < 60 ; i++){
    sinfo[i] = 0;
  }

  int callCount = 0;
  int retCount = 0;

	string* info;
	uint2* data ;
	uint8 length;

	info =readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);

  stack[0] = 0;
  for (uint8 i = 0; i < length; i++) {
    uint2 num = data[i];
    if (num > 0) {
      stack[top++] = num;
      assert(top < maxstacksize);
      sinfo[top]++;
      callCount++;
      if (max < top) max = top;
    } else {
      retCount++;
      top--;
      assert(top > 0);
    }
  }
  printf("Length: %d\nCall Count: %d\nReturn Count: %d\nMax Stack: %d\n",length,callCount,retCount,max);
  for (int i = 0 ; i < 60 ; i++){
    printf("sinfo[%d]=%d\n",i,sinfo[i]);
  }
}



void actionM3(string _trace, string _info, string _outPath){



	///////////////////////////////
	// Tokenizing trace and info
	///////////////////////////////
	string traceToken;
	string infoToken;

	traceToken = splitString(_trace, '/').back() ;
	infoToken = splitString(_info, '/').back() ;
	//printf("traceToken : %s\n", traceToken.c_str());
	//printf("infoToken : %s\n", infoToken.c_str());



	string infoPath = _outPath+"/"+infoToken+".txt";
	string tracePath = _outPath+"/"+traceToken+".txt";

	printf("tracePath : %s\n", tracePath.c_str());
	printf("infoPath : %s\n", infoPath.c_str());

	string* info;
	uint2* data ;
	uint8 length;

	info=readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);

	ofstream fdata;
	fdata.open(tracePath);
	for (uint8 i = 0 ; i < length ; i++){
    if (data[i] != 0)
		  fdata << data[i] << "," ;
	}
	fdata.close();


	ofstream finfo;
	finfo.open(infoPath);
	for (uint4 i = 0 ; i < 65536 ; i++){
		finfo << info[i] << "," ;
	}
	finfo.close();
	//primProcess(data, length, info, _outMode,action.c_str());
}


void actionM2(string _trace, string _info, int _outMode, string _outName){

	string action = "singleDecomp."+_outName;

	string* info;
	uint2* data ;
	uint8 length;

	info=readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);
	primProcess(data, length, info, _outMode,action.c_str());


}
