/**
 * Authors: Saeed Taheri, University of Utah, staheri@cs.utah.edu,
 * (decompression : Martin Burtscher, Texas State University, burtscher@cs.txstate.edu
 * Sindhu Devale, Texas State University)
 * 2018, All rights reserved
 * Code: trc_decomp.cpp
 * Description: Decompress and convert trace files to text files
 */
 
#ifndef TRC_DECOMP_H
#define TRC_DECOMP_H

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <utility>
#include <cxxabi.h>
#include <vector>
#include <map>
#include <vector>
#include <set>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iterator>
#include <iomanip>
 
using namespace std;

#define version 17
#define Q(x) #x
#define QUOTE(x) Q(x)




extern long hnum;

typedef unsigned char uint1; 
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long long uint8;

/**
 * Decompress data from traces and return a map<k,v> where k is each entry within the trace (based on mode) and v is its frequency
 */
map<string,int> process(const uint2 data[], const uint8 length, string info[], const int mode);

/**
 * Primary: Decompress data from traces and write the output to file (out) based on mode
 */
void primProcess(const uint2 data[], const uint8 length, string info[], const int mode,const char out[]);

/**
 * Read trace files to decompress the byte codes 
 */
uint2* readFile(const char name[], uint8& length);

/**
 * Read info file to decompress traces
 */
string* readInfo(const char name[]) ;
#endif
