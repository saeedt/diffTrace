/**
 * Authors: Saeed Taheri, University of Utah, staheri@cs.utah.edu,
 * (decompression : Martin Burtscher, Texas State University, burtscher@cs.txstate.edu
 * Sindhu Devale, Texas State University)
 * 2018, All rights reserved
 * Code: trc_decomp.cpp
 * Description: Decompress and convert trace files to text files
 */
#include "trc_decomp.h"

using namespace std;

#define INFSIZE 65536
//////////////////////////////////////////////////////////////////////////////
// traceReader(): Decompress, read info, read file, process
//////////////////////////////////////////////////////////////////////////////


static const uint2 maxstacksize = 4096;
static const uint1 hsizelg2 = 12;
static const uint2 hsize = 1 << hsizelg2;
static const uint1 psizelg2 = 16;
static const uint4 psize = 1 << psizelg2;
static const uint2 psizem1 = psize - 1;

uint2 hbuf[hsize];
uint2 pbuf[psize];


/**
 * Process the trace files
 *
 * Input: decompressed data, info, length of data, mode
 * Output: Depending on the mode a set of all either <freq,function calls> or  <freq,function edges>
 */

map<string,int> process(const uint2 data[], const uint8 length, string info[], const int mode){
	map<string,int> retDS;
	string tmp;
	//set<string> functions;

	info[0] = "[ret]";

	uint8 enter[INFSIZE];
	for (uint4 i = 0; i < INFSIZE; i++) {
		enter[i] = 0;
	}

	for (uint8 i = 0; i < length; i++) {
		enter[data[i]]++;
	}

	if (mode == 1){
		for (uint4 i = 1; i < INFSIZE; i++) {
			if (enter[i] != 0) {
				printf("FS------>%d,%s \n",enter[i], info[i].c_str());
				retDS[info[i]] = enter[i];
			}
		}
	}
	else{
		map<pair<uint2, uint2>, uint8> m;
		uint2 stack[maxstacksize], top = 1, max = 1;
		stack[0] = 0;

		for (uint8 i = 0; i < length; i++) {
			uint2 num = data[i];
			if (num > 0) {
				if (top > 0) m[std::make_pair(stack[top - 1], num)]++;
				stack[top++] = num;
				assert(top < maxstacksize);
				if (max < top) max = top;
			} else {
				top--;
	  //printf("top : %d num : %d\n",top,num);
				assert(top > 0);
			}
		}
		if (mode == 2){
			for (map<pair<uint2, uint2>, uint8>::iterator it = m.begin(); it != m.end(); it++) {
				tmp = string(info[it->first.first].c_str())+"-->"+info[it->first.second].c_str();
				retDS[tmp] = it->second ;
					}
		}
	}
	return retDS;
}



/**
 * Read (decompress) trace files by Martin Burtscher and Sindu Devale
 */
uint2* readFile(const char name[], uint8& length)
{
	//printf("INSIDE READFILE NAME: %s\n",name);
	FILE* f = fopen(name, "rb");  assert(f != NULL);
	fseek(f, 0, SEEK_END);

	uint8 csize = ftell(f);  assert(csize > 0);
	//printf("csize %.2f\n",csize);
	uint1* cbuf = new uint1[csize];
	fseek(f, 0, SEEK_SET);
	length = fread(cbuf, sizeof(uint1), csize, f);  assert(length == csize);
	fclose(f);

	uint8 bytes = 0;
	uint8 cpos = 0;
	while (cpos < csize) {
		uint1 bitpat = cbuf[cpos++];
		for (uint1 b = 0; b < 8; b++) {
			if (bitpat & (1 << b)) {
				cpos++;
			}
		}
		if (cpos == csize - 1) {
			bytes += cbuf[cpos++] * 2;
		} else {
			bytes += 8;
		}
	}
	uint1* bbuf = new uint1[bytes + 6];
	uint8 bpos = 0;
	cpos = 0;
	while (cpos < csize - 1) {
		uint1 bitpat = cbuf[cpos++];
		for (uint1 b = 0; b < 8; b++) {
			if (bitpat & (1 << b)) {
				bbuf[bpos++] = cbuf[cpos++];
			} else {
				bbuf[bpos++] = 0;
			}
		}
	}
	delete [] cbuf;

	uint2* dbuf = (uint2*)malloc(8192 * sizeof(uint2));  assert(dbuf != NULL);
	uint8 dcap = 8192;
	uint8 dpos = 0;

	bool iscount;
	uint2 ppos = 0;
	uint2 lpos = 0;
	uint2 hash = 0;
	uint8 tpos = 0;

	memset(hbuf, 0, hsize * sizeof(hbuf[0]));
	memset(pbuf, 0, psize * sizeof(pbuf[0]));

	uint8 words = bytes / 2;
	uint2* tbuf = (uint2*)bbuf;
	while (tpos < words) {
		lpos = hbuf[hash];
		iscount = (pbuf[(lpos - 3) & psizem1] == pbuf[(ppos - 3) & psizem1]) &&
		(pbuf[(lpos - 2) & psizem1] == pbuf[(ppos - 2) & psizem1]) &&
		(pbuf[(lpos - 1) & psizem1] == pbuf[(ppos - 1) & psizem1]);
		if (iscount) {
			uint2 count = tbuf[tpos++];
			for (uint2 i = 0; i < count; i++) {
				uint2 value = pbuf[lpos];
				if (dpos == dcap) {
					dcap *= 2;
					dbuf = (uint2*)realloc(dbuf, dcap * sizeof(uint2));  assert(dbuf != NULL);
				}
				dbuf[dpos++] = value;
				lpos = (lpos + 1) & psizem1;
				hbuf[hash] = ppos;
				hash = ((hash << (hsizelg2 / 3)) ^ value) % hsize;
				pbuf[ppos] = value;
				ppos = (ppos + 1) & psizem1;
			}
		}

		if (tpos < words) {
			uint2 value = tbuf[tpos++];
			if (dpos == dcap) {
				dcap *= 2;
				dbuf = (uint2*)realloc(dbuf, dcap * sizeof(uint2));  assert(dbuf != NULL);
			}
			dbuf[dpos++] = value;
			hbuf[hash] = ppos;
			hash = ((hash << (hsizelg2 / 3)) ^ value) % hsize;
			pbuf[ppos] = value;
			ppos = (ppos + 1) & psizem1;
		}
	}

	length = dpos;

	delete [] bbuf;
	return dbuf;
}


/**
 * Read info file by Martin Burtscher and Sindhu Devale
 */
string* readInfo(const char name[])
{
	char img[256], fnc[256];
	string* info = new string[INFSIZE];
	FILE* f = fopen(name, "rt");  assert(f != NULL);
	uint2 i = 1;
	while (fscanf(f, "%255s\n", fnc) == 1) {
		printf("First fnc: %s\n",fnc);
		if (fnc[0] == '+') {
			fscanf(f, "%255s\n", img);
			fscanf(f, "%255s\n", fnc);
		}
		printf("IMG: %s\n",img);

		string fncs = (string)fnc;
		size_t found1= fncs.find("@plt");
		size_t found2= fncs.find("_GLOBAL__sub_I_");
		if (found1!=string::npos)
			fnc = fncs.substr(0,fncs.length()-4).c_str();
			//printf("\t<><><>fnc: %s\n",fncs.substr(0,fncs.length()-4).c_str());
		if (found2!=string::npos)
			fnc = fncs.substr(15).c_str() ;
			//printf("\t<><><>fnc: %s\n",fncs.substr(15).c_str());

		img[255] = 0;
		fnc[255] = 0;
		char* p = fnc;
		while ((*p != 0) && (*p != '.')) p++;

		char* dname;
		int status;

		if (*p == '.') {
			printf("\t\t<<<>>> NULLL\n");
			*p = 0;
			dname = abi::__cxa_demangle(fnc, 0, 0, &status);
			*p = '.';
		} else{
			dname = abi::__cxa_demangle(fnc, 0, 0, &status);
		}

		if (dname != NULL) {
			if (found1!=string::npos)
				info[i] = dname + "@plt";
			else if (found2!=string::npos)
				info[i] = "_GLOBAL__sub_I_" + dname;
			else
				info[i] = dname;
			if (*p == '.') {
				info[i] += p;
			}
			free(dname);
		} else {
			printf("\t\t\t>>> NULLL\n");
			info[i] = fnc;
		}

		assert(i < INFSIZE);
		i++;
	}
	fclose(f);
	return info;
}


/**
 * Primary process() by Martin Burtscher and Sindhu Devale
 */
void primProcess(const uint2 data[], const uint8 length, string info[], const int mode,const char out[]){

	char temp[100];

	info[0] = "[ret]";
	FILE *pfile;
	uint8 enter[INFSIZE];
	for (uint4 i = 0; i < 65536; i++){
		enter[i] = 0;
	}


	for (uint8 i = 0; i < length; i++) {
		enter[data[i]]++;
	}


	if (mode == 1){
		strcpy(temp,out);
		strcat(temp,"_1_calls.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n--- function calls ---\n");

		for (uint4 i = 1; i < 65536; i++) {
			if (enter[i] != 0) {
				fprintf(pfile,"%8lld: %s\n", enter[i], info[i].c_str());
			}
		}
	}


	if (mode == 4) {
		strcpy(temp,out);
		strcat(temp,"_4_trace.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n---  call trace---\n");
	}


	map<pair<uint2, uint2>, uint8> m;
	uint2 stack[maxstacksize], top = 1, max = 1;
	stack[0] = 0;

	for (uint8 i = 0; i < length; i++) {
		uint2 num = data[i];
		if (num > 0) {
			if (top > 0) m[std::make_pair(stack[top - 1], num)]++;
			stack[top++] = num;
			assert(top < maxstacksize);
			if (mode == 4) {
				for (uint2 i = 1; i < top; i++) {
					fprintf(pfile,"  %s", info[stack[i]].c_str());
				}
				fprintf(pfile,"\n");
			}
			if (max < top) max = top;
		} else {
			top--;
			assert(top > 0);
		}
	}

	if (mode == 2){
		strcpy(temp,out);
		strcat(temp,"_2_edges.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n--- call edges ---\n");
		for (map<pair<uint2, uint2>, uint8>::iterator it = m.begin(); it != m.end(); it++) {
			fprintf(pfile,"%8lld:  %s  -->  %s\n", it->second, info[it->first.first].c_str(), info[it->first.second].c_str());
		}
	}


	if (mode == 3) {
		strcpy(temp,out);
		strcat(temp,"_3_stack.txt");
		pfile = fopen(temp,"w");
		printf("\n--- approximate call stack ---\n");
		for (uint8 i = 0; i < length; i++) {
			fprintf(pfile,"%s\n", info[data[i]].c_str());
		}
	}
}
