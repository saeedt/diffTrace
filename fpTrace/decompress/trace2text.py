
#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: trace2text.py
# Description: run CL traceReader to convert traces to texTrace

import glob
import sys,subprocess
import os

if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" dataPath"
	sys.exit(-1)

dataPath = sys.argv[1]


# READ DATA 
outpath = dataPath+"/texTrace"

print "mkdir -p "+outpath
process = subprocess.Popen(["mkdir -p "+outpath], stdout=subprocess.PIPE,shell=True)
si, err = process.communicate()

for f in sorted(glob.glob(dataPath+"/trace/*.info")):
	for i in range(0,20):
		if os.path.isfile(f.rpartition(".")[0]+"."+`i`):
			command = "/home/saeed/cld/traceOps/toText/exec -m 3 -i " + f + " -t " 
			command = command + f.rpartition(".")[0]+"."+`i`
			command = command + " -o " + outpath
			print command 
			process = subprocess.Popen([command], stdout=subprocess.PIPE,shell=True)
			si, err = process.communicate()
			print si
