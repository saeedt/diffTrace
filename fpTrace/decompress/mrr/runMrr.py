
#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: runMrr.py
# Description: runMRR

import glob
import sys,subprocess
import os
import mrrCore as mrr

if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" file"
	sys.exit(-1)


fii = [x for x in open(sys.argv[1],"r").readlines() if len(x) > 0]
fi = fii[0].split(",")
print "Len(input) = %d\n"%(len(fi))
s = ""
tts = mrr.toMRR(fi)
for item in tts:
    s = s + item + "\n"

fo = open("out.txt",w)
fo.write(s)
fo.close()
