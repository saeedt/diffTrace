/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: trc_decomp.cpp
 * Description: Part of traceToText decompression application
 */
#include "trc_decomp.h"

using namespace std;



//////////////////////////////////////////////////////////////////////////////
// Class CollectiveAnalysis definitions
//////////////////////////////////////////////////////////////////////////////

int collectiveAnalysis::colAnalCount = 0;

collectiveAnalysis::collectiveAnalysis(){}

collectiveAnalysis::collectiveAnalysis(int _tid, string _keyword){
	this->keyword = _keyword;
	this->tid = _tid;
	this->colAnalCount++;
	this->id = this->colAnalCount;
}

string collectiveAnalysis::toString(){
	printf(">>>>INSIDE: \n");
	stringstream ss;
	string s;
	
	const char separator    = ' ';
    const int nameWidth     = 30;
    const int numWidth      = 10;
	
	ss << "-----------------------------------------" << endl;
	ss << left << setw(nameWidth) << setfill(separator) << "Attribute" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << "Value" << "|" << endl;
	ss << "-----------------------------------------" << endl;
	ss << left << setw(nameWidth) << setfill(separator) << "Trace ID" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->tid << "|" << endl;
    //printf(">>>>KIRRR\n");
    printf(">>>>TID: %d\n",this->tid);

	ss << left << setw(nameWidth) << setfill(separator) << "KeyWord" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->keyword << "|" << endl;
    ss << left << setw(nameWidth) << setfill(separator) << "Word(func)" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->word << "|" << endl;
    ss << left << setw(nameWidth) << setfill(separator) << "First Appearance" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->fa << "|" << endl;
    ss << left << setw(nameWidth) << setfill(separator) << "Last Appearance" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->la << "|" << endl;
    ss << left << setw(nameWidth) << setfill(separator) << "First Appearance (R)" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->fa_r << "|" << endl;
    ss << left << setw(nameWidth) << setfill(separator) << "Last Appearance (R)" << "|" ;
    ss << left << setw(numWidth) << setfill(separator) << this->la_r << "|" << endl;
    s = s + ss.str();
	
	s = s + "-----------------------------------------\n";
	//printf(">>>>KIRRRRRRRRRRRRRRRRR\n");
	return s;
}
/*
void collectiveAnalysis::setAttributes(string _word, uint8 _fa, uint8 _la, float _fa_r, float _la_r, float _freq){
	this->word = _word;
	this->fa = _fa;
	this->la = _la;
	this->fa_r = _fa_r;
	this->la_r = _la_r;
	this->freq = _freq;
}
*/
//////////////////////////////////////////////////////////////////////////////
// Collective Analysis Operations
//////////////////////////////////////////////////////////////////////////////


bool isContain(string A,string B,int whole){
	bool ret = false;
	if (whole == 1){
		ret = (A == B);
	}else {
		if (A.find(B) != std::string::npos)
			ret = true;
	}
	return ret;
}

/**
 * Detecting collectives
 */
collectiveAnalysis detector(const uint2 data[], const uint8 length, string info[], int id, string wrd){

	collectiveAnalysis obj = collectiveAnalysis(id,wrd);
	//obj.tid = id;
	//obj.keyword = wrd;
	// FIND First Appearance FA
	for (uint8 i = 0 ; i <length ; i++) {
		if (isContain(info[data[i]],obj.keyword,0)){
			obj.word = info[data[i]];
			//printf("First Appearance of the word %s :\n\t%s\n\t%lld\n",obj.keyWord.c_str(),obj.word.c_str(),i);
			obj.fa=i;
			break;
		}
	}
	//printf("Inside detector %s for %s\n",obj.keyword.c_str(),obj.word.c_str());
	for (uint8 i = length-1 ; i != -1 ; i--) {
		if (isContain(info[data[i]],obj.keyword,0)){
			obj.word = info[data[i]];
			obj.la=i;
			//printf("Last Appearance of the word %s :\n\t%s\n\t%lld\n",obj.keyword.c_str(),obj.word.c_str(),i);
			break;
		}
	}
	//printf("After Second for\n");
	obj.fa_r = (float)obj.fa / (float)length;
	//printf("between\n");
	obj.la_r = (float)obj.la / (float)length;
	//printf("before return\n");
	return obj;
}

/**
 * Detect the collectives and generate analysis for them
 */
vector<collectiveAnalysis> collectiveDetector(const uint2 data[], const uint8 length, string info[],int tid){

	info[0] = "[ret]";

	// Desired collectives
	vector<string> keyWords;
	keyWords.push_back("Barrier");
	keyWords.push_back("barrier");
	keyWords.push_back("poll");
	keyWords.push_back("pend");
	keyWords.push_back("MPI_Barrier");
	keyWords.push_back("MPIDU_Sched_are_pending");

	// Vector to store the collective analysis
	vector<collectiveAnalysis> anal;
	//printf("\n****Checking collectives for %d\n\n",tid);

	// Calculating the frequencies
	uint8 enter[65536];
	for (uint4 i = 0; i < 65536; i++){
		enter[i] = 0;
	}
	for (uint8 i = 0; i < length; i++) {
		enter[data[i]]++;
	}
	uint8 totalFreq = 0;
	for (uint4 i = 0; i < 65536; i++){
		if (enter[i] != 0){
			totalFreq = totalFreq + enter[i];
		}
	}

	// Send data_array and each keyword to generate the analysis
	vector<string>::iterator kwit;
	for (kwit = keyWords.begin() ; kwit != keyWords.end() ; kwit++){
		collectiveAnalysis temp = detector(data,length,info,tid,*kwit);
		temp.freq = (float)enter[data[temp.fa]] / (float)totalFreq;
		anal.push_back(temp);
	}
	return anal;
	/*
	uint2* data1 = (uint2*)malloc(fp * sizeof(uint2));  assert(data1 != NULL);
	uint2* data2 = (uint2*)malloc((length - lp) * sizeof(uint2));  assert(data2 != NULL);
	 for (uint8 i = 0; i < fp ; i++) {
		data1[i] = data[i];
	 }
  	for (uint8 i = lp + 1; i < length; i++) {
		data2[i-lp + 1] = data[i];
	}*/
}
//////////////////////////////////////////////////////////////////////////////
// DATA(uint2) operations: Filter
//////////////////////////////////////////////////////////////////////////////


vector<uint2> traceFilter(const uint2 data[], const uint8 length, string info[],set<string> keywords, int whole){
	vector<uint2> ret;
	info[0]="[ret]";

	set<string>::iterator siter;
	string kw;
	int counter;
	bool present;

	for (uint8 i = 0 ; i < length ; i++){
		present = false;
		for (siter = keywords.begin(); siter != keywords.end() ; siter++){
			kw = *siter;
			if (isContain(info[data[i]],kw,whole)){
				//printf("Keyword \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i]].c_str(),i,data[i]);
				/*printf("\n\n>>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i-5]].c_str(),i-5,data[i-5]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i-4]].c_str(),i-4,data[i-4]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i-3]].c_str(),i-3,data[i-3]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i-2]].c_str(),i-2,data[i-2]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i-1]].c_str(),i-1,data[i-1]);
				printf("Keyword \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i]].c_str(),i,data[i]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i+1]].c_str(),i+1,data[i+1]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i+2]].c_str(),i+2,data[i+2]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i+3]].c_str(),i+3,data[i+3]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n",kw.c_str(),info[data[i+4]].c_str(),i+4,data[i+4]);
				printf(">>>>> \"%s\" found in \"%s\"\n >>> data[%d] = %d \n\n\n",kw.c_str(),info[data[i+5]].c_str(),i+5,data[i+5]);
				*/
				present = true;
				break;
			} else{
				//printf("Func \"%s\"\n",info[data[i]].c_str());
			}
		}
		if (!present){
			ret.push_back(data[i]);	
		}
	}

	return ret;
}


//////////////////////////////////////////////////////////////////////////////
// traceReader(): Decompress, read info, read file, process
//////////////////////////////////////////////////////////////////////////////


static const uint2 maxstacksize = 4096;
static const uint1 hsizelg2 = 12;
static const uint2 hsize = 1 << hsizelg2;
static const uint1 psizelg2 = 16;
static const uint4 psize = 1 << psizelg2;
static const uint2 psizem1 = psize - 1;

uint2 hbuf[hsize];
uint2 pbuf[psize];


/**
 * Process the trace files
 *
 * Input: decompressed data, info, length of data, mode
 * Output: Depending on the mode a set of all either <freq,function calls> or  <freq,function edges>
 */

map<string,int> process(const uint2 data[], const uint8 length, string info[], const int mode){
	map<string,int> retDS;
	string tmp;
	//set<string> functions;

	info[0] = "[ret]";

	uint8 enter[65536];
	for (uint4 i = 0; i < 65536; i++) {
		enter[i] = 0;
	}

	for (uint8 i = 0; i < length; i++) {
		enter[data[i]]++;
	}

	if (mode == 1){
		for (uint4 i = 1; i < 65536; i++) {
			if (enter[i] != 0) {
				printf("FS------>%d,%s \n",enter[i], info[i].c_str());
				retDS[info[i]] = enter[i];
      	//fprintf(pFile0,"%8lld: %s\n", enter[i], info[i].c_str());
			}
		}
	}
	else{
		map<pair<uint2, uint2>, uint8> m;
		uint2 stack[maxstacksize], top = 1, max = 1;
		stack[0] = 0;

		for (uint8 i = 0; i < length; i++) {
			uint2 num = data[i];
			if (num > 0) {
				if (top > 0) m[std::make_pair(stack[top - 1], num)]++;
				stack[top++] = num;
				assert(top < maxstacksize);
				if (max < top) max = top;
			} else {
				top--;
	  //printf("top : %d num : %d\n",top,num);
				assert(top > 0);
			}
		}
		if (mode == 2){
		//printf(">>>>>>>>>>>>>>>>>>>>>>>>>>MODE 2\n" );
			for (map<pair<uint2, uint2>, uint8>::iterator it = m.begin(); it != m.end(); it++) {
				tmp = string(info[it->first.first].c_str())+"-->"+info[it->first.second].c_str();
				retDS[tmp] = it->second ;
				//fprintf(pFile1,"%8lld:  %s  -->  %s\n", it->second, info[it->first.first].c_str(), info[it->first.second].c_str());
			}
		}
	}
	return retDS;
}



/**
 * Read (decompress) trace files
 */
uint2* readFile(const char name[], uint8& length)
{
	//printf("INSIDE READFILE NAME: %s\n",name);
	FILE* f = fopen(name, "rb");  assert(f != NULL);
	fseek(f, 0, SEEK_END);

	uint8 csize = ftell(f);  assert(csize > 0);
	//printf("csize %.2f\n",csize);
	uint1* cbuf = new uint1[csize];
	fseek(f, 0, SEEK_SET);
	length = fread(cbuf, sizeof(uint1), csize, f);  assert(length == csize);
	fclose(f);

	uint8 bytes = 0;
	uint8 cpos = 0;
	while (cpos < csize) {
		uint1 bitpat = cbuf[cpos++];
		for (uint1 b = 0; b < 8; b++) {
			if (bitpat & (1 << b)) {
				cpos++;
			}
		}
		if (cpos == csize - 1) {
			bytes += cbuf[cpos++] * 2;
		} else {
			bytes += 8;
		}
	}
	uint1* bbuf = new uint1[bytes + 6];
	uint8 bpos = 0;
	cpos = 0;
	while (cpos < csize - 1) {
		uint1 bitpat = cbuf[cpos++];
		for (uint1 b = 0; b < 8; b++) {
			if (bitpat & (1 << b)) {
				bbuf[bpos++] = cbuf[cpos++];
			} else {
				bbuf[bpos++] = 0;
			}
		}
	}
	delete [] cbuf;

	uint2* dbuf = (uint2*)malloc(8192 * sizeof(uint2));  assert(dbuf != NULL);
	uint8 dcap = 8192;
	uint8 dpos = 0;

	bool iscount;
	uint2 ppos = 0;
	uint2 lpos = 0;
	uint2 hash = 0;
	uint8 tpos = 0;

	memset(hbuf, 0, hsize * sizeof(hbuf[0]));
	memset(pbuf, 0, psize * sizeof(pbuf[0]));

	uint8 words = bytes / 2;
	uint2* tbuf = (uint2*)bbuf;
	while (tpos < words) {
		lpos = hbuf[hash];
		iscount = (pbuf[(lpos - 3) & psizem1] == pbuf[(ppos - 3) & psizem1]) &&
		(pbuf[(lpos - 2) & psizem1] == pbuf[(ppos - 2) & psizem1]) &&
		(pbuf[(lpos - 1) & psizem1] == pbuf[(ppos - 1) & psizem1]);
		if (iscount) {
			uint2 count = tbuf[tpos++];
			for (uint2 i = 0; i < count; i++) {
				uint2 value = pbuf[lpos];
				if (dpos == dcap) {
					dcap *= 2;
					dbuf = (uint2*)realloc(dbuf, dcap * sizeof(uint2));  assert(dbuf != NULL);
				}
				dbuf[dpos++] = value;
				lpos = (lpos + 1) & psizem1;
				hbuf[hash] = ppos;
				hash = ((hash << (hsizelg2 / 3)) ^ value) % hsize;
				pbuf[ppos] = value;
				ppos = (ppos + 1) & psizem1;
			}
		}

		if (tpos < words) {
			uint2 value = tbuf[tpos++];
			if (dpos == dcap) {
				dcap *= 2;
				dbuf = (uint2*)realloc(dbuf, dcap * sizeof(uint2));  assert(dbuf != NULL);
			}
			dbuf[dpos++] = value;
			hbuf[hash] = ppos;
			hash = ((hash << (hsizelg2 / 3)) ^ value) % hsize;
			pbuf[ppos] = value;
			ppos = (ppos + 1) & psizem1;
		}
	}

	length = dpos;
	
	delete [] bbuf;
	return dbuf;
}


/**
 * Read info file
 */
string* readInfo(const char name[])
{
	char img[256], fnc[256];
	string* info = new string[65536];
	FILE* f = fopen(name, "rt");  assert(f != NULL);
	uint2 i = 1;
	while (fscanf(f, "%255s\n", fnc) == 1) {
		if (fnc[0] == '+') {
			fscanf(f, "%255s\n", img);
			fscanf(f, "%255s\n", fnc);
		}
		img[255] = 0;
		fnc[255] = 0;
    //printf("%d\t%s\t%s\n", i, img, fnc);

		char* p = fnc;
		while ((*p != 0) && (*p != '.')) p++;

		char* dname;
		int status;
		if (*p == '.') {
			*p = 0;
			dname = abi::__cxa_demangle(fnc, 0, 0, &status);
			*p = '.';
		} else {
			dname = abi::__cxa_demangle(fnc, 0, 0, &status);
		}
		if (dname != NULL) {
			info[i] = dname;
			if (*p == '.') {
				info[i] += p;
			}
			free(dname);
		} else {
			info[i] = fnc;
		}

		assert(i < 65535);
		i++;
	}
	fclose(f);
	return info;
}


/**
 * Primary process() by Texas peeps
 */
void primProcess(const uint2 data[], const uint8 length, string info[], const int mode,const char out[]){

	char temp[100];

	info[0] = "[ret]";
	FILE *pfile;
	uint8 enter[65536];
	for (uint4 i = 0; i < 65536; i++){
		enter[i] = 0;
	}


	for (uint8 i = 0; i < length; i++) {
		enter[data[i]]++;
	}


	if (mode == 1){
		strcpy(temp,out);
		strcat(temp,"_1_calls.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n--- function calls ---\n");

		for (uint4 i = 1; i < 65536; i++) {
			if (enter[i] != 0) {
				fprintf(pfile,"%8lld: %s\n", enter[i], info[i].c_str());
			}
		}
	}


	if (mode == 4) {
		strcpy(temp,out);
		strcat(temp,"_4_trace.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n---  call trace---\n");
	}


	map<pair<uint2, uint2>, uint8> m;
	uint2 stack[maxstacksize], top = 1, max = 1;
	stack[0] = 0;

	for (uint8 i = 0; i < length; i++) {
		uint2 num = data[i];
		if (num > 0) {
			if (top > 0) m[std::make_pair(stack[top - 1], num)]++;
			stack[top++] = num;
			assert(top < maxstacksize);
			if (mode == 4) {
				for (uint2 i = 1; i < top; i++) {
					fprintf(pfile,"  %s", info[stack[i]].c_str());
				}
				fprintf(pfile,"\n");
			}
			if (max < top) max = top;
		} else {
			top--;
			assert(top > 0);
		}
	}

	if (mode == 2){
		strcpy(temp,out);
		strcat(temp,"_2_edges.txt");
		pfile = fopen(temp,"w");
		fprintf(pfile,"\n--- call edges ---\n");
		for (map<pair<uint2, uint2>, uint8>::iterator it = m.begin(); it != m.end(); it++) {
			fprintf(pfile,"%8lld:  %s  -->  %s\n", it->second, info[it->first.first].c_str(), info[it->first.second].c_str());
		}
	}


	if (mode == 3) {
		strcpy(temp,out);
		strcat(temp,"_3_stack.txt");
		pfile = fopen(temp,"w");
		printf("\n--- approximate call stack ---\n");
		for (uint8 i = 0; i < length; i++) {
			fprintf(pfile,"%s\n", info[data[i]].c_str());
		}
	}
}
