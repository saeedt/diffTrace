/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: trc_decomp.h
 * Description: Part of traceToText decompression application
 */
 #ifndef TRC_DECOMP_H
#define TRC_DECOMP_H

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <cstring>
#include <utility>
#include <cxxabi.h>
#include <vector>
#include <map>
#include <vector>
#include <set>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <iterator>
#include <iomanip>
 
using namespace std;

#define version 17
#define Q(x) #x
#define QUOTE(x) Q(x)




extern long hnum;

typedef unsigned char uint1; 
typedef unsigned short uint2;
typedef unsigned int uint4;
typedef unsigned long long uint8;


//!  Collective Analysis. 
/*!
  To Store information about collectives that we are looking for
  such as Barriers, first appearance (fa) last appearance (la) and frequency
*/

class collectiveAnalysis{
	public:
	int id; 
	int tid; /*!< Trace ID of target trace */ 
	string keyword; /*!< Main collective keyword */ 
	string word; /*!<  Full word for collective*/ 
	uint8 fa; /*!<  Index of first appearnce of the collective */ 
	uint8 la; /*!<  Index of last appearnce of the collective */ 
	float freq; /*!< Frequency of collective */ 
	float fa_r; /*!<  [Add later]*/ 
	float la_r; /*!<  [Add later]*/
	static int colAnalCount;
	collectiveAnalysis();
	collectiveAnalysis(int _tid, string _keyword);
	string toString();
};


/**
 * Decompress data from traces and return a map<k,v> where k is each entry within the trace (based on mode) and v is its frequency
 */
map<string,int> process(const uint2 data[], const uint8 length, string info[], const int mode);


string stackTrace(const uint2 data[], const uint8 length, string info[], const int mode);

/**
 * Primary: Decompress data from traces and write the output to file (out) based on mode
 */
void primProcess(const uint2 data[], const uint8 length, string info[], const int mode,const char out[]);

/**
 * Read trace files to decompress the byte codes 
 */
uint2* readFile(const char name[], uint8& length);

/**
 * Read info file to decompress traces
 */
string* readInfo(const char name[]) ;

/**
 * A mechanism to detect collectives within a trace
 */
vector<collectiveAnalysis> collectiveDetector(const uint2 data[], const uint8 length, string info[],int tid);

/**
 * Test if A contians or equal to B
 */
bool isContain(string A,string B,int whole);


/**
 * Filter the trace data
 * Input: Decompressed data, info, length of data, set of 
 *        keywords, (bool)whole, (bool)remove
 * Output: Depending on keywords and whole, either remove the 
 *         keyword from data and return new one or print 
 *         some stats about the keyword.
 * IF NOT isContain THEN add the element to the return vector ELSE 
 * IF whole THEN we want the target to be the whole keyword ELSE we want the target to contain the keyword
 */
vector<uint2> traceFilter(const uint2 _data[], const uint8 _length, string _info[],set<string> keywords, int whole);

/**
 * Search for a collective word within a trace and returns a collectiveAnalysis struct with all needed info
 */
collectiveAnalysis detector(const uint2 data[], const uint8 length, string info[], int tid, string keyword);

#endif
