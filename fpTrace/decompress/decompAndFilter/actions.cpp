/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: actions.cpp
 * Description: Part of traceToText decompression application
 */
#include "actions.h"

void actionM3(string _trace, string _info, string _outPath){

	

	///////////////////////////////
	// Tokenizing trace and info
	///////////////////////////////
	string traceToken;
	string infoToken;

	traceToken = splitString(_trace, '/').back() ;
	infoToken = splitString(_info, '/').back() ; 
	//printf("traceToken : %s\n", traceToken.c_str());
	//printf("infoToken : %s\n", infoToken.c_str());
	

	
	string infoPath = _outPath+"/"+infoToken+".txt";
	string tracePath = _outPath+"/"+traceToken+".txt";

	printf("tracePath : %s\n", tracePath.c_str());
	printf("infoPath : %s\n", infoPath.c_str());

	string* info;
	uint2* data ;
	uint8 length;

	info=readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);

	ofstream fdata;
	fdata.open(tracePath);
	for (uint8 i = 0 ; i < length ; i++){
		fdata << data[i] << "," ;
	}
	fdata.close();


	ofstream finfo;
	finfo.open(infoPath);
	for (uint4 i = 0 ; i < 65536 ; i++){
		finfo << info[i] << "," ;
	}
	finfo.close();
	//primProcess(data, length, info, _outMode,action.c_str());


}

 
void actionTest2(string _trace,string _info,int _outMode,string _outName){
	string action = "singleDecomp."+_outName;

	string* info;
	uint2* data ;
	uint8 length;
	set<string> keywords;
	vector<uint2> dvec;

	info=readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);

	//keywords.insert("ret");
	//keywords.insert("sched");
	//keywords.insert("wait");
	//keywords.insert("poll");
	keywords.insert("Barrier");
	keywords.insert("barrier");

	dvec = traceFilter(data,length,info,keywords,0);
	printf("Size Before Filter: %llu\nSize After Filter: %llu\n",length,dvec.size());


	//primProcess(data, length, info, _outMode,action.c_str());
}




void actionM2(string _trace, string _info, int _outMode, string _outName){
	
	string action = "singleDecomp."+_outName;

	string* info;
	uint2* data ;
	uint8 length;

	info=readInfo(_info.c_str());
	data = readFile(_trace.c_str(), length);
	primProcess(data, length, info, _outMode,action.c_str());
	

}

 