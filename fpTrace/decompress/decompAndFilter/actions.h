/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: actions.h
 * Description: Part of traceToText decompression application
 */

#ifndef ACTIONS_H
#define ACTIONS_H


#include <vector>
#include <set>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
//#include <fstream>
#include <sstream>
#include <iterator>
#include <dirent.h>
#include <unistd.h>
//#include <iostream>
#include <sstream>
#include <iostream>
#include <fstream>


#include "trc_decomp.h"
#include "gen_convert.h"



#include <iterator>


using namespace std;




/**
 * actionM2() called from main to generate single report from single trace and single info based on the mode
 */
void actionM2(string trace,string info,int outMode,string outName);


/**
 * actionM3() called from main to store the output of readFile() and readInfo() into files (from single traces)
 */
void actionM3(string trace,string info,string outName);


/**
 * on testing splitting traces (under Heavy Construction)
 */

void actionTest2(string trace,string info,int outMode,string outName);


 

#endif





