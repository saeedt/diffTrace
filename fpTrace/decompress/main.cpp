/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
 * Code: main.cpp
 * Description: The skeleton of this code is basically traceReader by Martin Burtscher and Sindhu Devale.
 * It reads in compressed trace files one by one, decompress them and write the sequence of function IDs to text files. Actual functions are in .info files(mode to run with: -m 3).
 */
#include "actions.h"
#include <typeinfo>

using namespace std;

//set<pair<string,pair<int,string> > > ftable;

struct globalArgs_t{
	char *path;
	char *info;
	char *trace;
	int mode;
	char *output;
	int outMode;
}globalArgs;


int main(int argc, char* argv[]) {
	printf("TraceReader v%s (%s)\n", QUOTE(version), __FILE__);
	//printUsage();

	static const char *optstring = "m:p:o:i:t:d:h?";
	globalArgs.path = NULL;
	globalArgs.info = NULL;
	globalArgs.trace = NULL;
	globalArgs.mode = 0;
	globalArgs.output = NULL;
	globalArgs.outMode = 0;

	int opt;

	while ((opt = getopt(argc,argv,optstring)) != -1){
		switch(opt){
			case 'm':
				if (strcmp("1",optarg) == 0){
					globalArgs.mode = 1;
					break;
				}else if (strcmp("2",optarg) == 0) {
					globalArgs.mode = 2;
					break;
				}else if (strcmp("3",optarg) == 0) {
					globalArgs.mode = 3;
					break;
				}else if (strcmp("4",optarg) == 0) {
					globalArgs.mode = 4;
					break;
				}else if (strcmp("8",optarg) == 0) {
					globalArgs.mode = 8;
					break;
				}else if (strcmp("9",optarg) == 0) {
					globalArgs.mode = 9;
					break;
				}

			case 'p':
				globalArgs.path = optarg;
				break;
			case 'o':
				globalArgs.output = optarg;
				break;
			case 'i':
				globalArgs.info = optarg;
				break;
			case 't':
				globalArgs.trace = optarg;
				break;
			case 'd':
				if (strcmp("1",optarg) == 0){
					globalArgs.outMode = 1;
					break;
				}else if (strcmp("2",optarg) == 0) {
					globalArgs.outMode = 2;
					break;
				}else if (strcmp("3",optarg) == 0) {
					globalArgs.outMode = 3;
					break;
				}else if (strcmp("4",optarg) == 0) {
					globalArgs.outMode = 4;
					break;
				}

			case 'h':
			case '?':
				if (optopt == 'm' || optopt == 'p' || optopt == 'o' || optopt == 'i' || optopt == 't' || optopt == 'd' ) {
					fprintf (stderr, "Option -%c requires an argument.\n", optopt);
					printUsage();
				} else if (isprint (optopt)){
					fprintf (stderr, "Unknown option `-%c'.\n", optopt);
					printUsage();
				} else {
					fprintf (stderr,"Unknown option character `\\x%x'.\n",optopt);
					printUsage();
				}
        		return 1;
				break;
			default:
				printUsage();
				abort();
		}
	}
	if ( globalArgs.mode == 1 ){
		//actionM1(globalArgs.path,globalArgs.outMode,globalArgs.output);
	}
	if ( globalArgs.mode == 2 ){
		actionM2(globalArgs.trace,globalArgs.info,globalArgs.outMode,globalArgs.output);
	}
	if ( globalArgs.mode == 3 ){
		actionM3(globalArgs.trace,globalArgs.info,globalArgs.output);
	}
  if ( globalArgs.mode == 4 ){
    printf("kireKhar\n");
		actionM4(globalArgs.trace,globalArgs.info,globalArgs.output);
	}
}
