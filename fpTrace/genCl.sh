#! /bin/bash

PTH=traces/oct18
for img in m a
do
	for f in -o2-avx-fma -o3-avx-fma -o3-unsafe
	do
		for a in 1 2 3 4
		do
			echo ./fptrace -m 4 -p $PTH/mfem-o0-$img/mfem-o0-$img.out -b $PTH/mfem$f-$img/mfem$f-$img.out -a $a -o $PTH/cl/mfemx$f-$a-$img-;
			./fptrace -m 4 -p $PTH/mfem-o0-$img/mfem-o0-$img.out -b $PTH/mfem$f-$img/mfem$f-$img.out -a $a -o $PTH/cl/mfemx$f-$a-$img-;
		done
	done
done
