# FPTrace
__FPTrace__ currently captures instructions in different image and routine granularites. Multiple levels of filtering are available ([more](#pre-collection-filters)).


## CONTENTS
- [Setup](#setup)
- [Example](#example)
- [Pre-collection Filters](#pre-collection-filters)
- [Post-collection Filters](#post-collection-filters)
- [Diff](#diff)

## Setup
Below command builds FPTrace tool and set the paths:
```
>$ source activate-parlot.sh <path-to-diffTrace>
```
This is how paths are set:
```
>$ export DIFF_ROOT=<path-to-diffTrace>
>$ export PIN_ROOT=$DIFF_ROOT/pin/
>$ export PATH=$PIN_ROOT:$PATH
>$ export FPTRACE_ROOT=$PIN_ROOT/source/tools/fpt-staticBlock/obj-intel64/
```

## Example
`>$ pin -t $FPTRACE_ROOT/obj-intel64/fpt-staticBlock.so [-o output] -- ./a.out`

## Pre-collection Filters

| Granularity | Filter | Description and HowTo |
|-------------|--------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Image | Main Image | Only Collect routines and instructions from the main image: ``` pin -t $FPTRACE_ROOT/fpt-staticBlock.so -o OUTPUT -filter_no_shared_libs -- ./a.out ``` |
| Image | All Images | Collect routines and instructions from all images (shared libraries): ``` pin -t $FPTRACE_ROOT/fpt-staticBlock.so -o OUTPUT -- ./a.out ``` |
| Routine | Specific Routine  (mangled function) | Lets say you want to capture instructions of a function with this signature "dot(float *, float *, int)" ``` pin -t $FPTRACE_ROOT/fpt-staticBlock.so -o OUTPUT -filter_rtn $($DIFF_HOME/scripts/mangle.bash "dot(float *, float *, int)") -- ./a.out ``` |

Above commands would result in three files:
### OUTPUT.out
A report about FP instructions within each block.
### OUTPUT.trc
The __basic block trace__ - a sequence of BBL ids. The corresponding name of each id is written to OUTPUT.info.
### OUTPUT.info
Each entry in this file is in this format:
<Block ID>:<Image Name>:<Routine Name>:<Trace ID>:<Block-per-Trace ID>
The combination of trace ID and block-per-trace ID is unique (i.e., sufficient for assigning unique block IDs)

## Post-collection Filters (NOT VALID YET)

| Granularity | Filter | Description |
|-------------|-------------------------------------------------|----------------------------------------------------------------------------------------|
| Routine | `--rtn_only` | Filter out instructions and only show routines (functions). |
| Routine | `--routine RTN [RTN ...]` | Only include instructions from this particular routine(s). |
| Instruction | `--filt_jumps` | Filter out jump (`jmp`,`jz`,etc.) instructions. |
| Instruction | `--filt_calls` | Filter out `call` instructions. |
| Instruction | `--filt_addresses` | Filter out addresses. |
| Instruction | `--filt_instInputs` | Filter out instruction inputs. |
| Instruction | `--instruction_set {all,FMA,SSE,FPall,FParith}` | Only include instructions from these instruction sets in `insets.toml`. Default: `all` |
| Instruction | --instruction INST [INST ...] | Only include this particular instruction(s) in all routines |


## Instruction Sets

| Set | Instructions | Regex |
|-------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------|
| FMA | ```"VFMADD132PDy","VFMADD132PSy","VFMADD132PDx","VFMADD132PSx","VFMADD132SD","VFMADD132SS","VFMADD213PDy","VFMADD213PSy","VFMAD D213PDx","VFMADD213PSx","VFMADD213SD","VFMADD213SS","VFMADD231PDy","VFMADD231PSy","VFMADD231PDx","VFMADD231PSx","VFMADD231SD","VFMADD231SS"``` | ```^VFMADD\w*$``` |
| SSE (data, arithmetic, comparison, logic) | ```"ADDSS","SUBSS","MULSS","DIVSS","RCPSS","SQRTSS","MAXSS","MINSS","RSQRTSS","ADDPS","SUBPS","MULPS","DIVPS","RCPPS","SQRTPS","MAXPS","MINPS","RSQRTPS","MOVSS","MOVAPS","MOVUPS","MOVLPS","MOVHPS","MOVLHPS","MOVHLPS","MOVMSKPS","SHUFPS", "UNPCKHPS", "UNPCKLPS", "CVTSI2SS", "CVTSS2SI", "CVTTSS2SI","CVTPI2PS", "CVTPS2PI", "CVTTPS2PI","CMPSS","COMISS","UCOMISS","ANDPS","ORPS","XORPS","ANDNPS"``` | ```^MOV\w*[S|P]S$,^UNP\w*PS$,^SHUFPS$,^CVT\w*[P|S][I|S]$``` |
| Floating Point Basic Arithmetic | ```"FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","F2XM1","FCOS","FPATAN","FPTAN","FSIN","FSINCOS","FYL2X","FYL2XP1"``` |  |
| Floating Point comparison and control | ```"FABS","FADD","FADDP","FCHS","FDIV","FDIVP","FDIVR","FDIVRP","FIADD","FIDIV","FIDIVR","FIMUL","FISUB","FISUBR","FMUL ","FMULP","FPREM","FPREM1","FRNDINT","FSCALE","FSQRT","FSUB","FSUBP","FSUBR","FSUBRP","FXTRACT","FCLEX","FDECSTP","FFREE","FINCSTP","FINIT","FLDCW","FLDENV","FNCLEX","FNINIT","FNOP","FNSAVE","FNSTCW","FNSTENV","F NSTSW","FRSTOR","FSAVE","FSTCW","FSTENV","FSTSW","FWAIT","WAIT"``` |  |


## Diff
In `diff` folder, execute '>$ python diff.py -h' to see how to generate diff graph of pair of FPTraces applying __post collection filters__.
Briefly, here is how it runs:
```
python diff.py dot1.txt dot2.txt --mode general --instruction_set all --filt_branches --configFile insets.toml
```
More to be added.
