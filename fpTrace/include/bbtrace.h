/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
 * Code: bbtrace.h
 * Description: decompression of ParLOT traces
 */
#ifndef BBTRACE_H
#define BBTRACE_H


#include <vector>
#include <map>
#include <unordered_map>
#include <string>
#include <string.h>
#include <stdio.h>
#include <algorithm>
#include <sstream>
#include <iterator>
#include <dirent.h>
#include <unistd.h>
#include <sstream>
#include <iostream>
#include <fstream>
#include <iterator>
#include <math.h>
#include <regex>
#include <assert.h>
#include "util.h"


using namespace std;

#define BITSETSIZE 17

class BBTrace{
  string function;
  string image;
  map< int,vector<int> > bitVector;
  string pathTpBBTrace;
  map<int,int> bbs;
  // vector<int> elements;
  // int lc;
  // static int distinctElemenets;
  // static int origLen;
  // static int ldataLen;
  // static int fdataLen;
  // static int maxLC;
  // static int maxLoopBody;
  // static map<int,string> dtab;
  // static unordered_map<string,int> rdtab;
  // static map<int,string> ltab;
  // static unordered_map<string,int> rltab;
public:
  //! Default Constructor
  BBTrace();

  //! Constructor
  BBTrace(string line);

  //! Destructor
  ~BBTrace();

  //! Set function name
  void setFunction(string s);

  //! Set Image name
  void setImage(string s);

  //! Add BBID to the map
  void addBBid(int bbid, int freq);

  //! Add BBVector to the map
  void addBBvector(int bbid, string vector);

  //! Set Trace Path
  void setTracePath(string s);

  //! Get number of BB for each function
  int getNumBB();

  //! Get Trace path
  string getTracePath();

  //! Get Bitvector (sum)
  vector<int> getBitvector();


};


#endif
