/**
 * Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2019, All rights reserved
 * Code: bbtrace.cpp
 * Description:
 */
#include "bbtrace.h"
#include "util.h"

BBTrace::BBTrace(){
  this->function = "";
  this->image = "";
  this->pathTpBBTrace = "";
  this->bitVector.clear();
  this->bbs.clear();
}

BBTrace::BBTrace(string line){
  // split line
  // it is guaranteed that line belongs to a function that has not been processed yet
  // set function name
  // set image name
  // set/add (bbid,freq)
  // set/add (bbid,bitvector)
  vector<string> v = splitString(line,'|');
  this->setFunction(v[0]);
  this->setImage(v[1]);
  this->addBBid(stoi(v[2]),stoi(v[3]));
  this->addBBvector(stoi(v[2]),v[4]);

}

BBTrace::~BBTrace(){}

void BBTrace::setFunction(string s){
  this->function = s;
}

void BBTrace::setImage(string s){
  this->image = s;
}

void BBTrace::addBBid(int bbid, int freq){
  this->bbs[bbid] = freq;
}

void BBTrace::addBBvector(int bbid, string vs){
  vector<int> v;
  vector<string> vctr;
  vctr = splitString(vs,'.');

  vector<string>::iterator vsit;

  for (vsit = vctr.begin(); vsit != vctr.end()-1;vsit++){
    v.push_back(stoi(*vsit));
  }
  this->bitVector[bbid] = v;
}

void BBTrace::setTracePath(string s){
  this->pathTpBBTrace = s;
}

int BBTrace::getNumBB(){
  return this->bbs.size();
}

string BBTrace::getTracePath(){
  return this->pathTpBBTrace;
}

vector<int> BBTrace::getBitvector(){
  vector<int> ret;
  map<int,vector<int> >::iterator mit;
  int sum;
  for (int i = 0 ; i < BITSETSIZE ; i++){
    sum = 0;
    for(mit = (this->bitVector).begin() ; mit != (this->bitVector).end() ; mit++){
      sum += (mit->second)[i] * (this->bbs[mit->first]);
    }
    ret.push_back(sum);
  }
  return ret;
}
