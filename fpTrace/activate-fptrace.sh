#!/bin/bash

export DIFF_ROOT=$1
export PIN_ROOT=$DIFF_ROOT/pin/
export PATH=$PIN_ROOT:$PATH

CUR=$PWD

cd $PIN_ROOT/source/tools/fpt-staticBlock
make clean;
make;

cd $PIN_ROOT/source/tools/fpt-blockTrace
make clean;
make;

cd $CUR

export FPTRACE_ROOT=$PIN_ROOT/source/tools/fpt-staticBlock/obj-intel64/
export BLTRACE_ROOT=$PIN_ROOT/source/tools/fpt-blockTrace/obj-intel64/
