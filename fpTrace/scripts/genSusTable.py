#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')
#matplotlib.use('tkagg')

import glob
import sys,subprocess
import os
import numpy as np
from scipy.cluster.hierarchy import fcluster
from scipy.cluster.hierarchy import *
import seaborn as sns ; sns.set(font_scale=1.1)
#import seaborn as sns
import matplotlib.pyplot as plt
import pandas as pd

from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict


def readMatrix(matFile):
	try:
		data = open(matFile,"r").read().split("\n")[:-1]
		dataSize = len(data)
		mat = np.ones((len(data),len(data)))
		for i in range(0,len(data)):
			item = [x for x in data[i].split(",") if len(x) > 0]
			#print item
			for j in range(0,len(item)):
				if float(item[j]) != 1:
					mat[i][j] = float(item[j])
	except Exception, e:
		print "ERROR %s"%e
		mat = np.zeros((dataSize,dataSize))
	return mat

def fcluster2map(cl):
	ret={}
	for i in range(0,len(cl)):
		if int(cl[i]-1) in ret.keys():
			ret[int(cl[i])-1].append(i)
		else:
			ret[int(cl[i])-1]=[i]
	# for k,v in sorted(ret.items()):
	# 	print "CL %d:"%k
	# 	print v
	return ret


def clusterMap2String(cm,type):
	s = ""
	for k,v in sorted(cm.items()):
		s = s + "CL %d:%d"%(k,len(v))
		if len(v) <= topn and type == "diff":
			s = s + ",{"
			for vv in v:
				s = s + `vv` + " "
			s = s + "}\n"
		else:
			s = s + "\n"
	return s

def jsm2StringCell(pdist1,type,method,criterion,thresh):
	if method == "single":
		z1 = single(pdist1)
	elif method == "complete":
		z1 = complete(pdist1)
	elif method == "average":
		z1 = average(pdist1)
	elif method == "weighted":
		z1 = weighted(pdist1)
	elif method == "centroid":
		z1 = centroid(pdist1)
	elif method == "median":
		z1 = median(pdist1)
	elif method == "ward":
		z1 = ward(pdist1)
	cm = fcluster2map(fcluster(z1,thresh,criterion=criterion))
	return clusterMap2String(cm,type)

def jsm2outliers(pdist1,method,criterion,thresh):
	if method == "single":
		z1 = single(pdist1)
	elif method == "complete":
		z1 = complete(pdist1)
	elif method == "average":
		z1 = average(pdist1)
	elif method == "weighted":
		z1 = weighted(pdist1)
	elif method == "centroid":
		z1 = centroid(pdist1)
	elif method == "median":
		z1 = median(pdist1)
	elif method == "ward":
		z1 = ward(pdist1)
	cm = fcluster2map(fcluster(z1,thresh,criterion=criterion))
	ret = []
	for k,v in sorted(cm.items()):
		if len(v) <= topn:
			ret.extend(v)
	return ret

def candidateRow(l,oz):
    ret={}
    rets = []
    i = 1
    for item in l:
        if item in ret.keys():
            ret[item] = ret[item]+1
        else:
            ret[item] = 1
    if oz == 0:
        for k,v in sorted(ret.items(), key = lambda x: x[1], reverse = True):
            if k%threadNum == 0:
                s = `k/threadNum`
                rets.append(s)
                i = i + 1
    else:
        for k,v in sorted(ret.items(), key = lambda x: x[1], reverse = True):
            if k%threadNum != 0:
                s = `k/threadNum` + "." + `k%threadNum`
                rets.append(s)
                i = i + 1
    return rets

def matchingMatrix(pdist1,pdist2,pdist3,method,criterion,thresh):
	np.set_printoptions(precision=6)
	if method == "single":
		z1 = single(pdist1)
		z2 = single(pdist2)
		z3 = single(pdist3)
	elif method == "complete":
		z1 = complete(pdist1)
		z2 = complete(pdist2)
		z3 = complete(pdist3)
	elif method == "average":
		z1 = average(pdist1)
		z2 = average(pdist2)
		z3 = average(pdist3)
	elif method == "weighted":
		z1 = weighted(pdist1)
		z2 = weighted(pdist2)
		z3 = weighted(pdist3)
	elif method == "centroid":
		z1 = centroid(pdist1)
		z2 = centroid(pdist2)
		z3 = centroid(pdist3)
	elif method == "median":
		z1 = median(pdist1)
		z2 = median(pdist2)
		z3 = median(pdist3)
	elif method == "ward":
		z1 = ward(pdist1)
		z2 = ward(pdist2)
		z3 = ward(pdist3)

	#print z1
	#print z2
	a1 = fcluster2map(fcluster(z1,thresh,criterion=criterion))
	a2 = fcluster2map(fcluster(z2,thresh,criterion=criterion))
	a3 = fcluster2map(fcluster(z2,thresh,criterion=criterion))
	# print "***********************************\n%s"%method
	# print a1
	# print ""
	# print a2
	# print ""
	# print a3

	n = (len(pdist1)*(len(pdist1)-1))/2
	#assert(len(a1.keys()) == len(a2.keys()))
	if len(a1.keys()) == len(a2.keys()):
		mm = np.zeros((len(a1.keys()),len(a1.keys())))
		for i in range(0,len(a1)):
			for j in range(0,len(a2)):
				mm[i][j]= len(list(set(a1[i]) & set(a2[j]) )) # len(intersection ( objects in cluster i of a1 and objects in cluster j of a2 ))
		return mm
	else:
		return []

def measureB (mm,n):
	t = 0
	p = 0
	q = 0
	pl = []
	for i in range(0,len(mm)):
		for j in range(0,len(mm)):
			t = t + math.pow(mm[i][j],2)
	t = t - n
	for i in range(0,len(mm)):
		pl = 0
		for j in range(0,len(mm)):
			pl = pl + mm[i][j]
		p = p + math.pow(pl,2)
	p = p - n

	for j in range(0,len(mm[0])):
		ql = 0
		for i in range(0,len(mm)):
			ql = ql + mm[i][j]
		q = q + math.pow(ql,2)
	q = q - n

	b = ( t * 1.0 ) / math.sqrt(p * q)
	return b


def topTable(exp,filt):
    app = exp.split(".")[0]
    bug = exp.split(".")[1]
    img = exp.split(".")[2]
    proc = exp.split(".")[3]
    thr = exp.split(".")[4]
    procNum = int(proc);
    if thr == "auto":
        threadNum = 5

	#hdrs = ["Filter","Attributes","K(cluster)","# Objects noBug","# Objects buggy","B score","# Objects diff"]
    hdrs = ["Filter","Attributes","K(cluster)","# Objects diff"]
    if filt == "mpi":
        filts = mpiFilters[:1]
    elif filt == "mpi2":
        filts = mpiFilters2
    elif filt == "omp":
        filts = ompFilters
    elif filt == "omp2":
        filts = omp2
    elif filt == "all":
        filts = filters
    elif filt == "eveything":
        filts = everythingFilters
    else:
        filts = omp2
    tab2=[]
    hdrs2 = ["Filter","Top Ranks (Process)","Top Ranks (Threads)"]

    for f in filts:
        ttab2 = []
        tab = []
        outliers = []
        for a in atrs:
            for th in [2,3,4]:
    			ttab = []
    			#print "Creating Table Cell for %s filter: %s , atr: %s, k-cluster: %d"%(exp,f,a,th)
    			jsmb = readMatrix(path+"/"+exp+"/cl/"+f+"/"+a+".w.jacmat.txt")
    			jsmnb = readMatrix(path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/cl/"+f+"/"+a+".w.jacmat.txt")
    			ttab.append(f)
    			ttab.append(a)
    			ttab.append(th)
    			#ttab.append(jsm2StringCell(jsmb,"buggy","metric","method","maxclust",th,0))
    			#ttab.append(jsm2StringCell(jsmnb,"noBug","metric","method","maxclust",th,0))
    			m = matchingMatrix(jsmb,jsmnb,"metric","method","maxclust",th,0)
    			#ttab.append(measureB(m,len(jsmb)))
    			ttab.append(jsm2StringCell(np.absolute(np.subtract(jsmb,jsmnb)),"diff","metric","method","maxclust",th,0))
    			outliers.extend(jsm2outliers(np.absolute(np.subtract(jsmb,jsmnb)),"metric","method","maxclust",th,0))
    			tab.append(ttab)

        ttab2.append(f)

    	ttab = [" "," ","TOP MPI Suspicious\nTraces to check"]
    	tmps = ""
    	tmp = candidateRow(outliers,0)
    	for i in range(0,min(topCandidates,len(tmp))):
    		tmps = tmps + tmp[i] + "\n"
    	ttab.append(tmps)
    	tab.append(ttab)

        ttab2.append(tmps)

        ttab = [" "," ","TOP OMP Suspicious\nTraces to check"]
    	tmps = ""
    	tmp = candidateRow(outliers,1)
    	for i in range(0,min(topCandidates,len(tmp))):
    		tmps = tmps + tmp[i] + "\n"
    	ttab.append(tmps)
    	tab.append(ttab)

        ttab2.append(tmps)

    	#print tabulate(tab,headers=hdrs,tablefmt="fancy_grid",showindex="always")
        process = subprocess.Popen("mkdir -p results/"+exp, stdout=subprocess.PIPE,shell=True)
        si, err = process.communicate()
        s = tabulate(tab,headers=hdrs,tablefmt="plain")
        fo = open("results/"+exp+"/"+f+".txt","w")
    	fo.write(s)
    	fo.close()
        tab2.append(ttab2)

    fo2 = open("results/"+exp+".txt","w")
    fo2.write(tabulate(tab2,headers=hdrs2,tablefmt="plain"))
    fo2.close()


def jac2pdist(m):
	max = 0
	min = 1
	ret = []
	for i in range(0,len(m)-1):
	    for j in range(i+1,len(m)):
			#print "M[%d][%d]= %.3f"%(i,j,m[i][j])
			ret.append(m[i][j])
			# if m[i][j] < min:
	        #     min = m[i][j]
	        # if m[i][j] > max:
			# 	max = m[i][j]
    # print "Min:" + min
    # print "Max:" + max
	print len(ret)
	return ret

def readObjTable(path):
	ret = {}
	for line in open(path,"r").readlines():
		if line.startswith("ID") or line.startswith("-"):
			continue
		lines = line.split("|")
		ret[int(lines[0].strip())] = lines[1].strip()
	return ret
	#ret[int(lines[0].strip())] = lines[1].strip()+"|"+lines[2].strip()

def mapObjtab(m,ot):
    for k,v in m.items():
        print "\n***********\nCluster "+`k`+"\n*************\n"
        for item in v:
            print `int(item)+1`+":"+ot[int(item)+1]


if len(sys.argv) != 3:
	print "USAGE:\n\t " +sys.argv[0]+" path1 path2"
	sys.exit(-1)

path1 = sys.argv[1]
path2 = sys.argv[2]
pathobj = path1.rpartition(".")[0].rpartition(".")[0] + ".objTable.txt"

linkageMethods= ["single","complete","average","weighted","centroid","median","ward"]

jsm1 = readMatrix(path1)
jsm2 = readMatrix(path2)
jsmd = np.absolute(np.subtract(jsm1,jsm2))
objtab = readObjTable(pathobj)
pdist1 = jac2pdist(jsm1)
pdist2 =jac2pdist(jsm2)
pdistd = jac2pdist(jsmd)

criterion = "maxclust"
thresh = 2
#show clusters of jsm1
print "###\nMFEM 1\n###\n"
np.set_printoptions(precision=5)
z1 = average(pdist1)
#for item in z1:
#    print item
mapObjtab(fcluster2map(fcluster(z1,thresh,criterion=criterion)),objtab)
#show clusters of jsm2
print "###\nMFEMX 2\n###\n"
z2 = average(pdist2)
mapObjtab(fcluster2map(fcluster(z2,thresh,criterion=criterion)),objtab)
#show clusters of jsmd
print "###\nDIFF\n###\n"
zd = average(pdistd)
#for item in zd:
#    print item
#criterion = "maxclust"
#thresh = 3
mapObjtab(fcluster2map(fcluster(zd,thresh,criterion=criterion)),objtab)


# Convert Mat(jsm) to Data-Frame
mat = jsmd
df = {}
for i in range(0,len(mat)):
	df[objtab[i+1]]=mat[i]


#ax = sns.heatmap(mat1,vmin=0, vmax=1)

ax = sns.clustermap(pd.DataFrame(df).transpose(),cbar=False,col_cluster=False)

#fig = ax.get_figure()
#fig.tight_layout()
#fig.savefig(out+"_JsmDdendo.pdf")


ax.savefig("dot4-jsmdDendo.pdf")

# outliers = jsm2outliers(pdistd,lm,"maxclust",thresh)
#
# for lm in linkageMethods:
# 			for thresh in [2,3,4]:
# 				#print filter
# 				ttab = []
# 				ttab.append(filter)
#
# 				satr = attribute.split(".")[0]+"."
# 				if attribute.split(".")[1] == "orig":
# 					satr = satr + "noFreq"
# 				else:
# 					satr = attribute
#
# 				ttab.append(satr)
# 				ttab.append(lm)
# 				ttab.append(thresh)
# 				#print jsm2StringCell(pdistd,"diff",lm,"maxclust",thresh)
# 				outliers = jsm2outliers(pdistd,lm,"maxclust",thresh)
# 				#print outliers
# 				#print ">"
# 				m = matchingMatrix(pdistb,pdistnb,pdistd,lm,"maxclust",thresh)
# 				if len(m) != 0:
# 					bscore = measureB(m,len(jsmb))
# 				else:
# 					bscore = 2
# 				ttab.append("%.3f"%bscore)
# 				tmps = ""
# 				tmp = candidateRow(outliers,0)
# 				for i in range(0,min(topCandidates,len(tmp))):
# 					tmps = tmps + tmp[i] + " , "
# 				ttab.append(tmps)
# 				tmps = ""
# 				tmp = candidateRow(outliers,1)
# 				for i in range(0,min(topCandidates,len(tmp))):
# 					tmps = tmps + tmp[i] + " , "
# 				ttab.append(tmps)
# 				tab.append(ttab)
# u = 0
# ntab = []
# print tab
# for item in sorted(tab, key = lambda k: k[-3]):
#
# 	#print item
# 	if u > 15:
# 		break
# 	if exp.startswith("ilcsTSP.a") or exp.startswith("ilcsTSP.b"):
# 		if item[5] != '':
# 			ntab.append(item)
# 			u = u + 1
# 	else:
# 		ntab.append(item)
# 		u = u + 1
# #print tabulate(sorted(tab, key = lambda k: k[-3],reverse=True),headers=hdrs,tablefmt="fancy_grid")
# #print tabulate(ntab,headers=hdrs,tablefmt="latex")
# s = "\\begin{table}[]\n"
# s = s + "\\centering\n"
# s = s  + "\\caption{"+exp+"}\n"
# s = s + "\\label{tab:"+exp.split(".")[1]+"}\n"
# s = s + "\\scalebox{0.5}{\n"
# s = s + tabulate(ntab,headers=hdrs,tablefmt="latex")
# s = s + "}\n\\end{table}\n"
# print s
#
# print tabulate(ntab,headers=hdrs,tablefmt="fancy_grid")
# #fo = open(exp+".tex","w")
# #fo.write(s)
# #fo.close
# 		#input()
#
# #mat1 = readMatrix(sys.argv[1]) # Buggy
# #mat2 = readMatrix(sys.argv[2]) # noBug
# #print "Generating candidate tables for ... "+sys.argv[1]
# #topTable(sys.argv[1],sys.argv[2])
