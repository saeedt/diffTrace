#!/bin/bash

export DIFF_ROOT=$1
export PIN_ROOT=$DIFF_ROOT/pin/
export PATH=$PIN_ROOT:$PATH

CUR=$PWD

cd $PIN_ROOT/source/tools/parlotm
make clean;
make;

cd ../parlota
make clean;
make;

cd $CUR

export PARLOTM=$PIN_ROOT/source/tools/parlotm/obj-intel64
export PARLOTA=$PIN_ROOT/source/tools/parlota/obj-intel64

