

#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>




void
single_round(int message_len, char* message,
	     int buffer_len, char* buffer,
	     int send_to, int recv_from,
	     int rank, int size)
{
  assert(message_len >= 0);
  assert(message != NULL);
  assert(buffer_len >= 0);
  assert(buffer != NULL);
  assert(message_len <= buffer_len);
  assert(rank >= 0);
  assert(size >= 0);
  assert(rank < size);
  assert(send_to < size);
  assert(recv_from < size);
  assert(send_to != rank);
  assert(recv_from != rank);


  int state, i;


  // Make sure buffer content is set
  for (i=0; i<buffer_len; i++) {
    buffer[i] = 'a';
  }
  buffer[buffer_len-1] = '\0';


  // Send and recieve using rank 0 to break the dining philosophers problem
  if (rank == 0) {
    // printf("%d: recieving from %d\n", rank, recv_from);
    state = MPI_Recv(buffer, message_len, MPI_CHAR, recv_from, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    assert(state == MPI_SUCCESS);

    // printf("%d: sending '%s' to %d\n", rank, message, send_to);
    state = MPI_Send(message, message_len, MPI_CHAR, send_to, 0, MPI_COMM_WORLD);
    assert(state == MPI_SUCCESS);
  } else {
    // printf("%d: sending '%s' to %d\n", rank, message, send_to);
    state = MPI_Send(message, message_len, MPI_CHAR, send_to, 0, MPI_COMM_WORLD);
    assert(state == MPI_SUCCESS);

    // printf("%d: recieving from %d\n", rank, recv_from);
    state = MPI_Recv(buffer, message_len, MPI_CHAR, recv_from, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    assert(state == MPI_SUCCESS);
  }


  // Print what we recieved
  printf("%d: received '%s' from %d\n", rank, buffer, recv_from);
}


int
main(int argc, char** argv)
{
  int state, size, rank, message_len, buffer_len, send_to, recv_from, i;
  char* message;
  char* buffer;

  // Init, get size and rank
  state = MPI_Init(&argc, &argv);
  assert(state == MPI_SUCCESS);

  state = MPI_Comm_size(MPI_COMM_WORLD, &size);
  assert(state == MPI_SUCCESS);
  assert(size > 1);

  state = MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  assert(state == MPI_SUCCESS);

  // One optional argument
  if (argc > 2) {
    if (rank == 0) {
      printf("This program takes one optional argument.\n given arguments:");
      for (i=1; i<argc; i++) {
	printf(" %s", argv[i]);
      }
      printf("\n");
    }
    MPI_Abort(MPI_COMM_WORLD, -1);
  }


  // Setup the recieve buffer
  buffer_len = 1024;
  buffer = (char*) malloc(sizeof(char)*buffer_len);
  assert(buffer != NULL);


  // Round 1
  send_to = (rank + 1) % size;
  recv_from = (rank + size - 1) % size;
  message = "Hello";
  message_len = ((int) strlen(message)) + 1;
  single_round(message_len, message, buffer_len, buffer, send_to, recv_from, rank, size);


  // Round 2
  send_to = (rank + size - 1) % size;
  recv_from = (rank + 1) % size;
  message = "world";
  message_len = ((int) strlen(message)) + 1;
  single_round(message_len, message, buffer_len, buffer, send_to, recv_from, rank, size);


  // Round 3
  send_to = (rank + 1) % size;
  recv_from = (rank + size - 1) % size;
  message = "foo";
  message_len = ((int) strlen(message)) + 1;
  single_round(message_len, message, buffer_len, buffer, send_to, recv_from, rank, size);


  // Round 4
  send_to = (rank + size - 1) % size;
  recv_from = (rank + 1) % size;
  message = "bar";
  message_len = ((int) strlen(message)) + 1;
  single_round(message_len, message, buffer_len, buffer, send_to, recv_from, rank, size);

  // Optional round 5
  if (argc == 2) {
    send_to = (rank + 1) % size;
    recv_from = (rank + size - 1) % size;
    message = argv[1];
    message_len = ((int) strlen(message)) + 1;
    single_round(message_len, message, buffer_len, buffer, send_to, recv_from, rank, size);
  }

  // Finish up
  free(buffer);
  state = MPI_Finalize();
  assert(state == MPI_SUCCESS);

  return 0;
}
