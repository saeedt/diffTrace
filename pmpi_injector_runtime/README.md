This system injects improper behavior into the MPI run-time.
Multiple behavior types are available and controllable.

## Building:
This codebase should be able to compile with any MPI C compiler.
By default mpicc is assumed to exist
`make` should build the system and place libmpirt.so in the obj directory.

## Compiling a Target:
Your LIBRARY_PATH should include the above obj directory.
When linking an MPI executable add `-lpmpirt` to the command.

## Using:
There are two phases to injecting an executable; analysis and injection.
Analysis writes out a file that contains information about the MPI calls made.
Injection then causes improper behavior using the information from the analysis.
Analysis and injection must be done with the same executable using the same
number of processes.

## Executable Flags:
Flags used for this tooling must be at the end of the arguments given to the
executable in the `mpirun` command. So if you would normally run
`mpirun -np 4 ./a.out <args>`
the tooled version would be
`mpirun -np4 ./a.out <args> <injection_runtume_args>`

### --injection-logfile filename
Default: pmpi_injector_log.txt

Filename is where the analysis file will be. If performing an injection this is
the file that will be read.

### --injection-mode mode
Default: analyze

Options: analyze inject_allreduce inject_bcast inject_irecv inject_isend inject_recv inject_reduce inject_send inject_wait
This controls which mode will be used.

### --injection-target-rank rank
Default: random valid rank

This option only makes sense when injecting. It selects the process that will
be injected. If not specified during an injection a random rank will be chosen.

### --injection-target-invocation invocation
Default: random valid invocation

This option only makes sense when injecting. It selects the invocation of the
selected MPI function that will be injected. This is counted per rank.

### --injection-payload payload
Default: size_minus_one

Options: size_minus_one rank_minus_one omit fake_error spin

This selects what will go wrong.
* size_minus_one : decrease the size argument by one
* rank_minus_one : decrease the rank argument by one
* root_minus_one : decrease the root argument by one
* wrong_op : parform an MPI_Op other than the one requested
* omit : do not actually preform the operation, just return
* fake_error : perform the operation as requested, but return an MPI error status
* spin : never return from the MPI call

Note that only some payloads are available for each injection mode.
Currently, using the wrong payload will result in an error when the injection occurs.

Compatability(compatable mode/payload marked with an X):

| Payload        | inject_allreduce | inject_bcast | inject_irecv | inject_isend | inject_recv | inject_reduce | inject_send | inject_wait |
|----------------|------------------|--------------|--------------|--------------|-------------|---------------|-------------|-------------|
| size_minus_one | X                | X            | X            | X            | X           | X             | X           |             |
| rank_minus_one |                  |              | X            | X            | X           |               | X           |             |
| root_minus_one |                  | X            |              |              |             | X             |             |             |
| wrong_op       | X                |              |              |              |             | X             |             |             |
| omit           | X                | X            | X            | X            | X           | X             | X           | X           |
| fake_error     | X                | X            | X            | X            | X           | X             | X           | X           |
| spin           | X                | X            | X            | X            | X           | X             | X           | X           |




## Example:
A standard workflow looks like this.

```
>>> cd example

>>> ls
hello_world.c

>>> mpicc hello_world.c -o hello_world -lpmpirt

>>> ls
hello_world  hello_world.c

>>> mpirun -np 3 hello_world
INJECTOR: mode: analyze
INJECTOR: logfile: 'pmpi_injector_log.txt'
2: received 'Hello' from 1
0: received 'Hello' from 2
0: received 'world' from 1
1: received 'Hello' from 0
1: received 'world' from 2
2: received 'world' from 0
2: received 'foo' from 1
1: received 'foo' from 0
1: received 'bar' from 2
0: received 'foo' from 2
0: received 'bar' from 1
2: received 'bar' from 0

>>> ls
hello_world  hello_world.c  pmpi_injector_log.txt

>>> mpirun -np 3 hello_world --injection-mode inject_recv
INJECTOR: mode: inject_recv
INJECTOR: logfile: 'pmpi_injector_log.txt'
INJECTOR: target_rank 1
INJECTOR: target_invocation 2
INJECTOR: payload 'size_minus_one'
2: received 'Hello' from 1
0: received 'Hello' from 2
1: received 'Hello' from 0
INJECTOR: Error about to be injected

0: received 'world' from 1
[integrus:18783] *** An error occurred in MPI_Recv
[integrus:18783] *** reported by process [335675393,1]
[integrus:18783] *** on communicator MPI_COMM_WORLD
[integrus:18783] *** MPI_ERR_TRUNCATE: message truncated
[integrus:18783] *** MPI_ERRORS_ARE_FATAL (processes in this communicator will now abort,
[integrus:18783] ***    and potentially your MPI job)
2: received 'world' from 0
0: received 'foo' from 2

>>> mpirun -np 3 hello_world --injection-mode inject_send --injection-payload fake_error
INJECTOR: mode: inject_send
INJECTOR: logfile: 'pmpi_injector_log.txt'
INJECTOR: target_rank 1
INJECTOR: target_invocation 2
INJECTOR: payload 'fake_error'
2: received 'Hello' from 1
0: received 'Hello' from 2
1: received 'Hello' from 0
0: received 'world' from 1
INJECTOR: Error about to be injected
hello_world: hello_world.c:54: single_round: Assertion `state == MPI_SUCCESS' failed.
[integrus:18797] *** Process received signal ***
[integrus:18797] Signal: Aborted (6)
[integrus:18797] Signal code:  (-6)
[integrus:18797] [ 0] /lib/x86_64-linux-gnu/libpthread.so.0(+0x12890)[0x7f80bfb31890]
[integrus:18797] [ 1] /lib/x86_64-linux-gnu/libc.so.6(gsignal+0xc7)[0x7f80bf76ce97]
[integrus:18797] [ 2] /lib/x86_64-linux-gnu/libc.so.6(abort+0x141)[0x7f80bf76e801]
[integrus:18797] [ 3] /lib/x86_64-linux-gnu/libc.so.6(+0x3039a)[0x7f80bf75e39a]
[integrus:18797] [ 4] /lib/x86_64-linux-gnu/libc.so.6(+0x30412)[0x7f80bf75e412]
[integrus:18797] [ 5] hello_world(+0x16e2)[0x557ebb2806e2]
[integrus:18797] [ 6] hello_world(+0x19e3)[0x557ebb2809e3]
[integrus:18797] [ 7] /lib/x86_64-linux-gnu/libc.so.6(__libc_start_main+0xe7)[0x7f80bf74fb97]
[integrus:18797] [ 8] hello_world(+0x12da)[0x557ebb2802da]
[integrus:18797] *** End of error message ***
2: received 'world' from 0
0: received 'foo' from 2
--------------------------------------------------------------------------
mpirun noticed that process rank 1 with PID 0 on node integrus exited on signal 6 (Aborted).
--------------------------------------------------------------------------

>>> mpirun -np 3 hello_world --injection-mode inject_send --injection-payload spin --injection-target-rank 1
INJECTOR: mode: inject_send
INJECTOR: logfile: 'pmpi_injector_log.txt'
INJECTOR: target_rank 1
INJECTOR: target_invocation 3
INJECTOR: payload 'spin'
INJECTOR: Error about to be injected
0: received 'Hello' from 2
0: received 'world' from 1
0: received 'foo' from 2
1: received 'Hello' from 0
1: received 'world' from 2
2: received 'Hello' from 1
2: received 'world' from 0
^C
```