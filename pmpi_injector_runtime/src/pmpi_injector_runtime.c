

#include "pmpi_injector_runtime.h"

#include <assert.h>
#include <errno.h>
#include <mpi.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>




MPI_Op injection_different_op(MPI_Op op) {
  if (op == MPI_OP_NULL) { return MPI_REPLACE; }
  if (op == MPI_MAX)	 { return MPI_OP_NULL; }
  if (op == MPI_MIN)	 { return MPI_MAX;     }
  if (op == MPI_SUM)	 { return MPI_MIN;     }
  if (op == MPI_PROD)	 { return MPI_SUM;     }
  if (op == MPI_LAND) 	 { return MPI_PROD;    }
  if (op == MPI_BAND)	 { return MPI_LAND;    }
  if (op == MPI_LOR) 	 { return MPI_BAND;    }
  if (op == MPI_BOR)	 { return MPI_LOR;     }
  if (op == MPI_LXOR) 	 { return MPI_BOR;     }
  if (op == MPI_BXOR)	 { return MPI_LXOR;    }
  if (op == MPI_MINLOC)	 { return MPI_BXOR;    }
  if (op == MPI_MAXLOC)	 { return MPI_MINLOC;  }
  if (op == MPI_REPLACE) { return MPI_MAXLOC;  }
  return MPI_PROD;
}


char*
injection_copy_str(const char* str)
{
  int len;
  char* new_str;
  assert(str != NULL);

  len = strlen(str) + 1;
  new_str = (char*) injection_xmalloc(sizeof(char)*len);
  strcpy(new_str, str);
  return new_str;
}


void
injection_bcast_str(char** bufp, int root)
{
  int my_rank;
  int len;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (my_rank == root) {
    assert(bufp != NULL);
    assert((*bufp) != NULL);
    len = strlen((*bufp)) + 1;
  }

  PMPI_Bcast(&len, 1, MPI_INT, root, MPI_COMM_WORLD);
  if (my_rank != root) {
    (*bufp) = (char*) injection_xmalloc(sizeof(char)*len);
  }
  PMPI_Bcast((*bufp), len, MPI_CHAR, root, MPI_COMM_WORLD);
}

void
injection_exit(int retcode)
{
  int mpi_p;

  PMPI_Initialized(&mpi_p);
  if (mpi_p) {
    PMPI_Abort(MPI_COMM_WORLD, retcode);
  } else {
    exit(retcode);
  }
}


void
injection_printf(const char *fmt, ...) {
  char* pre;
  char* post;
  int pre_len;
  int post_len;
  int fmt_len;
  int len;
  char* buf;
  va_list args;
  int ret;
  assert(fmt != NULL);

  pre = "INJECTOR: ";
  post = "\n";

  pre_len = strlen(pre);
  post_len = strlen(post);
  fmt_len = strlen(fmt);
  len = pre_len + fmt_len + post_len + 1;

  buf = (char*) injection_xmalloc(sizeof(char)*len*2);

  strcpy(buf, pre);

  va_start(args, fmt);
  ret = vsnprintf(&buf[pre_len], 2*len - pre_len, fmt, args);
  va_end(args);

  if (ret < 0 || ret > 2*len - pre_len) {
    fprintf(stderr, "Buffer size for print too small\n");
    injection_exit(-1);
  }

  strcpy(&buf[pre_len + ret], post);

  fprintf(stderr, "%s", buf);

  free(buf);
}


void*
injection_xmalloc(size_t bytes)
{
  void* temp;

  temp = malloc(bytes);
  if (temp == NULL) {
    injection_printf("Unable to malloc memory; exiting!");
    injection_exit(-1);
  }
  return temp;
}


char*
injection_mode_to_str(injection_mode op)
{
  switch(op) {
  case(analyze):     return "analyze";
  case(inject_allreduce): return "inject_allreduce";
  case(inject_bcast): return "inject_bcast";
  case(inject_irecv): return "inject_irecv";
  case(inject_isend): return "inject_isend";
  case(inject_recv): return "inject_recv";
  case(inject_reduce): return "inject_reduce";
  case(inject_send): return "inject_send";
  case(inject_wait): return "inject_wait";
  default:
    injection_printf("Injection mode impl missing: (raw) %d\n", (int)op);
    injection_exit(-1);
    return "Unreachable";
  }
}


injection_mode
str_to_injection_mode(char* str)
{
  assert(str != NULL);

  if (strcmp(str, injection_mode_to_str(analyze)) == 0) {
    return analyze;
  } else if (strcmp(str, injection_mode_to_str(inject_allreduce)) == 0) {
    return inject_allreduce;
  } else if (strcmp(str, injection_mode_to_str(inject_bcast)) == 0) {
    return inject_bcast;
  } else if (strcmp(str, injection_mode_to_str(inject_irecv)) == 0) {
    return inject_irecv;
  } else if (strcmp(str, injection_mode_to_str(inject_isend)) == 0) {
    return inject_isend;
  } else if (strcmp(str, injection_mode_to_str(inject_recv)) == 0) {
    return inject_recv;
  } else if (strcmp(str, injection_mode_to_str(inject_reduce)) == 0) {
    return inject_reduce;
  } else if (strcmp(str, injection_mode_to_str(inject_send)) == 0) {
    return inject_send;
  } else if (strcmp(str, injection_mode_to_str(inject_wait)) == 0) {
    return inject_wait;
  } else {
    injection_printf("Unknown injection mode: '%s'\n", str);
    injection_exit(-1);
    return analyze;
  }
}


char*
injection_payload_to_str(injection_payload p)
{
  switch(p) {
  case(UNSET):          return "UNSET";
  case(size_minus_one): return "size_minus_one";
  case(rank_minus_one): return "rank_minus_one";
  case(root_minus_one): return "root_minus_one";
  case(wrong_op):       return "wrong_op";
  case(omit):           return "omit";
  case(fake_error):     return "fake_error";
  case(spin):           return "spin";
  default:
    injection_printf("Injection payload impl missing: (raw) %d\n", (int)p);
    injection_exit(-1);
    return "Unreachable";
  }
}


injection_payload
str_to_injection_payload(char* str)
{
  assert(str != NULL);

  if (strcmp(str, injection_payload_to_str(UNSET)) == 0) {
    return UNSET;
  } else if (strcmp(str, injection_payload_to_str(size_minus_one)) == 0) {
    return size_minus_one;
  } else if (strcmp(str, injection_payload_to_str(rank_minus_one)) == 0) {
    return rank_minus_one;
  } else if (strcmp(str, injection_payload_to_str(root_minus_one)) == 0) {
    return root_minus_one;
  } else if (strcmp(str, injection_payload_to_str(wrong_op)) == 0) {
    return wrong_op;
  } else if (strcmp(str, injection_payload_to_str(omit)) == 0) {
    return omit;
  } else if (strcmp(str, injection_payload_to_str(fake_error)) == 0) {
    return fake_error;
  } else if (strcmp(str, injection_payload_to_str(spin)) == 0) {
    return spin;
  } else {
    injection_printf("Unknown payload: '%s'\n", str);
    injection_exit(-1);
    return UNSET;
  }
}


int
str_to_rank(char* str)
{
  char* endptr;
  int rank;
  assert(str != NULL);

  rank = strtod(str, &endptr);
  if (errno == ERANGE) {
    injection_printf("Injection target rank is too large: '%s'", str);
    injection_exit(-1);
  }
  if (endptr != str + strlen(str)) {
    injection_printf("Injection target rank is not a number: '%s'", str);
    injection_exit(-1);
  }
  if (rank < 0) {
    injection_printf("Injection target rank is negative: '%s'", str);
    injection_exit(-1);
  }
  return rank;
}


long long
str_to_invocation(char* str)
{
  char* endptr;
  long long invocation;
  assert(str != NULL);

  invocation = strtoll(str, &endptr, 10);
  if (errno == ERANGE) {
    injection_printf("Injection target invocation is too large: '%s'", str);
    injection_exit(-1);
	}
  if (endptr != str + strlen(str)) {
    injection_printf("Injection target invocation is not a number: '%s'", str);
    injection_exit(-1);
  }
  if (invocation <= 0) {
    injection_printf("Injection target invocation is not positive: '%s'", str);
    injection_exit(-1);
  }
  return invocation;
}


injection_information_t*
injection_read_logfile(char* filename)
{
  injection_information_t* info;
  int my_rank;
  int this_size;
  FILE* fp;
  const int n = 1024;
  char line[n];
  int items;
  int rank;
  char type_str[100];
  long long count;
  long long *all_allreduces;
  long long *all_bcasts;
  long long *all_irecvs;
  long long *all_isends;
  long long *all_recvs;
  long long *all_reduces;
  long long *all_sends;
  long long *all_waits;
  int i;
  assert(filename != NULL);

  info = (injection_information_t*) injection_xmalloc(sizeof(injection_information_t));

  // set filename
  info->logfile = injection_copy_str(filename);

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  PMPI_Comm_size(MPI_COMM_WORLD, &this_size);


  // Root does the parsing then sends to other ranks
  if (my_rank == 0) {

    fp = fopen(filename, "r");


    // get logged executable name
    fgets(line, n, fp);
    line[strlen(line)-1] = '\0';
    info->exe = injection_copy_str(line);


    // get logged world size, check if it matches the current world size
    items = fscanf(fp, "%d\n", &info->size);
    if (items != 1) {
      injection_printf("Log file corrupted: '%s' (stage size)", filename);
      injection_exit(-1);
    }
    if (this_size != info->size) {
      injection_printf("Analysis file was made with a different np: given '%d' expected '%d'", this_size, info->size);
      injection_exit(-1);
    }


    // get tab seperated items. Each row will be rank, injection_mode, count
    // where injection mode indicates the instruction type (inject_send etc.)
    all_allreduces = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_bcasts = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_irecvs = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_recvs = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_isends = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_reduces = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_sends = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    all_waits = (long long*) injection_xmalloc(sizeof(long long)*this_size);
    for (i=0; i<this_size; i++) {
      all_sends[i] = -1;
      all_recvs[i] = -1;
    }

    for (i=0; i<8*this_size; i++) {
      items = fscanf(fp, "%d\t%s\t%lld\n", &rank, type_str, &count);
      if (items != 3) {
	injection_printf("Log file corrupted: '%s' (stage row)", filename);
	injection_exit(-1);
      }

      if (rank < 0 || rank >= this_size) {
	injection_printf("Log file corrupted: '%s' (stage row rank)", filename);
	injection_exit(-1);
      }

      if (count < 0) {
	injection_printf("Log file corrupted: '%s' (stage row count)", filename);
	injection_exit(-1);
      }

      if (strcmp(type_str, injection_mode_to_str(inject_allreduce)) == 0) {
	all_allreduces[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_bcast)) == 0) {
	all_bcasts[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_irecv)) == 0) {
	all_irecvs[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_isend)) == 0) {
	all_isends[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_recv)) == 0) {
	all_recvs[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_reduce)) == 0) {
	all_reduces[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_send)) == 0) {
	all_sends[rank] = count;
      } else if (strcmp(type_str, injection_mode_to_str(inject_wait)) == 0) {
	all_waits[rank] = count;
      } else {
	injection_printf("Log file corrupted: '%s' (stage row mode)", filename);
	injection_exit(-1);
      }
    }

    fclose(fp);


    // Check that all ranks were in the log and send off to other ranks
    info->sends = all_sends[0];
    info->recvs = all_recvs[0];
    for (i=1; i<this_size; i++) {
      if (all_allreduces[i] == -1 ||
	  all_bcasts[i] == -1 ||
	  all_irecvs[i] == -1 ||
	  all_isends[i] == -1 ||
	  all_recvs[i] == -1 ||
	  all_reduces[i] == -1 ||
	  all_sends[i] == -1 ||
	  all_waits[i] == -1) {
	injection_printf("Log file corrupted: '%s' (stage rank check)", filename);
	injection_exit(-1);
      }
      PMPI_Send(&all_allreduces[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_bcasts[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_irecvs[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_isends[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_recvs[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_reduces[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_sends[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
      PMPI_Send(&all_waits[i], 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD);
    }
  } else {
    // Non root ranks get the counts from root
    PMPI_Recv(&info->allreduces, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->bcasts, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->irecvs, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->isends, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->recvs, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->reduces, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->sends, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
    PMPI_Recv(&info->waits, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
  }

  // Update non-root
  injection_bcast_str(&info->exe, 0);
  PMPI_Bcast(&info->size, 1, MPI_INT, 0, MPI_COMM_WORLD);

  return info;
}
