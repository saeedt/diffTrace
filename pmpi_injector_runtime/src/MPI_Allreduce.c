

#include "pmpi_injector_runtime.h"

#include <mpi.h>




int
MPI_Allreduce(const void* sendbuf, void* recvbuf, int count,
	      MPI_Datatype datatype, MPI_Op op, MPI_Comm comm)
{
  int my_rank;
  long long invocation;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  injection_information.allreduces += 1;
  invocation = injection_information.allreduces;

  if (injection_target.mode == inject_recv &&
      my_rank == injection_target.rank &&
      invocation == injection_target.invocation) {

    injection_printf("Error about to be injected\n");
    switch(injection_target.payload) {
    case size_minus_one:
      return PMPI_Allreduce(sendbuf, recvbuf, count-1, datatype, op, comm);

    case rank_minus_one:
      injection_printf("Payload rank_minus_one is not compatable with inject_allreduce");
      injection_exit(-2);

    case root_minus_one:
      injection_printf("Payload root_minus_one is not compatable with inject_allreduce");
      injection_exit(-2);

    case wrong_op:
      return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, injection_different_op(op), comm);

    case omit:
      return MPI_SUCCESS;

    case fake_error:
      PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
      return MPI_ERR_COMM;

    case spin:
      for(;;) {}
      return -1;

    case UNSET:
      injection_printf("This should not be possible, injection made with UNSET payload");
      injection_exit(-2);

    default:
      injection_printf("Injection payload impl missing: '%s'\n", injection_payload_to_str(injection_target.payload));
      injection_exit(-1);
    }

    injection_printf("Unreachable code");
    injection_exit(-1);
    return -1;

  }

  return PMPI_Allreduce(sendbuf, recvbuf, count, datatype, op, comm);
}
