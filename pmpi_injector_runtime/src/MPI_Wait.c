

#include "pmpi_injector_runtime.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>




int
MPI_Wait(MPI_Request *request, MPI_Status *status)
{
  int my_rank;
  long long invocation;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  injection_information.waits += 1;
  invocation = injection_information.waits;

  if (injection_target.mode == inject_wait &&
      my_rank == injection_target.rank &&
      invocation == injection_target.invocation) {

    injection_printf("Error about to be injected");
    switch(injection_target.payload) {
    case size_minus_one:
      injection_printf("Payload size_minus_one is not compatable with inject_wait");
      injection_exit(-2);

    case rank_minus_one:
      injection_printf("Payload rank_minus_one is not compatable with inject_wait");
      injection_exit(-2);

    case root_minus_one:
      injection_printf("Payload root_minus_one is not compatable with inject_wait");
      injection_exit(-2);

    case wrong_op:
      injection_printf("Payload wrong_op is not compatable with inject_wait");
      injection_exit(-2);

    case omit:
      return MPI_SUCCESS;

    case fake_error:
      PMPI_Wait(request, status);
      return MPI_ERR_COMM;

    case spin:
      for(;;) {}
      return -1;

    case UNSET:
      injection_printf("This should not be possible, injection made with UNSET payload");
      injection_exit(-2);

    default:
      injection_printf("Injection payload impl missing: '%s'\n", injection_payload_to_str(injection_target.payload));
      injection_exit(-1);
    }

    injection_printf("Unreachable code");
    injection_exit(-1);
    return -1;
  }

  return PMPI_Wait(request, status);
}
