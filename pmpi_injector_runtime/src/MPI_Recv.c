

#include "pmpi_injector_runtime.h"

#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>




int
MPI_Recv(void* buf, int count, MPI_Datatype datatype, int source, int tag,
	 MPI_Comm comm, MPI_Status* status)
{
  int my_rank;
  long long invocation;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  injection_information.recvs += 1;
  invocation = injection_information.recvs;

  if (injection_target.mode == inject_recv &&
      my_rank == injection_target.rank &&
      invocation == injection_target.invocation) {

    injection_printf("Error about to be injected\n");
    switch(injection_target.payload) {
    case size_minus_one:
      return PMPI_Recv(buf, count-1, datatype, source, tag, comm, status);

    case rank_minus_one:
      return PMPI_Recv(buf, count, datatype, source-1, tag, comm, status);

    case root_minus_one:
      injection_printf("Payload root_minus_one is not compatable with inject_recv");
      injection_exit(-2);

    case wrong_op:
      injection_printf("Payload wrong_op is not compatable with inject_recv");
      injection_exit(-2);

    case omit:
      return MPI_SUCCESS;

    case fake_error:
      PMPI_Recv(buf, count, datatype, source, tag, comm, status);
      return MPI_ERR_COMM;

    case spin:
      for(;;) {}
      return -1;

    case UNSET:
      injection_printf("This should not be possible, injection made with UNSET payload");
      injection_exit(-2);

    default:
      injection_printf("Injection payload impl missing: '%s'\n", injection_payload_to_str(injection_target.payload));
      injection_exit(-1);
    }

    injection_printf("Unreachable code");
    injection_exit(-1);
    return -1;
  }

  return PMPI_Recv(buf, count, datatype, source, tag, comm, status);
}
