

#include "pmpi_injector_runtime.h"

#include <assert.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


injection_information_t injection_information;
injection_target_t injection_target;




static void
check_invocation(long long max)
{
  int my_rank;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (my_rank == injection_target.rank && max == 0) {
    injection_printf("Injection target rank %d did not perform operation in analysis", injection_target.rank);
    injection_exit(-1);
  }
  if (injection_target.invocation == -1) {
    injection_target.invocation = rand() % max;
    PMPI_Bcast(&injection_target.invocation, 1, MPI_LONG_LONG, injection_target.rank, MPI_COMM_WORLD);
  }
  if (my_rank == injection_target.rank && injection_target.invocation > max) {
    injection_printf("Injection target incovation is out of range: given %llu valid_range %d to %llu", injection_target.invocation, 0, max);
    injection_exit(-1);
  }
}


static void
check_argc(int idx, int argc, char* arg)
{
  assert(arg != NULL);

  if (idx+1 >= argc) {
    injection_printf("Argument specifier given without actual argument: '%s'", arg);
    injection_exit(-1);
  }
}


static void
remove_two_from_argv(int idx, int* argcp, char*** argvp)
{
  char** argv;
  int argc;
  int i;
  assert(argcp != NULL);
  assert(argvp != NULL);
  assert(idx < (*argcp));

  argv = *argvp;
  argc = *argcp;
  for (i=idx+2; i<argc; i++) {
    assert(argv[i-2] != NULL);
    assert(argv[i] != NULL);

    argv[i-2] = argv[i];
  }

  (*argcp) = argc - 2;
}


static void
parse_args(int* argcp, char*** argvp)
{
  int my_rank;
  int this_size;
  char** argv;
  int i;

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  PMPI_Comm_size(MPI_COMM_WORLD, &this_size);


  // init defaults
  injection_information.logfile = "pmpi_injector_log.txt";
  injection_information.exe = "NULL";
  injection_information.size = this_size;
  injection_information.sends = 0;
  injection_information.recvs = 0;
  injection_target.mode = analyze;
  injection_target.rank = -1;
  injection_target.invocation = -1;
  injection_target.payload = size_minus_one;

  if (argcp == NULL || argvp == NULL) {
    return;
  }

  injection_information.exe = injection_copy_str((*argvp)[0]);

  argv = *argvp;
  for (i=0; i<(*argcp); i++) {
    assert(argv[i] != NULL);

    // Logfile name
    if (strcmp(argv[i], "--injection-logfile") == 0) {
      check_argc(i, (*argcp), argv[i]);
      injection_information.logfile = injection_copy_str(argv[i+1]);
      remove_two_from_argv(i, argcp, argvp);
      i -= 1;
      continue;
    }


    // Mode
    if (strcmp(argv[i], "--injection-mode") == 0) {
      check_argc(i, (*argcp), argv[i]);
      injection_target.mode = str_to_injection_mode(argv[i+1]);
      remove_two_from_argv(i, argcp, argvp);
      i -= 1;
      continue;
    }


    // Target rank
    if (strcmp(argv[i], "--injection-target-rank") == 0) {
      check_argc(i, (*argcp), argv[i]);
      injection_target.rank = str_to_rank(argv[i+1]);
      remove_two_from_argv(i, argcp, argvp);
      i -= 1;
      continue;
    }


    // Target invocation
    if (strcmp(argv[i], "--injection-target-invocation") == 0) {
      check_argc(i, (*argcp), argv[i]);
      injection_target.invocation = str_to_invocation(argv[i+1]);
      remove_two_from_argv(i, argcp, argvp);
      i -= 1;
      continue;
    }


    // Payload
    if (strcmp(argv[i], "--injection-payload") == 0) {
      check_argc(i, (*argcp), argv[i]);
      injection_target.payload = str_to_injection_payload(argv[i+1]);
      remove_two_from_argv(i, argcp, argvp);
      i -= 1;
      continue;
    }
  }
}


int
MPI_Init(int* mpi_argc, char*** mpi_argv)
{
  int retval;
  int my_rank;
  int this_size;
  injection_information_t* info;

  retval = PMPI_Init(mpi_argc, mpi_argv);

  PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
  PMPI_Comm_size(MPI_COMM_WORLD, &this_size);


  // Parse arguments
  parse_args(mpi_argc, mpi_argv);


  // Print info
  if (my_rank == 0) {
    injection_printf("mode: %s", injection_mode_to_str(injection_target.mode));
    injection_printf("logfile: '%s'", injection_information.logfile);
  }


  // Early out for analyze
  if (injection_target.mode == analyze) {
    PMPI_Barrier(MPI_COMM_WORLD);
    return retval;
  }


  // We will be injecting, read the log.
  info = injection_read_logfile(injection_information.logfile);


  // Check exe name
  if (my_rank == 0 && strcmp(info->exe, (*mpi_argv)[0]) != 0) {
    injection_printf("Analysis file was made with a different executable: given '%s' expected '%s'", (*mpi_argv)[0], info->exe);
    injection_exit(-1);
  }


  // Check injection rank
  if (injection_target.rank == -1) {
    injection_target.rank = rand() % this_size;
    PMPI_Bcast(&injection_target.rank, 1, MPI_INT, 0, MPI_COMM_WORLD);
  }
  if (my_rank == 0 && info->size < injection_target.rank) {
    injection_printf("Injection target rank is out of range: given %d valid_range %d to %d", injection_target.rank, 0, info->size);
    injection_exit(-1);
  }


  // Check invocation based on the injected instruction type
  switch(injection_target.mode) {

  case(inject_allreduce):
    check_invocation(info->allreduces);
    break;

  case(inject_bcast):
    check_invocation(info->bcasts);
    break;

  case(inject_irecv):
    check_invocation(info->irecvs);
    break;

  case(inject_isend):
    check_invocation(info->isends);
    break;

  case(inject_recv):
    check_invocation(info->recvs);
    break;

  case(inject_reduce):
    check_invocation(info->reduces);
    break;

  case(inject_send):
    check_invocation(info->sends);
    break;

  case(inject_wait):
    check_invocation(info->waits);
    break;


    // We are injecting, not analyzing
  case(analyze):
    injection_printf("This code should be unreachable!");
    injection_exit(-2);


    // Catch new injection instruction targets that havent been implemented
  default:
    injection_printf("Injection mode impl missing: '%s'\n", injection_mode_to_str(injection_target.mode));
    injection_exit(-1);
  }


  // Print configuration
  if (my_rank == 0) {
    injection_printf("target_rank %d", injection_target.rank);
    injection_printf("target_invocation %lld", injection_target.invocation);
    injection_printf("payload '%s'", injection_payload_to_str(injection_target.payload));
  }


  // Wait until arguments have been parsed (may not be needed)
  PMPI_Barrier(MPI_COMM_WORLD);


  return retval;
}
