

#include "pmpi_injector_runtime.h"

#include <assert.h>
#include <mpi.h>
#include <stdio.h>



int
MPI_Finalize(void)
{
  int my_rank;
  FILE* fp;
  long long other_allreduces;
  long long other_bcasts;
  long long other_irecvs;
  long long other_isends;
  long long other_recvs;
  long long other_reduces;
  long long other_sends;
  long long other_waits;
  int i;

  // Wait until everyone is here
  PMPI_Barrier(MPI_COMM_WORLD);

  // Log only in analyze
  if (injection_target.mode == analyze) {
    PMPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

    // Log only in root
    if (my_rank == 0) {

      fp = fopen(injection_information.logfile, "w");
      assert(fp != NULL);

      // Executable name and world size
      assert(injection_information.exe != NULL);
      fprintf(fp, "%s\n", injection_information.exe);
      fprintf(fp, "%d\n", injection_information.size);


      // Function call counts for root
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_allreduce), injection_information.allreduces);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_bcast), injection_information.bcasts);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_irecv), injection_information.irecvs);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_isend), injection_information.isends);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_recv), injection_information.recvs);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_reduce), injection_information.reduces);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_send), injection_information.sends);
      fprintf(fp, "%d\t%s\t%llu\n", 0, injection_mode_to_str(inject_wait), injection_information.waits);

      // Function call counts for other ranks
      for (i=1; i<injection_information.size; i++) {
	PMPI_Recv(&other_allreduces, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_bcasts, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_irecvs, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_isends, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_recvs, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_reduces, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_sends, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
	PMPI_Recv(&other_waits, 1, MPI_LONG_LONG, i, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE);

	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_allreduce), other_allreduces);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_bcast), other_bcasts);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_irecv), other_irecvs);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_isend), other_isends);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_recv), other_recvs);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_reduce), other_reduces);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_send), other_sends);
	fprintf(fp, "%d\t%s\t%llu\n", i, injection_mode_to_str(inject_wait), other_waits);
      }

      fclose(fp);


    } else {
      // Send function call counts to root
      PMPI_Send(&injection_information.allreduces, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.bcasts, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.irecvs, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.isends, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.recvs, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.reduces, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.sends, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
      PMPI_Send(&injection_information.waits, 1, MPI_LONG_LONG, 0, 0, MPI_COMM_WORLD);
    }
  }

  return PMPI_Finalize();
}
