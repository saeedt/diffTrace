#ifndef PMPI_INJECTOR_RUNTIME_H
#define PMPI_INJECTOR_RUNTIME_H


#include <mpi.h>
#include <stddef.h>



typedef enum {
  analyze = 1,
  inject_allreduce,
  inject_bcast,
  inject_irecv,
  inject_isend,
  inject_recv,
  inject_reduce,
  inject_send,
  inject_wait,
} injection_mode;

typedef enum {
  UNSET=0,
  size_minus_one,
  rank_minus_one,
  root_minus_one,
  wrong_op,
  omit,
  fake_error,
  spin,
} injection_payload;

typedef struct {
  char* logfile;
  char* exe;
  int size;
  long long allreduces;
  long long bcasts;
  long long irecvs;
  long long isends;
  long long recvs;
  long long reduces;
  long long sends;
  long long waits;
} injection_information_t;

typedef struct {
  injection_mode mode;
  int rank;
  long long invocation;
  injection_payload payload;
} injection_target_t;




extern injection_information_t injection_information;

extern injection_target_t injection_target;




MPI_Op injection_different_op(MPI_Op op);

char* injection_copy_str(const char* str);

void injection_bcast_str(char** buf, int root);

void injection_exit(int retcode);

void injection_printf(const char *fmt, ...);

void* injection_xmalloc(size_t bytes);

char* injection_mode_to_str(injection_mode op);

injection_mode str_to_injection_mode(char* str);

char* injection_payload_to_str(injection_payload p);

injection_payload str_to_injection_payload(char* str);

int str_to_rank(char* str);

long long str_to_invocation(char* str);

injection_information_t* injection_read_logfile(char* filename);


#endif // PMPI_INJECTOR_RUNTIME_H

