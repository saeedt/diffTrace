import random

class diffMrrGraph:
	def __init__(self,name):
		self.name = name
		self.rndID = `int(random.random()*10000)`
		self.nodes = {}
		self.nodes[self.rndID+"s"] = "[label = \"{Start}\" , group=g0]"
		self.nodes[self.rndID+"f"] = "[label = \"{End}\" , group=g0]"
		self.edges = []
		self.eseq = []
		self.invisNodes = {}
	def addNodes(self,eseq,showC):
		self.eseq = eseq
		if len(eseq) <= 0:
			print "ERROR"
			sys.exit(-1)
		else:
			for i in range(0,len(eseq)):
				# Add C
				if len(eseq[i].c) != 0:
					nodeContent = "[label = \"{"
					if showC == 1:
						seq = specialCharFilter(eseq[i].c)
						for item in seq:
							nodeContent = nodeContent + item+"\\l"
					else:
						nodeContent = nodeContent + "hidden"
					nodeContent = nodeContent + "}\" , group=g0, color=green4]"
					self.nodes[self.rndID+"c"+`i`] = nodeContent
				else:
					if i != 0:
						print "BIG ERROR"
						break

				# Add A
				if len(eseq[i].a) == 0:
					self.invisNodes[self.rndID+"a"+`i`] = "[label = \"{garbage}\",group=g1 , color=blue, style=invis]"
				else:
					nodeContent = "[label = \"{"
					seq = specialCharFilter(eseq[i].a)
					for item in seq:
						nodeContent = nodeContent + item+"\\l"
					nodeContent = nodeContent + "}\" , group=g1, color=blue]"
					self.nodes[self.rndID+"a"+`i`] = nodeContent

				# Add B
				if len(eseq[i].b) == 0:
					self.invisNodes[self.rndID+"b"+`i`] = "[label = \"{garbage}\",group=g2 , color=red, style=invis]"
				else:
					nodeContent = "[label = \"{"
					seq = specialCharFilter(eseq[i].b)
					for item in seq:
						nodeContent = nodeContent + item+"\\l"
					nodeContent = nodeContent + "}\" , group=g2, color=red]"
					self.nodes[self.rndID+"b"+`i`] = nodeContent
	def addEdges(self):
		alist = []
		blist = []
		clist = []
		for item in self.nodes.keys():
			if item.startswith(self.rndID+'a'):
				alist.append(item)
			elif item.startswith(self.rndID+'b'):
				blist.append(item)
			elif item.startswith(self.rndID+'c'):
				clist.append(item)
			elif item.startswith(self.rndID+'s') or item.startswith(self.rndID+'f'):
				continue
			else:
				print "Error. Node started with something other than a,b,c,s or f"
		# from Start to others
		flg = [0,0,0]
		if self.rndID+"c0" in clist:
			self.edges.append("\""+self.rndID+"s\" -> \""+self.rndID+"c0\"")
			flg[0] = 1
		else:
			if self.rndID+"a0" in alist:
				self.edges.append("\""+self.rndID+"s\" -> \""+self.rndID+"a0\"")
				flg[1] = 1
			if self.rndID+"b0" in blist:
				self.edges.append("\""+self.rndID+"s\" -> \""+self.rndID+"b0\"")
				flg[2] = 1
		if (flg == [0,1,0] or flg == [0,0,1]) and self.rndID+"c1" in clist:
			self.edges.append("\""+self.rndID+"s\" -> \""+self.rndID+"c1\"")
		# from others to End

		#rest
		# Add invisible edges of As and Bs
		for i in range(0,len(self.eseq)-1):
			for case in [self.rndID+"a",self.rndID+"b"]:
				self.edges.append("\""+case+`i` + "\" -> " + "\""+ case+`i+1` + "\" [style = invis]")

		# Add edges from Cs to their As and Bs or the next C
		#print "addEdge c sort extractNodeID"
		clistSorted = sorted(clist,key = lambda k: int(extractNodeID(k)))
		for ll in range(0,len(clistSorted)):
			c = clistSorted[ll]
			#print "addEdge c extractNodeID"
			i = int(extractNodeID(c))
			if self.rndID+"a"+`i` in alist:
				self.edges.append("\""+c+"\" -> \""+self.rndID+"a"+`i`+"\"")
			if self.rndID+"b"+`i` in blist:
				self.edges.append("\""+c+"\" -> \""+self.rndID+"b"+`i`+"\"")
			if self.rndID+"b"+`i` not in blist or self.rndID+"a"+`i` not in alist:
				if self.rndID+"c"+`i+1` in clistSorted:
					self.edges.append("\""+c+"\" -> \""+self.rndID+"c"+`i+1`+"\"")
		# Add edges of As and Bs to the next Cs
		#print "addEdge a sort extractNodeID"
		alistSorted = sorted(alist,key = lambda k: int(extractNodeID(k)))
		for ll in range(0,len(alistSorted)):
			a = alistSorted[ll]
			#print "addEdge a extractNodeID"
			i = int(extractNodeID(a))
			if self.rndID+"c"+`i+1` in clist:
				self.edges.append("\"" +self.rndID+"a"+`i` +"\" -> \""+self.rndID+"c"+`i+1`+"\"")
		#print "addEdge b sort extractNodeID"
		blistSorted = sorted(blist,key = lambda k: int(extractNodeID(k)))
		for ll in range(0,len(blistSorted)):
			b = blistSorted[ll]
			#print "addEdge b extractNodeID"
			i = int(extractNodeID(b))
			if self.rndID+"c"+`i+1` in clist:
				self.edges.append("\"" +self.rndID+"b"+`i` +"\" -> \""+self.rndID+"c"+`i+1`+"\"")

		# END part
		flg =[0,0,0]
		if self.rndID+"a"+`len(self.eseq)-1` in alist:
			self.edges.append("\"" +self.rndID+"a"+`len(self.eseq)-1`+"\"" + " -> \""+self.rndID+"f"+"\"")
			flg[1] = 1
		if self.rndID+"b"+`len(self.eseq)-1` in blist:
			self.edges.append("\"" +self.rndID+"b"+`len(self.eseq)-1`+"\"" + " -> \""+self.rndID+"f"+"\"")
			flg[2] = 1
		if flg == [0,1,0] or flg == [0,0,1] or flg == [0,0,0]:
			self.edges.append("\"" +self.rndID+"c"+`len(self.eseq)-1`+"\"" + " -> \""+self.rndID+"f"+"\"")
	def toDot(self,isBuggy):
		if (isBuggy):
			s = "{\n\tnode[shape=record]\n\n\t"
		else:
			s = "{\n\tnode[shape=record style=dashed]\n\n\t"
		#print "toDot extractNodeID"
		for node,cont in sorted(self.nodes.items(),key=lambda k: extractNodeID(k[0])):
			#print node
			#print "toDot2 extractNodeID"
			if (node.startswith(self.rndID+"a") and self.rndID+"b"+extractNodeID(node) in self.nodes.keys()) or (node.startswith(self.rndID+"b") and self.rndID+"a"+extractNodeID(node) in self.nodes.keys()):
				s = s + "{rank = same ; \""+self.rndID+"a"+extractNodeID(node)+"\""+self.nodes[self.rndID+"a"+extractNodeID(node)]+" ; \""+self.rndID+"b" + extractNodeID(node)+"\""+self.nodes[self.rndID+"b"+extractNodeID(node)]+"}\n\t"
			else:
				s = s + "\"" + node + "\" " + cont + "\n\t"
		#print "toDot3 extractNodeID"
		for node,cont in sorted(self.invisNodes.items(),key=lambda k: extractNodeID(k[0])):
			#print node
			s = s + "\"" + node + "\" " + cont + "\n\t"

		s = s + "\n\t"
		for edge in self.edges:
			s = s + edge + "\n\t"
		s = s + "\n}"
		#print s
		return s


def extractNodeID(node):
	#print node
	if "a" in node:
		return node.partition("a")[2]
	elif "b" in node:
		return node.partition("b")[2]
	elif "c" in node:
		return node.partition("c")[2]
	elif "s" in node:
		return node.partition("s")[2]
	elif "f" in node:
		return node.partition("f")[2]
	else:
		print "ERROR in extractNodeID"
		return "-1"

class editSeq:
	def __init__(self,a,b,c):
		self.a = a
		self.b = b
		self.c = c
	def toString(self):
		s = "C: %s\n"%self.c
		s = s + "\tA: %s\n"%self.a
		s = s + "\tB: %s\n"%self.b
		return s

def processBuf(buf):
	ret = {}
	inserts = []
	deletes = []
	if len(buf) != 0:
		for item in buf:
			if item[0] == 1:
				#add to inserts
				for tt in [x.strip().strip("'") for x in item[1].partition("[")[2].rpartition("]")[0].split(",")] :
					inserts.append(tt)
			else:
				for tt in [x.strip().strip("'") for x in item[1].partition("[")[2].rpartition("]")[0].split(",")] :
					deletes.append(tt)
				#add to deletes
	ret["A"]=deletes
	ret["B"]=inserts
	return ret

def mergeCs(li):
	i = 0
	line = []
	while i < len(li):
		if li[i].startswith("C:"):
			#Check next ones, look for merging options
			j = i + 1
			#befc = [x.strip().strip("'") for x in li[i].rpartition("[")[2].partition("]")[0].split(",") if len(x) > 0]
			befc = [x.strip().strip("'") for x in li[i].partition("[")[2].rpartition("]")[0].split(",") if len(x) > 0]
			s = "C: ["
			while j < len(li)-1 and li[j].startswith("C:"):
				#print j
				#print len(li)
				#print "\t INSIDE WHILE\n\tLine[%d] to process: %s"%(j,li[j])
				toAdd = [x.strip().strip("'") for x in li[j].partition("[")[2].rpartition("]")[0].split(",") if len(x) > 0]
				for item in toAdd:
					befc.append(item)
				j = j + 1
			i = j
			i = j
			for k in range(0,len(befc)):
				#print befc[k]
				if k != len(befc) - 1:
					s = s + "'"+befc[k]+"',"
				else:
					s = s + "'"+befc[k]+"'"
			s = s + "]"
			#print "\tS: "+s
			line.append(s)
		else:
			line.append(li[i])
			i = i + 1
	# Removing empty Cs, As and Bs
	ret = []
	for item in line:
		if len(item.partition("[")[2].rpartition("]")[0]) != 0:
			ret.append(item)
	return ret

def edit2eseq(li):
	line = mergeCs(li)
	i = 0
	prevC = -1
	buf = []
	eseqObjs = []
	while i < len(line):
		#print "Line to process: %s"%(line[i])
		if not line[i].startswith("C:"):
			if line[i].startswith("B:"):
				buf.append((1,line[i]))
			elif line[i].startswith("A:"):
				buf.append((0,line[i]))
		elif line[i].startswith("C:"):
			if prevC == -1 and len(buf) != 0:
				# Empty C should be inserted
				# Create editSeq object using contents of buf (for A and B)
				a = processBuf(buf)["A"]
				b = processBuf(buf)["B"]
				c = []
				obj = editSeq(a,b,c)
				#print obj.toString()
				eseqObjs.append(obj)
				prevC = i
				buf = []
			elif prevC == -1 and len(buf) == 0:
				# edit starts with C, only update prevC, do nothing
				prevC = i
			elif prevC != -1 and len(buf) != 0:
				# Create editSeq object using prevC and contents of buf (for A and B)
				a = processBuf(buf)["A"]
				b = processBuf(buf)["B"]
				c = [x.strip().strip("'") for x in line[prevC].partition("[")[2].rpartition("]")[0].split(",") if len(x) > 0]
				prevC = i
				obj = editSeq(a,b,c)
				#print obj.toString()
				eseqObjs.append(obj)
				buf = []
			else:
				print "Error, two consequitive Cs in the edit"
				sys.exit(-1)
		else:
			print "Error, Line starts with something other than A, B or C"
			sys.exit(-1)
		i = i + 1

	if len(buf) == 0:
		a = []
		b = []
	else:
		a = processBuf(buf)["A"]
		b = processBuf(buf)["B"]
	if prevC != -1:
		c = [x.strip().strip("'") for x in line[prevC].partition("[")[2].rpartition("]")[0].split(",") if len(x) > 0]
	else:
		c = []
	obj = editSeq(a,b,c)
	#print obj.toString()
	eseqObjs.append(obj)
	return eseqObjs

def edit2dot(lcs,name,showC,isBuggy):
	li = [x.strip() for x in lcs.split("\n") if len(x) != 0]
	es = edit2eseq(li)
	dmg = diffMrrGraph(name)
	dmg.addNodes(es,showC)
	dmg.addEdges()
	return dmg.toDot(isBuggy)


def specialCharFilter(seq):
	special = ["}","{",">","<"]
	retSeq = []
	for item in seq:
		retItem = ""
		for i in range(0,len(item)):
			if item[i] in special:
				retItem = retItem + "-"
			else:
				retItem = retItem + item[i]
		retSeq.append(retItem)
	return retSeq
