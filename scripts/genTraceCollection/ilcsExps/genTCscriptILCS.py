#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
# Code: newGenSub.py
# Description: generates job submission scripts to run on Stampede for NAS applications with different flags {1/16,4/64,16/256,64/1024}

import toml
import argparse
import glob
import sys,subprocess

server = "psc"
tpn = "8"

def s_envSetup():
	srv = v["server"][server]
	s = ""
	s = s + "\n#ENV VAR SETUP\n"
	s = s + "export PIN_ROOT=" + srv["pin_root"] + "\n"
	s = s + "export PTOOL_ROOT="+srv["ptool_root"]+"\n"
	s = s + "export BUG_REPO="+srv["bug_repo"]+"\n"
	s = s + "export WORKSPACE="+srv["workspace"]+"\n"
	s = s + "export APPS="+srv["app_root"]+"\n"
	return s

# Insert the server-related configuration to the final output
def s_server_config(exp,proc): # tpn: task per nod
	srv = v["server"][server]
	s = ""
	s = s + "#!/bin/bash\n"
	for opt in srv["options"]:
		s = s + "#SBATCH " + opt + "\n"
	#print (proc)
	#print (tpn)
	#print (int(proc)/int(tpn))
	if proc == "27":
		s = s + "#SBATCH -N 3 \n"
		s = s + "#SBATCH --ntasks-per-node 9\n"
	else:
		s = s + "#SBATCH -N " + `int(proc)/int(tpn)` + "\n"
		s = s + "#SBATCH --ntasks-per-node "+tpn+"\n"
	s = s + "#SBATCH -t 00:10:00\n"
	s = s + "#SBATCH -o " + exp +  ".%j.output\n"
	s = s + "#SBATCH -e " + exp +  ".%j.output\n"
	s = s + "#SBATCH --mail-user=staheri@cs.utah.edu\n"
	s = s + "#SBATCH --mail-type=ALL\n\n"

	return s



# Generates and inserts the body of job (modules,executables,repitations,...)
def s_body(exp,binp,toCompile):
	ks=exp.split(".")
	print exp
	app = ks[0]
	bug = ks[1]
	img = ks[2]
	proc = ks[3]
	thr = ks[4]
	# Read compile and runtime of application
	if toCompile == "yes":
		s = "\n#COMPILE APP\n"
		s = s + "mkdir -p $WORKSPACE/"+exp+";\n"
		s = s + v["allApps"][app]["compiler"] + " "
		for src in v["allApps"][app][bug]["sources"]:
			s = s + v["allApps"][app]["path"]+"/"+src+" "
		s = s + " -o $WORKSPACE/"+exp+"/"+v["allApps"][app][bug]["exec"]+" "
		for fl in v["allApps"][app][bug]["compileFlags"]:
			s = s + fl + " "
		s = s + ";\n"
	else: # it means that the application is already compiled and we should copy exec to workspace
		s = "\n#COPY EXEC\n"
		s = s + "mkdir -p $WORKSPACE/"+exp+";\n"
		s = s + "cp " + v["allApps"][app]["path"]+"/"+v["allApps"][app]["exec"] + " "
		s = s + " $WORKSPACE/"+exp+"/ ;\n"
	# MOVE TO WORKSPACE
	s = s + "\n#MOVE TO WORKSPACE\n"
	s = s + "export CUR=$PWD;\n"
	s = s + "cd $WORKSPACE/"+exp+" ;\n"

	# RUN + Collect ALL Traces
	s = s + "\n# RUN + Collect Traces\n"
	if v["server"][server]["command"] == "mpirun -np":
		s = s + v["server"][server]["command"] + " " + proc
	else:
		s = s + v["server"][server]["command"]
	s = s + " $PIN_ROOT/pin -t $PTOOL_ROOT/parlot"+img+"/obj-intel64/parlot"+img+".so -- ./"+v["allApps"][app]["exec"]+" "

	for fl in v["allApps"][app]["input"]:
		s = s + fl + " "

	for fl in binp:
		s = s + fl + " "
	s = s + ";\n"

	s = s + "\n# CREATE & MOVE TRACES to BUG_REPO\n"
	s = s + "mkdir -p $BUG_REPO/"+exp+"/ptrace;\n"
	s = s + "mv parlot* $BUG_REPO/"+exp+"/ptrace;\n"

	s = s + "\n#Moving back and remove the workspace directory"
	s = s + "\ncd $CUR; \nrm -rf $WORKSPACE/"+exp+"/* ;\n"
	return s


# Generate the final SLURM script
def genScript(key,binp):
	ks=key.split(".")
	app = ks[0]
	bug = ks[1]
	img = ks[2]
	proc = ks[3]
	thr = ks[4]
	jobName = key
	if (server == "local"):
		s = "#!/bin/bash\n\n#RUN LOCALLY"
		s = s + s_envSetup()
		s = s + s_body(key,binp,toCompile="no")
		sub = open(pathToJobSub+"/"+jobName+".sh","w")
		sub.write(s)
		sub.close()
	else:
		s = s_server_config(jobName,proc)
		s = s + s_envSetup()
		s = s + s_body(key,binp,toCompile="no")
		sub = open (pathToJobSub+"/"+jobName+".slurm","w")
		sub.write(s)
		sub.close()






if len(sys.argv) != 6:
	print "USAGE:\n\t " +sys.argv[0]+" pathToJobSub bfunc bbug bproc bthread"
	sys.exit(-1)
conf = "configILCS.toml"
pathToJobSub = sys.argv[1]
app = "ilcsTSP"
bfunc = sys.argv[2]
bbug = sys.argv[3]
bproc = sys.argv[4]
bthread = sys.argv[5]
bug = bfunc+"-"+bbug+"-"+bproc+"-"+bthread
binp = [bfunc,bbug,bproc,bthread]
img = "m"
proc = "8"
thr = "auto"

exp_name = app + "." + bug + "." + img + "." + proc + "." + thr

# Create directory to stor jobSubmission scripts
#process = subprocess.Popen(["mkdir -p "+pathToJobSub+"/"+exp_name], stdout=subprocess.PIPE,shell=True)
#si, err = process.communicate()

# Read Config
config_file=open(conf)
v=toml.loads(config_file.read())
genScript(exp_name,binp)
