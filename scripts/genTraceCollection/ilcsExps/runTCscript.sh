#!/bin/bash

#print "USAGE:\n\t " +sys.argv[0]+" config-file pathToJobSub tool experiment_name #ofRuns psize node flag runName"
+sys.argv[0]+" pathToJobSub bfunc bbug bproc bthread"

python genTCscriptILCS.py $SCRATCH/jobSub nn nn nn nn;

for bf in ar1 ar2 ar3 ar4
do
	for bb in wo ws
	do
		for bp in 0 1 2 3 4 5 6 7 n0 n1 n2 n3 n4 n5 n6 n7 odd even all
		do
			python genTCscriptILCS.py $SCRATCH/jobSub $bf $bb $bp nn;
		done
	done
done

for bf in bc1 bc2
do
	for bb in wr ws
	do
		for bp in 0 1 2 3 4 5 6 7 n0 n1 n2 n3 n4 n5 n6 n7 odd even all
		do
			python genTCscriptILCS.py $SCRATCH/jobSub $bf $bb $bp nn;
		done
	done
done

for bf in mc1 mc2
do
	for bt in 0 1 2 n0 n1 n2 odd even all
	do
		for bp in 0 1 2 3 4 5 6 7 n0 n1 n2 n3 n4 n5 n6 n7 odd even all
		do
			python genTCscriptILCS.py $SCRATCH/jobSub $bf mc $bp $bt;
		done
	done
done

for bp in 0 1 2 3 4 5 6 7 n0 n1 n2 n3 n4 n5 n6 n7 odd even all
do
	python genTCscriptILCS.py $SCRATCH/jobSub mc3 mc $bp all;
done
