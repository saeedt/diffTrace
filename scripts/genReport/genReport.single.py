#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')

import glob
import sys,subprocess
import os
import numpy as np
import seaborn as sns ; sns.set(font_scale=0.7)
import matplotlib.pyplot as plt



from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict




	
if len(sys.argv) != 5:
	print "USAGE:\n\t " +sys.argv[0]+" base-path target-path freq-bins filter-mpi"
	sys.exit(-1)



#READ Matrix FILE
allData=[]
allLabel = []
dataSize = 2

basePath = sys.argv[1] if not sys.argv[1].endswith("/") else sys.argv[1][:-1]
targetPath=sys.argv[2] if not sys.argv[2].endswith("/") else sys.argv[2][:-1]
freq = sys.argv[3]
filt = sys.argv[4]
wsize = '1'
ov = '0'

app = basePath.split("/")[-2]
baseLabel = basePath.split("/")[-1].partition(".")[0]
targetLabel = targetPath.split("/")[-1].partition(".")[0]
gTitle = "App:"+app+", Frequency bin:"+freq+", filter-non-MPI: "+filt


# BASE
matFile = basePath+"/cl/"+wsize+"."+freq+"."+ov+"."+filt+"/"+basePath.rpartition("/")[2]+"."+wsize+"."+freq+"."+ov+"."+filt+".jacmat.txt"
print matFile
subplotLabel = baseLabel
try:
	data = open(matFile,"r").read().split("\n")[:-1]
	dataSize = len(data)
	mat = np.ones((len(data),len(data)))
	for i in range(0,len(data)):
		item = [x for x in data[i].split(",") if len(x) > 0]
		#print item
		for j in range(0,len(item)):
			if float(item[j]) != 1:
				mat[i][j] = float(item[j])
except Exception, e:
	print "ERROR %s"%e
	mat = np.zeros((dataSize,dataSize))
#allData[sys.argv[1].rpartition("/")[2]+"."+`wsize`+"."+`freq`+".1"] = mat
allData.append(mat)
allLabel.append(subplotLabel)

# TARGET
matFile = targetPath+"/cl/"+wsize+"."+freq+"."+ov+"."+filt+"/"+targetPath.rpartition("/")[2]+"."+wsize+"."+freq+"."+ov+"."+filt+".jacmat.txt"
print matFile
subplotLabel = targetLabel
try:
	data = open(matFile,"r").read().split("\n")[:-1]
	dataSize = len(data)
	mat = np.ones((len(data),len(data)))
	for i in range(0,len(data)):
		item = [x for x in data[i].split(",") if len(x) > 0]
		#print item
		for j in range(0,len(item)):
			if float(item[j]) != 1:
				mat[i][j] = float(item[j])
except Exception, e:
	print "ERROR %s"%e
	mat = np.zeros((dataSize,dataSize))
#allData[sys.argv[1].rpartition("/")[2]+"."+`wsize`+"."+`freq`+".1"] = mat
allData.append(mat)
allLabel.append(subplotLabel)


fig,axn = plt.subplots(1,2,sharex=True,sharey=True)
cbar_ax = fig.add_axes([.91, .3, .03, .4])
#cbar_ax = fig.add_axes()
for i, ax in enumerate(axn.flat):
	ax.set_title(allLabel[i],fontsize=7)
	#ax.set_title(allLabel[i])
	#sns.heatmap(allData[i],ax=ax)
	sns.heatmap(allData[i],ax=ax,cbar=i == 0, vmin=0, vmax=1,cbar_ax=None if i else cbar_ax)

#fig = ax.get_figure()
fig.suptitle(gTitle)
#fig.tight_layout()
fileName = "heatmap."+app+"."+baseLabel+"VS."+targetLabel+"."+wsize+"."+freq+"."+ov+"."+filt+".png"
fig.savefig(fileName)
