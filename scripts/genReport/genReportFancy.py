#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')
#matplotlib.use('tkagg')

import glob
import sys,subprocess
import os
import numpy as np
from scipy.cluster.hierarchy import fcluster
import seaborn as sns ; sns.set(font_scale=0.43)
#import seaborn as sns
import matplotlib.pyplot as plt



from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict




if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" matfile"
	sys.exit(-1)

matFile = sys.argv[1]

try:
	data = open(matFile,"r").read().split("\n")[:-1]
	dataSize = len(data)
	mat = np.ones((len(data),len(data)))
	for i in range(0,len(data)):
		item = [x for x in data[i].split(",") if len(x) > 0]
		#print item
		for j in range(0,len(item)):
			if float(item[j]) != 1:
				mat[i][j] = float(item[j])
except Exception, e:
	print "ERROR %s"%e
	mat = np.zeros((dataSize,dataSize))
print mat
# corr = np.corrcoef(mat)
# mask = np.zeros_like(corr)
# mask[np.triu_indices_from(mask)] = True
# with sns.axes_style("white"):
# 	ax = sns.heatmap(corr, mask=mask,vmin=0, vmax=1,annot=True, fmt="f")

tickLabels = []
tnum = 5
for j in range(0,len(mat)):
	tickLabels.append(`j/tnum`+"."+`j%tnum`)
#snsPlot = sns.clustermap(mat,xticklabels=tickLabels,yticklabels=tickLabels,metric="correlation")
snsPlot = sns.clustermap(mat)
# snsPlot = sns.clustermap(mat,\
# 	xticklabels=tickLabels,\
# 	yticklabels=tickLabels\
# 	)
#snsPlot.savefig("cl-buggy.pdf")

z = snsPlot.dendrogram_col.linkage
for item in z:
	for t in item:
		print "%.2f "%t,
	print "\n"
#print z
print len(snsPlot.dendrogram_col.linkage)
#print snsPlot.dendrogram_row.linkage

print "fcluster(z,2,criterion='maxclust')"
print fcluster(z,2,criterion='maxclust')

print "fcluster(z,3,criterion='maxclust')"
print fcluster(z,3,criterion='maxclust')

print "fcluster(z,4,criterion='maxclust')"
print fcluster(z,4,criterion='maxclust')

print "fcluster(z,5,criterion='maxclust')"
print fcluster(z,5,criterion='maxclust')

# print "fcluster(z,0.5,criterion='distance')"
# print fcluster(z,0.5,criterion='distance')
#
# print "fcluster(z,1,criterion='distance')"
# print fcluster(z,1,criterion='distance')
#
# print "fcluster(z,2,criterion='distance')"
# print fcluster(z,2,criterion='distance')
#
# print "fcluster(z,4,criterion='distance')"
# print fcluster(z,4,criterion='distance')
#
#
# print "fcluster(z,8,depth = 2)"
# print fcluster(z,8,depth = 2)
#
# print "fcluster(z,8,depth = 4)"
# print fcluster(z,8,depth = 4)
#
# print "fcluster(z,8,depth = 8)"
# print fcluster(z,8,depth = 8)
#
# print "fcluster(z,8,depth = 16)"
# print fcluster(z,8,depth = 16)
#
# print "fcluster(z,4,depth = 2)"
# print fcluster(z,4,depth = 2)
#
# print "fcluster(z,4,depth = 4)"
# print fcluster(z,4,depth = 4)
#
# print "fcluster(z,4,depth = 8)"
# print fcluster(z,4,depth = 8)
#
# print "fcluster(z,4,depth = 16)"
# print fcluster(z,4,depth = 16)
#
# print "fcluster(z,2,depth = 2)"
# print fcluster(z,2,depth = 2)
#
# print "fcluster(z,2,depth = 4)"
# print fcluster(z,2,depth = 4)
#
# print "fcluster(z,2,depth = 8)"
# print fcluster(z,2,depth = 8)
#
# print "fcluster(z,2,depth = 16)"
# print fcluster(z,2,depth = 16)
#
# print "fcluster(z,1,depth = 2)"
# print fcluster(z,1,depth = 2)
#
# print "fcluster(z,1,depth = 4)"
# print fcluster(z,1,depth = 4)
#
# print "fcluster(z,1,depth = 8)"
# print fcluster(z,1,depth = 8)
#
# print "fcluster(z,1,depth = 16)"
# print fcluster(z,1,depth = 16)
#
# print "fcluster(z,0.5,depth = 2)"
# print fcluster(z,0.5,depth = 2)
#
# print "fcluster(z,0.5,depth = 4)"
# print fcluster(z,0.5,depth = 4)
#
# print "fcluster(z,0.5,depth = 8)"
# print fcluster(z,0.5,depth = 8)
#
# print "fcluster(z,0.5,depth = 16)"
# print fcluster(z,0.5,depth = 16)
