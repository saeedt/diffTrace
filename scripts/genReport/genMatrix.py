#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
# Code: newGenSub.py
# Description: generates job submission scripts to run on Stampede for NAS applications with different flags {1/16,4/64,16/256,64/1024}

import toml
import argparse
import glob
import sys,subprocess
import time

if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" paths.txt"
	sys.exit(-1)


lines = open(sys.argv[1],"r").readlines()
for path in lines:
	cmd = "python cl2jacmat.py $HOME/diffTrace/bugRepo/"+path.strip()
	print cmd
	process = subprocess.Popen([cmd], stdout=subprocess.PIPE,shell=True)
	si, err = process.communicate()
	print si
	time.sleep(1)
