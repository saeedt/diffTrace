#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')

import glob
import sys,subprocess
import os
import numpy as np
import seaborn as sns ; sns.set(font_scale=0.2)
import matplotlib.pyplot as plt



from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict




	
if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" test_folder"
	sys.exit(-1)


#READ Matrix FILE
#allData={}
allData=[]
overlap = "1"
allLabel = []
dataSize = 2
for wsize in range(1,4):
	for freq in range(0,4):
		for ov in range (0,2):
			for filt in range (0,2):
				matFile = sys.argv[1]+"/cl/"+`wsize`+"."+`freq`+"."+`ov`+"."+`filt`+"/"+sys.argv[1].rpartition("/")[2]+"."+`wsize`+"."+`freq`+"."+`ov`+"."+`filt`+".jacmat.txt"
				print matFile
				subplotLabel = "wsize:%d, ftype:%d, ov:%d, nMPI-filter:%d"%(wsize,freq,ov,filt)
				try:
					data = open(matFile,"r").read().split("\n")[:-1]
					dataSize = len(data)
					mat = np.ones((len(data),len(data)))
					for i in range(0,len(data)):
						item = [x for x in data[i].split(",") if len(x) > 0]
						#print item
						for j in range(0,len(item)):
							if float(item[j]) != 1:
								mat[i][j] = float(item[j])
				except Exception, e:
					print "ERROR %s"%e
					mat = np.zeros((dataSize,dataSize))
				#allData[sys.argv[1].rpartition("/")[2]+"."+`wsize`+"."+`freq`+".1"] = mat
				allData.append(mat)
				allLabel.append(subplotLabel)

# fig,axn = plt.subplots(5,4,sharex=True,sharey=True)
# cbar_ax = fig.add_axes([.91, .3, .03, .4])
# for i, ax in enumerate(axn.flat):
# 	ax.set_title(allLabel[i],fontsize=3)
# 	#ax.set_title(allLabel[i])
# 	sns.heatmap(allData[i],ax=ax,cbar=i == 0, vmin=0, vmax=1,cbar_ax=None if i else cbar_ax)

fig,axn = plt.subplots(6,8,sharex=True,sharey=True)
cbar_ax = fig.add_axes([.91, .3, .03, .4])
#cbar_ax = fig.add_axes()
for i, ax in enumerate(axn.flat):
	ax.set_title(allLabel[i],fontsize=2.7)
	#ax.set_title(allLabel[i])
	#sns.heatmap(allData[i],ax=ax)
	sns.heatmap(allData[i],ax=ax,cbar=i == 0, vmin=0, vmax=1,cbar_ax=None if i else cbar_ax)

#fig = ax.get_figure()
#fig.suptitle(sys.argv[1].rpartition("/")[2])
#fig.tight_layout()
fig.savefig(sys.argv[1].rpartition("/")[2]+".pdf")
