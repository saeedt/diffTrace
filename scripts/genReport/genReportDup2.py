#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')
#matplotlib.use('tkagg')

import glob
import sys,subprocess
import os
import numpy as np
import seaborn as sns ; sns.set(font_scale=0.01)
import matplotlib.pyplot as plt



from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict

procs = 8
threads = 5

class subhmap():
	def __init__(self,path,bitTitle,oz):
		clCon = path.rpartition("/")[2].split(".")
		self.clMode = clCon[0]
		self.clFreq = clCon[1]
		self.clOv = clCon[2]
		self.filter = path.rpartition("/")[0].rpartition("/")[2]
		exCon = path.split("/")[-4].split(".")
		self.app = exCon[0]
		self.bug = exCon[1]
		self.img = exCon[2]
		self.proc = exCon[3]
		self.thread = exCon[4]
		self.subts = [self.app,self.bug,self.img,self.proc,self.thread,self.filter,self.clMode,self.clFreq,self.clOv]
		self.dlen = 0
		try:
			data = open(path,"r").read().split("\n")[:-1]
			dataSize = len(data)
			self.dlen = dataSize
			if oz == 0:
				matx = np.ones((len(data),len(data)))
				step = 1
			else:
				matx = np.ones((procs,procs))
				step = threads
			for i in range(0,len(data),step):
				item = [x for x in data[i].split(",") if len(x) > 0]
				#print item
				for j in range(0,len(item),step):
					if float(item[j]) != 1:
						matx[i/step][j/step] = float(item[j])
		except Exception, e:
			print "ERROR %s"%e
			matx = np.zeros((dataSize,dataSize))
		self.mat = matx
		subtitlesEnum = {0:"App",1:"Bug",2:"Img",3:"Proc",4:"Thread",5:"Filter",6:"clMode",7:"clFreq",8:"clOv"}
		st = ""
		for i in range(0,len(bitTitle)):
			if bitTitle[i] == '1':
				st = st + subtitlesEnum[i]+": "+self.subts[i]+ " "
		self.title = st
	def subtract(self,noBug):
		print "self before"
		print self.mat
		print "noBug"
		print noBug.mat
		self.mat = np.absolute(np.subtract(self.mat,noBug.mat))
		print "self after"
		print self.mat

class hmap():
	def __init__(self,subList,title,xdim,ydim):
		self.subMaps = subList
		self.title = title
		self.xdim = xdim / 2
		self.ydim = ydim * 2

	def draw(self,oz):
		#fig,axn = plt.subplots(self.xdim,self.ydim,sharex=True,sharey=True)
		fig,axn = plt.subplots(self.xdim,self.ydim)
		cbar_ax = fig.add_axes([.91, .38, .015, .25])
		#cbar_ax = fig.add_axes()
		for i, ax in enumerate(axn.flat):
			ax.set_title(self.subMaps[i].title,fontsize=1.1)
			ax.set_aspect("equal")
			tickLabels=[]
			if oz == 0:
				for j in range(0,len(self.subMaps[i].mat)):
					tickLabels.append(`j/threads`+"."+`j%threads`)
			else:
				for j in range(0,len(self.subMaps[i].mat)):
					tickLabels.append(`j`+".0")
			gt = sns.heatmap(self.subMaps[i].mat,ax=ax,cbar=i == 0, vmin=0, vmax=1,cbar_ax=None if i else cbar_ax,xticklabels=tickLabels,yticklabels=tickLabels)
			#gt.set_yticklabels(gt.get_yticklabels(),size='small')
			#gt.set_xticklabels(gt.get_xticklabels(),size='x-small')
		fig.savefig(self.title+".pdf")





def readConfig(config,onlyZero):
	subtitlesEnum = {0:"App",1:"Bug",2:"Img",3:"Proc",4:"Thread",5:"Filter",6:"clMode",7:"clFreq",8:"clOv"}
	conf=open(config,"r").readlines()
	xdim = int(conf[0].partition(":")[2].partition("*")[0])
	ydim = int(conf[0].partition(":")[2].partition("*")[2])
	title = conf[1].partition(":")[2]
	subtitleBitcode = conf[2].partition(":")[2]
	# Conf[3] is blank
	print ydim*xdim+4
	print len(conf)
	assert((ydim*xdim)+4 == len(conf))
	sublist = []
	for i in range(0,xdim):
		for j in range(0,ydim):
			shmp = subhmap(conf[(i*ydim+j)+4].strip(),subtitleBitcode,onlyZero)
			sublist.append(shmp)
	hmp = hmap(sublist,title.strip(),xdim,ydim)
	return hmp


def readConfig2(config,onlyZero,atrs,filter,title,subtitleBitcode):
	print "Reading config:\n%s\nTitle:%s\nFilter:%s\nAttributes:%s\n"%(config,title,filter,atrs)
	subtitlesEnum = {0:"App",1:"Bug",2:"Img",3:"Proc",4:"Thread",5:"Filter",6:"clMode",7:"clFreq",8:"clOv"}
	conf=open(config,"r").readlines()
	xdim = len(conf)-1
	ydim = len(atrs)
	db = {}
	sublist = []
	for path in conf:
		for atr in atrs:
			print path.strip()
			#print filter
			#print atr
			p = path.strip()+"/cl/"+filter+"/"+atr+".w.jacmat.txt"
			#print p
			shmp = subhmap(p,subtitleBitcode,onlyZero)
			db[path.strip().rpartition("/")[2].split(".")[1]]=shmp
			#print shmp.mat
			#sublist.append(shmp)
	for k,v in db.items():
		tshmp = v
		v.subtract(db["noBug"])
		if k != "noBug":
			sublist.append(v)
	hmp = hmap(sublist,title,xdim,ydim)
	return hmp

if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+"config"
	sys.exit(-1)

#onlyZero = 0
#figure = readConfig(sys.argv[1],onlyZero)
#figure.draw(onlyZero)


# MPI bugs
filters = ["01.1K10","01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","01.plt.0K10","11.1K10","11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10"]
mpiFilters = ["01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10"]
ompFilters = ["11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10"]
everythingFilters = ["01.1K10","01.plt.0K10","11.1K10","11.plt.cust.0K10"]

singAtrs = ["sing.orig","sing.log10","sing.actual"]
singAtrs2 = ["sing.orig"]
doubAtrs = ["doub.orig","doub.log10","doub.actual"]
statAtrs = ["stat.orig","stat.log10","stat.orig"]
allAtrs = ["sing.orig","sing.log10","sing.actual","doub.orig","doub.log10","doub.actual","stat.orig","stat.orig","stat.orig"]


# OMP
onlyZero = 0
subtBit = "010000110"
title = "ilcsTSP.ompBugs.m.8.1memOmpCitCustSingSub"
filter = ompFilters[1]
atrs = singAtrs2
figure = readConfig2(sys.argv[1],onlyZero,atrs,filter,title,subtBit)
figure.draw(onlyZero)
