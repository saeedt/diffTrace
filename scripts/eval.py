#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genCl.py
# Description: Generate concept lattice from a set of attributes



import glob
import sys,subprocess
import os
from tabulate import tabulate
import math
import time



def appendStat(tab):
	#minl = ["-","Min",`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`,`float("inf")`]
	minl = ["-","Min"]
	minl.extend([`float("inf")`] * (len(tab[0])-2))
	maxl = ["-","Max"]
	maxl.extend([`-1`] * (len(tab[0])-2))
	avgl = ["-","Avg"]
	avgl.extend([`0`] * (len(tab[0])-2))
	for item in tab:
		for i in range(2,len(item)):
			avgl[i] = `float(avgl[i]) + float(item[i])`
			minl[i] = `min(float(minl[i]),float(item[i]))`
			maxl[i] = `max(float(maxl[i]),float(item[i]))`

	for i in range(2,len(item)):
		avgl[i] = `float(avgl[i]) * 1.0 / len(tab)`
	tab.append(minl)
	tab.append(maxl)
	tab.append(avgl)
	return tab



def min(a,b):
	if a<b:
		return a
	else:
		return b

def max(a,b):
	if a<b:
		return b
	else:
		return a



par = {}
ser = {}
#
# Trace Length:830960
# Decompression Time:11014,0.011014
# Filter OMP Threads:24
# After Filter Length:830919
# Filter Time - CPU:350128,0.350128
# Filter Time - WALL:0.0826031
# NLR OMP Threads: 24
# NLR Length:458158
# NLR(overall) Length:458145
# NLR Time:34.6018
# NLR(Final Pass) Time:355.244
# NLR(overall) Time:389.846

# [0: Trace length, 1: Decompression Time, 2: filter OMP threads, 3: After Filter Length, 4: Filter CPU time,
#  5: Filter wall clock time, 6: NLR omp threads, 7: NLR parallel length, 8: NLR final length, 9: Parallel NLR time, 10: Final Pass NLR, 11: Overall NLR time]

for f in glob.glob("/home/saeed/diffTrace/bugRepo/lulesh1.noBug.m.8.auto/prep/*/parlog.txt"):
	filter = f.split("/")[-2].partition(".")[2]
	lines = [x.strip() for x in open(f,"r").readlines()]
	#print f
	#print filter
	inner = {}
	for i in range(0,len(lines),13):
		if not lines[i].startswith("+++"):
			print "error"
			break
		lt = []
		lt.append(lines[i+1].rpartition(":")[2]) # trace length
		lt.append(lines[i+2].rpartition(",")[2]) # decomp time
		lt.append(lines[i+3].rpartition(":")[2]) # filter omp threads
		lt.append(lines[i+4].rpartition(":")[2]) # after filter length
		lt.append(lines[i+5].rpartition(":")[2].rpartition(",")[2]) # filter cpu time
		lt.append(lines[i+6].rpartition(":")[2]) # filter wc time
		lt.append(lines[i+7].rpartition(":")[2]) # NLR omp threads
		lt.append(lines[i+8].rpartition(":")[2]) # NLR parallel length
		lt.append(lines[i+9].rpartition(":")[2]) # NLR final length
		lt.append(lines[i+10].rpartition(":")[2]) # NLR parallel time
		lt.append(lines[i+11].rpartition(":")[2]) # NLR final pass time
		lt.append(lines[i+12].rpartition(":")[2]) # NLR overall time
		inner[lines[i].partition("+++")[2]] = lt
	inner["ltab"] = int(open(f.rpartition("/")[0]+"/ltab.txt","r").readlines()[-1].partition(":")[0])+1
	inner["dtab"] = int(open(f.rpartition("/")[0]+"/dtab.txt","r").readlines()[-1].partition(":")[0])+1
	par[filter] = inner

for k,v in par.items():
	print k
	for kk,vv in v.items():
		print "\t%s"%kk
		print "\t>%s"%vv


#
# +++parlotm.r211.4683.0
# Trace Length:830960
# Decompression Time:16859,0.016859
# After Filter Length:415441
# Filter Time:73332,0.073332
# NLR Length:4932
# NLR Time19591667427,19591.7

for f in glob.glob("/home/saeed/diffTrace/bugRepo/lulesh1.noBug.m.8.auto/prep/*/serlog.txt"):
	filter = f.split("/")[-2].partition(".")[2]
	lines = [x.strip() for x in open(f,"r").readlines()]
	#print f
	print filter
	inner = {}
	if len(lines) < 220:
		continue
	for i in range(0,len(lines),7):
		if not lines[i].startswith("+++"):
			print "error"
			break
		lt = []
		lt.append(lines[i+1].rpartition(":")[2]) # trace length
		lt.append(lines[i+2].rpartition(",")[2]) # decomp time
		lt.append(lines[i+3].rpartition(":")[2]) # after filter length
		lt.append(lines[i+4].rpartition(",")[2]) # filter cpu time
		lt.append(lines[i+5].rpartition(":")[2]) # NLR length
		lt.append(lines[i+6].rpartition(",")[2]) # NLR time
		inner[lines[i].partition("+++")[2]] = lt
	inner["ltab"] = int(open(f.rpartition("/")[0]+"/ltab.txt","r").readlines()[-1].partition(":")[0])+1
	inner["dtab"] = int(open(f.rpartition("/")[0]+"/dtab.txt","r").readlines()[-1].partition(":")[0])+1
	ser[filter] = inner

tab = []
for k,v in ser.items():
	print "*********************\n%s\n**********************\n"%k
	for kk,vv in v.items():
		if kk != "dtab" and kk != "ltab":
			print "\t%s"%kk
			print "\t>%s"%vv
			print "\t>%s"%par[k][kk]
			ttab = []
			ttab.append(k) # Filter
			ttab.append(kk.split(".")[-2]+"."+kk.split(".")[-1]) # Trace
			ttab.append(vv[0]) # Len
			print vv[0]
			print par[k][kk][0]
			#assert(vv[0] == par[k][kk][0])
			ttab.append(vv[3]) # Serial Filter Time
			ttab.append(par[k][kk][5]) # Parallel Filter Time
			ttab.append(vv[2]) # After Filter Len
			#assert(vv[2] == par[k][kk][3])
			ttab.append(float(vv[3]) / float(par[k][kk][5])) # Filter Speedup
			ttab.append(vv[5]) # Serial NLR
			ttab.append(par[k][kk][9]) # Parallel NLR
			ttab.append(par[k][kk][11]) # Parallel F-NLR
			ttab.append(float(vv[5]) / float(par[k][kk][9])) # NLR Speedup
			ttab.append(float(vv[5]) / float(par[k][kk][11])) # F-NLR speedup
			ttab.append((float(vv[5]) + float(vv[3])) / (float(par[k][kk][9])+float(par[k][kk][5]))) # overall speedup
			ttab.append((float(vv[5]) + float(vv[3])) / (float(par[k][kk][11])+float(par[k][kk][5]))) # overall f-speedup
			if float(vv[4]) != 0.0:
				ttab.append(float(vv[2]) / float(vv[4])) # SerCR
			else:
				ttab.append("0.0")
			if float(par[k][kk][7]) != 0.0:
				ttab.append(float(par[k][kk][3]) / float(par[k][kk][7])) # ParCR
			else:
				ttab.append("0.0")
			tab.append(ttab)

#tab2 = [x for x in tab if (x[0].startswith("11.mpi") or x[0].startswith("01.mpi") or x[0].startswith("11.plt") or x[0].startswith("01.plt") ) and x[1].partition(".")[2] == "0"]
#tab2 = [x for x in tab if (x[0].startswith("11.mpi") or x[0].startswith("01.mpi")) and x[1].partition(".")[2] == "0"]
#tab2 = [x for x in tab if (x[0].startswith("11.omp") or x[0].startswith("01.omp")) and x[1].partition(".")[2] != "0"]
tab2 = [x for x in tab if x[0].startswith("11.mpiall.ompall") or x[0].startswith("01.mpiall.ompall")]
#tab2 = [x for x in tab if x[0].startswith("11.plt") or x[0].startswith("01.plt")]
#tab2 = [x for x in tab if (x[0].startswith("11.1") or x[0].startswith("01.1"))and x[1].partition(".")[2] == "0" ]
s = sorted(tab2, key = lambda x: int(x[2]), reverse = True)

hdrs = ["Filter","Trace", "Len", "Ser Fil T","Par Fil T","Fil Len","FIL SPDUP","Ser NLR","Par NLR","Par F-NLR","NLR SPDUP","F-NLR SPDUP","O Spdup","Of Spdup","SerCR","ParCR"]
fin = appendStat(s)
print tabulate(fin,headers=hdrs,tablefmt="fancy_grid")

for i in range(2,len(fin[-3])):
	print fin[-3][i]+",",
print ""
for i in range(2,len(fin[-2])):
	print fin[-2][i]+",",
print ""

for i in range(2,len(fin[-1])):
	print fin[-1][i]+",",
print ""


tab = []
for k,v in ser.items():
	#print "*********************\n%s\n**********************\n"%k
	for kk,vv in v.items():
		if kk == "dtab" or kk == "ltab":
			#print "\t%s"%kk
			#print "\t>%s"%vv
			#print "\t>%s"%par[k][kk]
			ttab = []
			ttab.append(k.partition("K")[0]) # Filter
			ttab.append(k.partition("K")[2]) # K
			ttab.append(kk) # dtab/ltab
			ttab.append(vv) # serial
			ttab.append(par[k][kk]) # Parallel
			tab.append(ttab)
tab2 = [x for x in tab if x[3] != x[4]]
#tab2 = [x for x in tab if x[0].startswith("11") and x[3] != x[4]]
s = sorted(tab2, key = lambda x: int(x[1]), reverse = True)
hdrs2 = ["Filter","K", "(l/d)tab","Serial","Parallel"]
print tabulate(s,headers=hdrs2,tablefmt="fancy_grid")
