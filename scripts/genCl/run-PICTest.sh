#!/bin/bash

mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 1100000000000001 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 1100000000000001 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 1100000000000001 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 1100000000000001 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 0110000000000000 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 0110000000000000 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 0110000000000000 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-1.noBug.m.8.1/ -f 0110000000000000 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 1100000000000001 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 1100000000000001 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 1100000000000001 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 1100000000000001 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 0110000000000000 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 0110000000000000 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 0110000000000000 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-2.noBug.m.8.1/ -f 0110000000000000 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 1100000000000001 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 1100000000000001 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 1100000000000001 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 1100000000000001 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 0110000000000000 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 0110000000000000 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 0110000000000000 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2-4.noBug.m.8.1/ -f 0110000000000000 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 1100000000000001 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 1100000000000001 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 1100000000000001 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 1100000000000001 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 0110000000000000 -k 40 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 0110000000000000 -k 50 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 0110000000000000 -k 80 -a 1 -q 0 -n 0 ;

sleep 1;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/pre-mpbpic2.noBug.m.8.1/ -f 0110000000000000 -k 100 -a 1 -q 0 -n 0 ;

sleep 1;

