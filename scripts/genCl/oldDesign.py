#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
# Code: traceCollcetion.py
# Description: Generates bash scripts for collecting ParLOT traces and decompressing them. Target application information should be stored in the file apps.toml. For more information, please refer to README.md

import toml
import argparse
import glob
import sys,subprocess

conf = {}
# Generate execution path for NAS
def genExecPath(common,flags,comp,top,app,psize):
	s = common+"/"+flags+"/"
	s = s + app+"."+psize+"."+top
	return s

# Taking out the last section of the path (job)
def ext(s):
	return s.rsplit("/")[-1]	

def s_common():
	s = "#!/bin/bash"
	s = s + "\n\n"
	s = s + "#SET PATHS\n"
	s = s + "export WORKSPACE=$DIFF_HOME/workspace;\n"
	s = s + "export APPS=$DIFF_HOME/applications;\n"
	s = s + "\n#ACTIVATE PARLOT\n"
	s = s + "export PIN_ROOT=$HOME/parlot/pin/pin3.5;\n"
	s = s + "export PATH=$PIN_ROOT:$PATH;\n"
	s = s + "export M_MUTATOR_PATH=$PIN_ROOT/source/tools/DBG17main/obj-intel64;\n"
	s = s + "export A_MUTATOR_PATH=$PIN_ROOT/source/tools/DBG17all/obj-intel64;\n"
	return s

# Generate script to run test series
def genRuntestScript(testName):
	# series.test.m(ain)/a(ll).#procs.#threads.input
	series = testName.split(".")[0]
	app = testName.split(".")[1]
	image = testName.split(".")[2]
	procs = testName.split(".")[3]
	threads = testName.split(".")[4]
	inp = testName.split(".")[5]

	v = conf["allApps"][series]
	vi = v[app]

	# Add common env vars
	s = s_common()

	# Making directory for traces in data folder
	s = s + "\n# Making DATA directory\n"
	s = s + "mkdir -p $DIFF_HOME/data/"+series+"/"+app+"."+image+"."+procs+"."+threads+"."+inp+"/trace ;\n"

	s = s + "\n# Making WORKSPACE directory\n"
	myworkspace = "$WORKSPACE/"+testName
	s = s + "mkdir -p "+myworkspace+";\n"

	# Compile App and redirect its output to working directory
	s = s + "\n#COMPILE APP\n"
	s = s + v["compiler"] + " "
	for src in vi["sources"]:
		s = s + v["path"]+"/"+src+" "
	s = s + " -o "+myworkspace+"/"+vi["exec"]+" "
	for fl in vi["compileFlags"]:
		s = s + fl + " "
	s = s + ";\n"

	# MOVE TO WORKSPACE
	s = s + "\n#MOVE TO WORKSPACE\n"
	s = s + "export CUR=$PWD;\n"
	s = s + "cd "+myworkspace+" ;\n"

	# RUN + Collect ALL Traces
	s = s + "\n# RUN + Collect Traces\n"
	if v["run"] == "mpirun -np":
		s = s + v["run"] + " " + procs
	else:
		s = s + v["run"]
	if image == "a":
		s = s + " pin -t $A_MUTATOR_PATH/DBGpin17allimages.so -- ./"+vi["exec"]+" "
	else:
		s = s + " pin -t $M_MUTATOR_PATH/DBGpin17mainimage.so -- ./"+vi["exec"]+" "
	for fl in vi["runFlags"]:
		s = s + fl + " "

	for fl in vi["input"]:
		s = s + fl + " "
		
	s = s + ";\n"
	s = s + "mv Hdbg* $DIFF_HOME/data/"+series+"/"+app+"."+image+"."+procs+"."+threads+"."+inp+"/trace ;\n"

	s = s + "\n#Moving back and remove the workspace directory"
	s = s + "\ncd $CUR; \nrm -rf "+myworkspace+" ;\n"
	
	# Decompressing traces and generate texTrace
	s = s + "\n# Decompressing traces and generate texTrace\n"
	s = s + "python $DIFF_HOME/traceOps/toText/trace2text.py $CLD_HOME/data/"+series+"/"+app+"."+image+"."+procs+"."+threads+"."+inp+";\n"
	s = s + "\n#--------\n"
	
	s = s + "\n#Moving back and remove the workspace directory"
	s = s + "\ncd $CUR; \nrm -rf "+myworkspace+" ;\n"

	scriptPath = "$DIFF_HOME/scripts/traceCollection/collectTrace/collect."+testName+".sh"
	fo = open (scriptPath,"w")
	fo.write(s)
	fo.close()
	return scriptPath


# Reads the configuration file and store the values in the Data Structure
if __name__ == '__main__':
        
	if len(sys.argv) != 2:
		print "USAGE:\n\t " +sys.argv[0]+" series.test.m(ain)/a(ll).#procs.#threads.input"
		sys.exit(-1)
	testName = sys.argv[1]

	config_file=open("$DIFF_ROOT/scripts/traceCollection/apps.toml")
	conf=toml.loads(config_file.read())
	collectScript = genRuntestScript(testName)
	# RUN collectScript.sh
	process = subprocess.Popen("chmod +x "+collectScript, stdout=subprocess.PIPE,shell=True)
	si, err = process.communicate()
	
	process = subprocess.Popen(collectScript, stdout=subprocess.PIPE,shell=True)
	si, err = process.communicate()
	print si
	
	
