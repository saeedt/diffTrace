#!/bin/bash

mkdir -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 0 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 2 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 3 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 0 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 2 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.adl-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 3 -n 0 ;

mkdir -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/cl;
mkdir -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 0 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 2 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 1 -q 3 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 0 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 2 -n 0 ;

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/oddEven.b-5-x-8.m.16.1/ -f 1100000100000000 -k 10 -a 2 -q 3 -n 0 ;

