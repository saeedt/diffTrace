#!/bin/bash

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-0-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-1-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-2-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-3-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-4-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-5-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-6-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-7-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-all-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-even-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n0-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n1-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n2-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n3-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n4-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n5-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n6-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-n7-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc1-mc-odd-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-0-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-1-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-2-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-3-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-4-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-5-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-6-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-7-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-all-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-even-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n0-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n1-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n2-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n3-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n4-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n5-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n6-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-n7-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-even.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n0.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n1.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-n2.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc2-mc-odd-odd.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-all-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-even-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n0-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n1-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n2-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n3-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n4-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n5-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n6-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-n7-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.mc3-mc-odd-all.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/cl;
mkdir -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/prep;

# Commands:

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 1111000100000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0111000000000010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0101000000010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0101000000001010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0101000000000110 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0111000100010010 -k 10 -a 1 -q 0 -n 0 &

../../clTrace/cltrace -m 2 -p $HOME/diffTrace/bugRepo/ilcsTSP.noBug.m.8.auto/ -f 0111000100000110 -k 10 -a 1 -q 0 -n 0 &

wait

