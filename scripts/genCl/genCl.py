#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
# Code: newGenSub.py
# Description: generates job submission scripts to run on Stampede for NAS applications with different flags {1/16,4/64,16/256,64/1024}

import toml
import argparse
import glob
import sys,subprocess

if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" config.txt"
	sys.exit(-1)

s = "#!/bin/bash\n\n"
lines = open(sys.argv[1],"r").readlines()
checks = []
for i in range(0,len(lines)):
	print lines[i]
	if lines[i].strip() == "x":
		checks.append(i)
print checks
cnt = 0
for path in lines[checks[3]+1:checks[4]]:
	#process = subprocess.Popen(["mkdir -p $HOME/diffTrace/bugRepo/"+path.strip()+"/cl"], stdout=subprocess.PIPE,shell=True)
	#si, err = process.communicate()
	s = s + "mkdir -p $HOME/diffTrace/bugRepo/"+path.strip()+"/cl;\n"
	#process = subprocess.Popen(["mkdir -p $HOME/diffTrace/bugRepo/"+path.strip()+"/prep"], stdout=subprocess.PIPE,shell=True)
	#si, err = process.communicate()
	s = s + "mkdir -p $HOME/diffTrace/bugRepo/"+path.strip()+"/prep;\n"
	s = s + "\n# Commands:\n\n"
	for nlrk in lines[checks[0]+1:checks[1]]:
		for bs in lines[checks[2]+1:checks[3]]:
			for aqn in lines[checks[1]+1:checks[2]]:
				atra = aqn.split(" ")[0]
				atrq = aqn.split(" ")[1]
				atrn = "0"
				cmd = "../../parCltrace/cltrace -m 2"
				cmd = cmd + " -p $HOME/diffTrace/bugRepo/"+path.strip()
				cmd = cmd + "/ -f "+bs.strip()
				cmd = cmd + " -k " +nlrk.strip()
				cmd = cmd + " -a " +atra.strip()
				cmd = cmd + " -q " +atrq.strip()
				cmd = cmd + " -n " +atrn.strip() +"&"
				s = s + cmd + "\n\n"
				cnt = cnt + 1
				if cnt%20==0:
					s = s + "wait\n\n"
				#s = s + cmd + "\n\nsleep 1;\n\n"
				#print cmd
				#process = subprocess.Popen([cmd], stdout=subprocess.PIPE,shell=True)
				#si, err = process.communicate()
				#print si


fo = open("run-s-"+sys.argv[1].rpartition(".")[0]+".sh","w")
fo.write(s)
fo.close()
