#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genCl.py
# Description: Generate concept lattice from a set of attributes



import glob
import sys,subprocess
import os
from tabulate import tabulate
import math
import time



if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" texTrace"
	sys.exit(-1)

trace = sys.argv[1]

infof = trace.rpartition(".")[0].rpartition(".")[0]+".info.txt"
print infof

fopen = open(trace,"r")
data = fopen.read().split(",")[:-1]
fopen = open(infof,"r")
info = fopen.readlines()[2:]
info[0] = "[ret]"

for item in data:
	#print item
	print info[int(item)]
