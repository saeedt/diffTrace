#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: gdiff.py
# Description: generate graph diff of two dot files


import glob
import sys,subprocess
import os
import numpy as np

from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict

def labelGen(label):
	if len(label.split("|")) == 6:
		s = "\"" + label.split("|")[1] + " * " +label.split("|")[3] + "\\l" + label.split("|")[4] + "\""
	else:
		s = label
	return s

if len(sys.argv) != 4:
	print "USAGE:\n\t " +sys.argv[0]+" dotA dotB out"
	sys.exit(-1)

dota = [ x for x in open(sys.argv[1],"r").readlines() if "->" in x]
dotb = [ x for x in open(sys.argv[2],"r").readlines() if "->" in x]

outfile = open(sys.argv[3]+".dot","w")

bnodes = set([])
anodes = set([])
aedge = {}
bedge = {}

for item in dota:
	src = item.partition("->")[0].strip()
	dest = item.partition("->")[2].partition("[label")[0].strip()
	label= item.partition("[label =")[2].partition("]")[0].strip().strip("\"")
	edge = labelGen(src) + " -> " + labelGen(dest)
	anodes.add(src)
	anodes.add(dest)
	aedge[edge]=label
	#print "item: %s\nsource:%s \ndest:%s\nlabel:%s\n***\n"%(item,src,dest,label)

for item in dotb:
	src = item.partition("->")[0].strip()
	dest = item.partition("->")[2].partition("[label")[0].strip()
	label= item.partition("[label =")[2].partition("]")[0].strip().strip("\"")
	edge = labelGen(src) + " -> " + labelGen(dest)
	bnodes.add(src)
	bnodes.add(dest)
	bedge[edge]=label
	#print "item: %s\nsource:%s \ndest:%s\nlabel:%s\n***\n"%(item,src,dest,label)

s = "digraph g{\n\t"

cnodes = set([])
cedges = []

for item in bnodes.intersection(anodes): # a union b
	st = labelGen(item) + " [label = " + labelGen(item) + ", color = black]"
	print labelGen(item)
	s = s + st + "\n\t"
	cnodes.add(st)

for item in anodes.difference(bnodes):
	st = labelGen(item) + " [label = " + labelGen(item) + ", color = blue]"
	print labelGen(item)
	s = s + st + "\n\t"
	cnodes.add(st)

for item in bnodes.difference(anodes):
	st = labelGen(item) + " [label = " + labelGen(item) + ", color = red]"
	print labelGen(item)
	s = s + st + "\n\t"
	cnodes.add(st)

s = s + "\n"
for edge,label in aedge.items():
	#print "aedge[%s] =  %s\n"%(edge,label)
	if edge in bedge.keys():
		if label == bedge[edge]:
			st = edge + " [label = " + label + ", color = black]"
			s = s + st + "\n\t"
			cedges.append(st)
		else:
			st = edge + " [label = " + label + ", color = blue]"
			s = s + st + "\n\t"
			cedges.append(st)
			st = edge + " [label = " + bedge[edge] + ", color = red]"
			s = s + st + "\n\t"
			cedges.append(st)
	else:
		st = edge + " [label = " + label + ", color = blue]"
		s = s + st + "\n\t"
		cedges.append(st)

for edge,label in bedge.items():
	if edge not in aedge.keys():
		st = edge + " [label = " + label + ", color = red]"
		s = s + st + "\n\t"
		cedges.append(st)

s = s + "\n}"

outfile.write(s)
outfile.close()

print "Write outdot file to %s.dot"%sys.argv[3]

process = subprocess.Popen("dot -Tpdf "+sys.argv[3]+".dot -o "+sys.argv[3]+".pdf", stdout=subprocess.PIPE,shell=True)
si, err = process.communicate()

print "Generating PDF file: %s.pdf"%sys.argv[3]

#print s
