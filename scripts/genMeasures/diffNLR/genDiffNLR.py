#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: cmrx.py
# Description: Read texTrace and convert them to MRR (minimal reptetive representation)

import glob
import sys,subprocess
from tabulate import tabulate
import math
import re
from sets import Set
import mrrCore as mrr
import diffCore as diff
import newVis as nvis
import toml

conf={}
mpiFilters = ["11.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10"]
def loopExpansion(l,ltab,other):
	ret = []
	for item in l:
		ls = item.partition("^")
		if len(ls[1]) > 0 and len(ls[2]) > 0 :
			# it is a loop,
			# if the current loop is equal to other : do not do anything
			# else replace it
			if ls[0][1:] in other.keys():
				if ltab[ls[0][1:]] == other[ls[0][1:]]:
					# no need to replace, append whole item
					ret.append(item)
				else:
					#replace it
					for lc in range(0,int(ls[2])):
						for lb in ltab[ls[0][1:]]:
							ret.append(lb)
			else:
				#replace it
				for lc in range(0,int(ls[2])):
					for lb in ltab[ls[0][1:]]:
						ret.append(lb)

		else:
			# no need to replace, append item
			ret.append(item)
	return ret

def readLtab(lf):
	ret = {}
	lines = [x for x in open(lf,"r").readlines()]
	for l in lines:
		lid = l.partition(":")[0]
		lbs = l.partition(":")[2].strip().split(" - ")
		ret[lid] = lbs
	return ret



def ltab2html(ltab,pre):
	lines = [x for x in open(ltab,"r").readlines()]
	s = "{\n\n\t"
	s = s + pre+"HtmlTable [\n\t\t"
	s = s + "shape=plaintext\n\t\t"
	if pre == "b":
		s = s + "color=Red\n\t\t"
	elif pre == "nb":
		s = s + "color=Blue\n\t\t"
	s = s + "label=<\n\t\t\t"
	s = s + "<table border=\'0\' cellborder=\'1\'>\n\t\t\t\t "
	s = s + "<tr><td> Loop </td> <td> Body"
	if pre == "b":
		s = s + " (buggy)"
	elif pre == "nb":
		s = s + " (noBug)"
	s = s + " </td></tr>\n\t\t\t\t "
	for line in lines:
		s = s + "<tr><td> L"+line.partition(":")[0]+" </td> <td> "+line.partition(":")[2].strip()+" </td></tr>\n\t\t\t\t "
	s = s + "</table>\n\t>];\n}"
	return s



if len(sys.argv) != 3:
	print "USAGE:\n\t " +sys.argv[0]+"app PT_number"
	sys.exit(-1)

expPath = "/home/saeed/diffTrace/bugRepo/"+sys.argv[1]
app = sys.argv[1].split(".")[0]
bug = sys.argv[1].split(".")[1]
img = sys.argv[1].split(".")[2]
proc = sys.argv[1].split(".")[3]
thr =  sys.argv[1].split(".")[4]
pt = int(sys.argv[2])
#outPath = sys.argv[3]


# Create a directory under "results/outPath"
# For each filter under appPath/prep:
# 	find the corresponding pt
#	find the corresponding bug-free
# 	do Loop extenstion for both
#	create dot file
#	Create pdf

process = subprocess.Popen("mkdir -p results/"+bug+"/dots", stdout=subprocess.PIPE,shell=True)
si, err = process.communicate()

for filt in glob.glob(expPath+"/prep/*/"):
	print filt

	flt = filt[:-1].rpartition("/")[2]
	print "flt"+flt
	#if "mpi" not in flt:
	#	continue
	files = sorted(glob.glob(filt+"/parlot*"),key = lambda x: (int(x.rpartition("/")[2].split(".")[-3]) , int(x.rpartition("/")[2].split(".")[-2]) ))
	nbPath = "/home/saeed/diffTrace/bugRepo/"+app+".noBug."+img+"."+proc+"."+thr+"/prep/"+flt
	nbFiles = sorted(glob.glob(nbPath+"/parlot*"),key = lambda x: (int(x.rpartition("/")[2].split(".")[-3]) , int(x.rpartition("/")[2].split(".")[-2]) ))
	pathA = nbFiles[pt]
	pathB = files[pt]

	pathAltab = readLtab(pathA.rpartition("/")[0]+"/ltab.txt")
	pathBltab = readLtab(pathB.rpartition("/")[0]+"/ltab.txt")

	pathAexpanded = loopExpansion([x.strip() for x in open(pathA,"r").readlines() if len(x) > 0],pathAltab,pathBltab)
	pathBexpanded = loopExpansion([x.strip() for x in open(pathB,"r").readlines() if len(x) > 0],pathBltab,pathAltab)

	nm = flt+"_"+pathA.rpartition("/")[2].rpartition(".")[0]+"_"+pathB.rpartition("/")[2].rpartition(".")[0]
	print nm
	fe = diff.lcs(pathAexpanded,pathBexpanded)
	print fe
	#ltab = ltab2html(pathA.rpartition("/")[0]+"/ltab.txt","b")
	#ltab_nb = ltab2html(pathB.rpartition("/")[0]+"/ltab.txt","nb")
	diffNLR = nvis.edit2dot(fe,nm,1)

	finalDot= "digraph \"diffNLR\""+diffNLR+"\n\t"
	#finalDot= finalDot +"subgraph diffNLR"+diffNLR+"\n\t"
	#finalDot= finalDot +"subgraph ltab_nb"+ltab_nb+"\n\t"
	#finalDot= finalDot +"}"

	f = open("results/"+bug+"/dots/"+nm+".dot","w")
	f.write(finalDot)
	f.close()
	process = subprocess.Popen("dot -Tpdf "+"results/"+bug+"/dots/"+nm+".dot -o "+"results/"+bug+"/"+flt+"-"+`pt`+".pdf", stdout=subprocess.PIPE,shell=True)
	si, err = process.communicate()
