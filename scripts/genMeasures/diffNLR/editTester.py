#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: cmrx.py
# Description: Read texTrace and convert them to MRR (minimal reptetive representation)

import glob
import sys,subprocess
from tabulate import tabulate
import math
import re
from sets import Set
import mrrCore as mrr
import diffCore as diff
import visdiffCore as visd

gftab = {}

def stack2string(stack):
	s = "["
	for item in stack:
		s = s + item + "."
	s = s + "]"
	return s
def get_gftab(item):
	#print item
	if item in gftab.keys():
		return gftab[item]
	else:
		gftab[item] = len(gftab)
		return gftab[item]

def summary(data,info):
	pattern1 = re.compile(r'^MPI\w*@plt$')
	#pattern1 = re.compile(r'^\w*$')
	pattern2 = re.compile(r'^poll\w*@plt$')
	pattern3 = re.compile(r'\w*free\w*')
	stack = []
	buf = []
	ret = []
	for item in data:
		#if info[int(item)] == ".plt":
		#	continue
		#print "item to check: %s"%info[int(item)]
		#if pattern1.match(info[int(item)]):
		if pattern1.match(info[int(item)]) or pattern2.match(info[int(item)]) or pattern1.match(info[int(item)]):
			gid = get_gftab(info[int(item)])
			#print "\tMatches with regex: %s"%info[int(item)]

			sst = stack2string(stack)
			#print "\tStacksnapshot (before): %s"%(sst)
			s = ""
			#for itemd in buf:
			#	s = s + itemd + ","
			buf = []
			ret.append("X")
			#ret.append(`hash(s)`)
			#ret.append(sst + "." + info[int(item)])
			ret.append(info[int(item)])
			stack.append(`gid`)
			#print "\tStacksnapshot (after): %s"%(stack2string(stack))
			#input("regex match")
			#flush out the stack
		else:
			#print "\tDid not matche with regex"
			if item == "0":
				##print "\t\tIt is return"
				#print "\t\tStacksnapshot (before): %s"%(stack2string(stack))
				stack = stack[:-1]
				#print "\t\tStacksnapshot (after): %s"%(stack2string(stack))
			else:
				gid = get_gftab(info[int(item)])
				#s = stack2string(stack)
				#s = s + "." + info[int(item)]
				#s = s + "." + `gid`
				#buf.append(s)
				stack.append(`gid`)
				#print "\t\tXXX stack snap: %s"%(stack2string(stack))
	if len(buf) != 0:
		s = ""
		#for itemd in buf:
		#	s = s + itemd + ","
		ret.append("X")
		#ret.append(`hash(s)`)
	return ret


if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" EditFile"
	sys.exit(-1)

fo = open(sys.argv[1],"r").readlines()
s = ""
for f in fo:
	s = s + f + "\n"

visd.edit2dot(s,sys.argv[1]+".dot")
process = subprocess.Popen("dot -Tpdf "+sys.argv[1]+".dot -o "+sys.argv[1]+".pdf", stdout=subprocess.PIPE,shell=True)
si, err = process.communicate()
		