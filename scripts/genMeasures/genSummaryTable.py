#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')
#matplotlib.use('tkagg')

import glob
import sys,subprocess
import os
import numpy as np
from scipy.cluster.hierarchy import fcluster
import seaborn as sns ; sns.set(font_scale=0.43)
#import seaborn as sns
import matplotlib.pyplot as plt

from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict


topn = 6
topCandidates = 6
threadNum = 5
procNum = 8

path = "/home/saeed/diffTrace/bugRepo"
rpath = "/home/saeed/diffTrace/results/diffNLR"


hdrs = ["Level","Bug","Buggy Process","Buggy Thread","Filter","Candidate Susupicious Process(es)","Candidate Susupicious Thread(s)","Description"]
tab = []

hdrs2 = ["Level","Bug","BP","BT","Filter","Attribute","KK","P","T"]
for rprt in sorted(glob.glob("results/ilcsTSP.ar1*/*.txt")):
	report = rprt.split("/")[1]
	filter = rprt.split("/")[2].rpartition(".")[0]
	app = report.split(".")[0]
	bugg = report.split(".")[1]
	bug = bugg.split("-")[0]+"-"+bugg.split("-")[1]
	bproc = bugg.split("-")[2]
	bthr = bugg.split("-")[3]
	if bug.startswith("a") or bug.startswith("b"):
		level = "MPI"
	elif bug.startswith("mc"):
		level = "OMP"
	atr = "-"
	kk = "-"
	sproc = ""
	sthr = ""

	repLines = open(rprt,"r").readlines()
	susp = []
	i = 1
	while i<len(repLines):
	#for i in range(1,len(repLines)):
	#for line in repLines[1:]:
		susp = []
		line = repLines[i]
		if line.strip().startswith("TOP"):
			break
		else:
			#print `len(line.split())` + " ",
			#print line
			if line.startswith("01") or line.startswith("11"):
				atr = line.split()[1]
				kk = line.split()[2].strip()
				susp = [int(x) for x in line.rpartition(":")[2].partition("{")[2].partition("}")[0].split()]
				j = i+1
				#print "i: %d, Line starts with 01 or 11: %s"%(i,line.split())
				#print atr + " <> " + kk
				#print susp
				#print "j:"+`j`
				#print repLines[j]
				while (not  repLines[j].strip().startswith("TOP") and not repLines[j].startswith("01") and not repLines[j].startswith("11")):
					#print "j: %d, Other Line: %s"%(j,repLines[j].split())
					lll = repLines[j]
					for ss in lll.rpartition(":")[2].partition("{")[2].partition("}")[0].split():
						susp.append(int(ss))
					#print susp
					j = j + 1

				i = j
				#print susp
				#input()
				sproc = ""
				sthr = ""
				for ss in susp:
					if ss%threadNum == 0:
						# it is a process
						sproc = sproc + `ss/threadNum` +" "
					else:
						#it is a thread
						sthr = sthr + `ss/threadNum`+"."+`ss%threadNum` +" "
				ttab = []
				ttab.append(level)
				ttab.append(bug)
				ttab.append(bproc)
				ttab.append(bthr)
				ttab.append(filter)
				ttab.append(atr)
				ttab.append(kk)
				ttab.append(sproc)
				ttab.append(sthr)
				tab.append(ttab)
				#print "ADDED TO TABLE"
				#print tabulate(tab,headers=hdrs2,tablefmt="moinmoin")

print tabulate(tab,headers=hdrs2,tablefmt="fancy_grid")
#input()
# for report in sorted(glob.glob("results/ar*/*.txt")):
# 	print report
# 	app = report.split(".")[0]
# 	bugg = report.split(".")[1]
# 	bug = bugg.split("-")[0]+"-"+bugg.split("-")[1]
# 	level = ""
# 	susp=""
# 	sust=""
# 	if bug.startswith("a") or bug.startswith("b"):
# 		level = "MPI"
# 	elif bug.startswith("mc"):
# 		level = "OMP"
# 	bproc = bugg.split("-")[2]
# 	bthr = bugg.split("-")[3]
# 	filt = ""
# 	#tab = []
# 	#read text
# 	fi = open(report,"r")
# 	lines = fi.readlines()
# 	if level == "OMP":
# 		for line in lines:
# 			if line.startswith("01") or line.startswith("11"):
# 				filt = line.split()[0]
# 				print "Main Line"
# 				print line.split()
# 				if susp != "" or sust != 0:
# 					ttab = []
# 					ttab.append(level)
# 					ttab.append(bug)
# 					if bproc.startswith("n"):
# 						ttab.append("All processes except "+bproc[1:])
# 					elif bproc == "odd":
# 						ttab.append("1-3-5-7")
# 					elif bproc == "even":
# 						ttab.append("0-2-4-6")
# 					elif bproc == "all":
# 						ttab.append("All processes")
# 					else:
# 						ttab.append(bproc)
# 					if bthr.startswith("n"):
# 						ttab.append("All threads except "+bthr[1:])
# 					elif bthr == "odd":
# 						ttab.append("1-3-5...")
# 					elif bthr == "even":
# 						ttab.append("0-2-4...")
# 					elif bthr == "all":
# 						ttab.append("All threads")
# 					else:
# 						ttab.append(bthr)
# 					ttab.append(filt)
# 					ttab.append(susp)
# 					ttab.append(sust)
# 					tab.append(ttab)
# 					ttab = []
# 					susp=""
# 					sust=""
# 				#print "HEAD",
# 				if len(line.split()) == 3:
# 					susp = susp+`int(line.split()[1].split("-")[1])/threadNum`+"-"
# 					sustmp = `int(line.split()[2].split("-")[1])/threadNum`+"."+`int(line.split()[2].split("-")[1])%threadNum`
# 					sust = sust+sustmp+"-"
# 				elif len(line.split()) == 2:
# 					if int(line.split()[1].split("-")[1])%threadNum == 0:
# 						susp = susp+`int(line.split()[1].split("-")[1])/threadNum`+"-"
# 					else:
# 						sustmp = `int(line.split()[1].split("-")[1])/threadNum`+"."+`int(line.split()[1].split("-")[1])%threadNum`
# 						sust = sust+sustmp+"-"
# 				else:
# 					print "nothing to do"
# 			else:
# 				print "Other Lines"
# 				print line.split()
# 				if len(line.split()) == 2:
# 					susp = susp+`int(line.split()[0].split("-")[1])/threadNum`+"-"
# 					sustmp = `int(line.split()[1].split("-")[1])/threadNum`+"."+`int(line.split()[1].split("-")[1])%threadNum`
# 					sust = sust+sustmp+"-"
# 				elif len(line.split()) == 1:
# 					if int(line.split()[0].split("-")[1])%threadNum == 0:
# 						susp = susp+`int(line.split()[0].split("-")[1])/threadNum`+"-"
# 					else:
# 						sustmp = `int(line.split()[0].split("-")[1])/threadNum`+"."+`int(line.split()[0].split("-")[1])%threadNum`
# 						sust = sust+sustmp+"-"
# 				else:
# 					print "nothing to do"
# 				#print line.split()
# 		if susp != "" or sust != "":
# 			ttab = []
# 			ttab.append(level)
# 			ttab.append(bug)
# 			if bproc.startswith("n"):
# 				ttab.append("All processes except "+bproc[1:])
# 			elif bproc == "odd":
# 				ttab.append("1-3-5-7")
# 			elif bproc == "even":
# 				ttab.append("0-2-4-6")
# 			elif bproc == "all":
# 				ttab.append("All processes")
# 			else:
# 				ttab.append(bproc)
# 			if bthr.startswith("n"):
# 				ttab.append("All threads except "+bthr[1:])
# 			elif bthr == "odd":
# 				ttab.append("1-3-5...")
# 			elif bthr == "even":
# 				ttab.append("0-2-4...")
# 			elif bthr == "all":
# 				ttab.append("All threads")
# 			else:
# 				ttab.append(bthr)
# 			ttab.append(filt)
# 			ttab.append(susp)
# 			ttab.append(sust)
# 			tab.append(ttab)
# 			ttab = []
# 			susp=""
# 			sust=""
#
# 	if level == "MPI":
# 		for line in lines:
# 			if line.startswith("01") or line.startswith("11"):
# 				filt = line.split()[0]
# 				print "Main Line"
# 				print line.split()
# 				if susp != "":
# 					ttab = []
# 					ttab.append(level)
# 					ttab.append(bug)
# 					if bproc.startswith("n"):
# 						ttab.append("All processes except "+bproc[1:])
# 					elif bproc == "odd":
# 						ttab.append("1-3-5-7")
# 					elif bproc == "even":
# 						ttab.append("0-2-4-6")
# 					elif bproc == "all":
# 						ttab.append("All processes")
# 					else:
# 						ttab.append(bproc)
# 					ttab.append("-")
# 					ttab.append(filt)
# 					ttab.append(susp)
# 					ttab.append("-")
# 					tab.append(ttab)
# 					ttab = []
# 					susp=""
# 				#print "HEAD",
# 				if len(line.split()) == 3:
# 					susp = susp+`int(line.split()[1].split("-")[1])/threadNum`+"-"
# 					#sust = sust+int(line.split()[1].split("-")[1])/threadNum+","
# 				elif len(line.split()) == 2:
# 					if int(line.split()[1].split("-")[1])%threadNum == 0:
# 						susp = susp+`int(line.split()[1].split("-")[1])/threadNum`+"-"
# 					else:
# 						print "nothing to do"
# 				else:
# 					print "nothing to do"
# 			else:
# 				print "Other Lines"
# 				print line.split()
# 				if len(line.split()) == 2:
# 					susp = susp+`int(line.split()[0].split("-")[1])/threadNum`+"-"
# 					#sust = sust+int(line.split()[1].split("-")[1])/threadNum+","
# 				elif len(line.split()) == 1:
# 					if int(line.split()[0].split("-")[1])%threadNum == 0:
# 						susp = susp+`int(line.split()[0].split("-")[1])/threadNum`+"-"
# 					else:
# 						print "nothing to do"
# 				else:
# 					print "nothing to do"
# 				#print line.split()
# 		if susp != "":
# 			ttab = []
# 			ttab.append(level)
# 			ttab.append(bug)
# 			if bproc.startswith("n"):
# 				ttab.append("All processes except "+bproc[1:])
# 			elif bproc == "odd":
# 				ttab.append("1-3-5-7")
# 			elif bproc == "even":
# 				ttab.append("0-2-4-6")
# 			elif bproc == "all":
# 				ttab.append("All processes")
# 			else:
# 				ttab.append(bproc)
# 			ttab.append("-")
# 			ttab.append(filt)
# 			ttab.append(susp)
# 			ttab.append("-")
# 			tab.append(ttab)
# 			ttab = []
# 			susp=""
#
# print tabulate(tab,headers=hdrs,tablefmt="moinmoin")
# #    fo2 = open("results/"+exp+".txt","r")
# #    fo2.write(tabulate(tab2,headers=hdrs2,tablefmt="plain"))
# #    fo2.close()
#
# #        process = subprocess.Popen("mkdir -p results/"+exp, stdout=subprocess.PIPE,shell=True)
# #        si, err = process.communicate()
