#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: genMatrix.py
# Description: Generate Jaccard similarity matrix from CL dot file (includes CL redundant removal and LCA) (input: dot)

import matplotlib
matplotlib.use('Agg')
#matplotlib.use('tkagg')

import glob
import sys,subprocess
import os
import numpy as np
from scipy.cluster.hierarchy import fcluster
import seaborn as sns ; sns.set(font_scale=0.43)
#import seaborn as sns
import matplotlib.pyplot as plt

from tabulate import tabulate
import math
from sets import Set
from collections import defaultdict


topn = 6
topCandidates = 6
threadNum = 1
procNum = 16

path = "/home/saeed/diffTrace/bugRepo"
rpath = "/home/saeed/diffTrace/results/diffNLR"
filters = ["01.1K10","01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","01.plt.0K10","11.1K10","11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10","11.mem.mpicol.ompcrit.cust.0K10"]
atrs = ["sing.orig","sing.log10","sing.actual","doub.orig","doub.log10","doub.actual"]



mpiFilters = ["11.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10"]
#mpiFilters = ["01.mpiall.cust.0K10","01.mpicol.cust.0K10","01.mpi.cust.0K10","01.plt.cust.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","11.plt.cust.0K10"]
#mpiFilters = ["01.mpiall.cust.0K10","01.mpicol.cust.0K10"]
mpiFilters2 = ["11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10"]
ompFilters2 = ["11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10","01.1K10","01.plt.0K10"]
everythingFilters = ["01.1K10","01.plt.0K10","11.1K10","11.plt.cust.0K10"]
omp2 = ["01.1K10","11.1K10","11.mem.ompcrit.cust.0K10","11.ompall.0K10","11.ompcrit.cust.0K10","11.plt.0K10"]

ompFilters = ["01.mem.ompall.cust.0K10","01.mem.ompcrit.cust.0K10","01.mem.ompmutex.cust.0K10","01.plt.mem.cust.0K10","01.plt.mem.mpi.ompall.cust.0K10","01.plt.mem.mpi.ompcrit.cust.0K10","11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.plt.mem.cust.0K10","11.plt.mem.mpi.ompall.cust.0K10","11.plt.mem.mpi.ompcrit.cust.0K10"]

custFilters1 = ["01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","01.1K10","01.plt.0K10","11.1K10","11.plt.cust.0K10"]
custFilters2 = ["11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.ompcrit.ompmutex.cust.0K10","01.1K10","01.plt.0K10","11.1K10"]



def readMatrix(matFile):
	try:
		data = open(matFile,"r").read().split("\n")[:-1]
		dataSize = len(data)
		mat = np.ones((len(data),len(data)))
		for i in range(0,len(data)):
			item = [x for x in data[i].split(",") if len(x) > 0]
			#print item
			for j in range(0,len(item)):
				if float(item[j]) != 1:
					mat[i][j] = float(item[j])
	except Exception, e:
		print "ERROR %s"%e
		mat = np.zeros((dataSize,dataSize))
	return mat

def fcluster2map(cl):
	ret={}
	for i in range(0,len(cl)):
		if int(cl[i]-1) in ret.keys():
			ret[int(cl[i])-1].append(i)
		else:
			ret[int(cl[i])-1]=[i]
	# for k,v in sorted(ret.items()):
	# 	print "CL %d:"%k
	# 	print v
	return ret


def clusterMap2String(cm,type):
	s = ""
	for k,v in sorted(cm.items()):
		s = s + "CL %d:%d"%(k,len(v))
		if len(v) <= topn and type == "diff":
			s = s + ",{"
			for vv in v:
				s = s + `vv` + " "
			s = s + "}\n"
		else:
			s = s + "\n"
	return s

def jsm2StringCell(m,type,metric,method,criterion,thresh,depth):
	z = sns.clustermap(m).dendrogram_col.linkage
	cm = fcluster2map(fcluster(z,thresh,criterion=criterion))
	return clusterMap2String(cm,type)

def jsm2outliers(m,metric,method,criterion,thresh,depth):
	z = sns.clustermap(m).dendrogram_col.linkage
	cm = fcluster2map(fcluster(z,thresh,criterion=criterion))
	ret = []
	for k,v in sorted(cm.items()):
		if len(v) <= topn:
			ret.extend(v)
	return ret

def candidateRow(l,oz):
    ret={}
    rets = []
    i = 1
    for item in l:
        if item in ret.keys():
            ret[item] = ret[item]+1
        else:
            ret[item] = 1
    if oz == 0:
        for k,v in sorted(ret.items(), key = lambda x: x[1], reverse = True):
            if k%threadNum == 0:
                s = `i`+"-"+`k`
                rets.append(s)
                i = i + 1
    else:
        for k,v in sorted(ret.items(), key = lambda x: x[1], reverse = True):
            if k%threadNum != 0:
                s = `i`+"-"+`k`
                rets.append(s)
                i = i + 1
    return rets

def measureMat(m1,m2,metric,method,criterion,thresh,depth):
	np.set_printoptions(precision=2)
	z1 = sns.clustermap(m1).dendrogram_col.linkage
	#print z1
	#print fcluster(z1,thresh,criterion=criterion)
	a1 = fcluster2map(fcluster(z1,thresh,criterion=criterion))
	#print "Buggy (A1)"
	#print clusterMap2String(a1,"buggy")

	z2 = sns.clustermap(m2).dendrogram_col.linkage
	#print z2
	#print fcluster(z2,thresh,criterion=criterion)
	a2 = fcluster2map(fcluster(z2,thresh,criterion=criterion))
	#print "noBug (A2)"
	#print clusterMap2String(a2,"noBug")

	n = len(m1)
	mm = np.zeros((thresh,thresh))
	for i in range(0,len(a1)):
		for j in range(0,len(a2)):
			mm[i][j]= len(list(set(a1[i]) & set(a2[j]) )) # len(intersection ( objects in cluster i of a1 and objects in cluster j of a2 ))
	return mm

def measureB (mm,n):
	t = 0
	p = 0
	q = 0
	pl = []
	for i in range(0,len(mm)):
		for j in range(0,len(mm)):
			t = t + math.pow(mm[i][j],2)
	t = t - n
	for i in range(0,len(mm)):
		pl = 0
		for j in range(0,len(mm)):
			pl = pl + mm[i][j]
		p = p + math.pow(pl,2)
	p = p - n

	for j in range(0,len(mm[0])):
		ql = 0
		for i in range(0,len(mm)):
			ql = ql + mm[i][j]
		q = q + math.pow(ql,2)
	q = q - n

	b = ( t * 1.0 ) / math.sqrt(p * q)
	return b


def topTable(exp,filt):
    app = exp.split(".")[0]
    bug = exp.split(".")[1]
    img = exp.split(".")[2]
    proc = exp.split(".")[3]
    thr = exp.split(".")[4]
    procNum = int(proc);
    if thr == "auto":
        threadNum = 5

	#hdrs = ["Filter","Attributes","K(cluster)","# Objects noBug","# Objects buggy","B score","# Objects diff"]
    hdrs = ["Filter","Attributes","K(cluster)","# Objects diff"]
    if filt == "mpi":
        filts = mpiFilters[:1]
    elif filt == "mpi2":
        filts = mpiFilters2
    elif filt == "omp":
        filts = ompFilters
    elif filt == "omp2":
        filts = omp2
    elif filt == "all":
        filts = filters
    elif filt == "eveything":
        filts = everythingFilters
    else:
        filts = omp2
    tab2=[]
    hdrs2 = ["Filter","Top Ranks (Process)","Top Ranks (Threads)"]

    for f in filts:
        ttab2 = []
        tab = []
        outliers = []
        for a in atrs:
            for th in [2,3,4]:
    			ttab = []
    			#print "Creating Table Cell for %s filter: %s , atr: %s, k-cluster: %d"%(exp,f,a,th)
    			jsmb = readMatrix(path+"/"+exp+"/cl/"+f+"/"+a+".w.jacmat.txt")
    			jsmnb = readMatrix(path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/cl/"+f+"/"+a+".w.jacmat.txt")
    			ttab.append(f)
    			ttab.append(a)
    			ttab.append(th)
    			#ttab.append(jsm2StringCell(jsmb,"buggy","metric","method","maxclust",th,0))
    			#ttab.append(jsm2StringCell(jsmnb,"noBug","metric","method","maxclust",th,0))
    			m = measureMat(jsmb,jsmnb,"metric","method","maxclust",th,0)
    			#ttab.append(measureB(m,len(jsmb)))
    			ttab.append(jsm2StringCell(np.absolute(np.subtract(jsmb,jsmnb)),"diff","metric","method","maxclust",th,0))
    			outliers.extend(jsm2outliers(np.absolute(np.subtract(jsmb,jsmnb)),"metric","method","maxclust",th,0))
    			tab.append(ttab)

        ttab2.append(f)

    	ttab = [" "," ","TOP MPI Suspicious\nTraces to check"]
    	tmps = ""
    	tmp = candidateRow(outliers,0)
    	for i in range(0,min(topCandidates,len(tmp))):
    		tmps = tmps + tmp[i] + "\n"
    	ttab.append(tmps)
    	tab.append(ttab)

        ttab2.append(tmps)

        ttab = [" "," ","TOP OMP Suspicious\nTraces to check"]
    	tmps = ""
    	tmp = candidateRow(outliers,1)
    	for i in range(0,min(topCandidates,len(tmp))):
    		tmps = tmps + tmp[i] + "\n"
    	ttab.append(tmps)
    	tab.append(ttab)

        ttab2.append(tmps)

    	#print tabulate(tab,headers=hdrs,tablefmt="fancy_grid",showindex="always")
        process = subprocess.Popen("mkdir -p results/"+exp, stdout=subprocess.PIPE,shell=True)
        si, err = process.communicate()
        s = tabulate(tab,headers=hdrs,tablefmt="plain")
        fo = open("results/"+exp+"/"+f+".txt","w")
    	fo.write(s)
    	fo.close()
        tab2.append(ttab2)

    fo2 = open("results/"+exp+".txt","w")
    fo2.write(tabulate(tab2,headers=hdrs2,tablefmt="plain"))
    fo2.close()

if len(sys.argv) != 3:
	print "USAGE:\n\t " +sys.argv[0]+"exp filters"
	sys.exit(-1)

#mat1 = readMatrix(sys.argv[1]) # Buggy
#mat2 = readMatrix(sys.argv[2]) # noBug
print "Generating candidate tables for ... "+sys.argv[1]
topTable(sys.argv[1],sys.argv[2])


# for th in [2,3,4]:
# 	print "Threshold = %d"%th
# 	m = measureMat(mat1,mat2,"metric","method","maxclust",th,0)
# 	b = measureB(m,len(mat1))
# 	# tab =[]
# 	# hdrs = ["-","-"]
# 	# ttab = ["-","-"]
# 	# for i in range(0,th):
# 	# 	hdrs.append("noBug")
# 	# 	ttab.append(i)
# 	# tab.append(ttab)
# 	# for i in range(0,len(m)):
# 	# 	ttab = ["buggy",i]
# 	# 	for j in range(0,len(m[i])):
# 	# 		ttab.append(m[i][j])
# 	# 	tab.append(ttab)
# 	# print tabulate(tab,headers=hdrs,tablefmt="fancy_grid")
# 	print "B_%d = %.3f"%(th,b)
# 	diff = np.absolute(np.subtract(mat1,mat2))
# 	z = sns.clustermap(diff).dendrogram_col.linkage
# 	#print z
# 	print fcluster(z,th,criterion="maxclust")
# 	a = fcluster2map(fcluster(z,th,criterion="maxclust"))
# 	print "\n*********\nDIFF "
# 	for k,v in sorted(a.items()):
# 		print "len(CL %d)=%d"%(k,len(v))
# 		if len(v) <= topn:
# 			print v
# 	print "\n****END*****\n"
