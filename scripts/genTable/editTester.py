
import sys,subprocess
import numpy as np
import diffCore as diff
import newVis as nvis

if len(sys.argv) != 3:
	print "USAGE:\n\t " +sys.argv[0]+" A B"
	sys.exit(-1)

f1 = [x.strip() for x in open(sys.argv[1],"r").readlines() if len(x.strip()) != 0]
f2 = [x.strip() for x in open(sys.argv[2],"r").readlines() if len(x.strip()) != 0]


lc = diff.lcs(f1,f2)

name = "test"
dt = nvis.edit2dot(lc,name,showC,1)

print dt
#f = open(name+".dot","w")
#f.write(dt)
#f.close()
#process = subprocess.Popen("dot -Tpdf "+name+".dot -o "+name+".pdf", stdout=subprocess.PIPE,shell=True)
#si, err = process.communicate()
