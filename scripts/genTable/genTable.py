#!/usr/bin/env python

# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2017, All rights reserved
# Code: genTable.py
# Description: generate table of differences

import sys,subprocess
import numpy as np
import diffCore as diff
import newVis as nvis
from tabulate import tabulate

showCommon = 1
topn = 3
threadNum = 5

path = "/home/saeed/diffTrace/bugRepo"
rpath = "/home/saeed/diffTrace/results/diffNLR"
filters = ["01.1K10","01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","01.plt.0K10","11.1K10","11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10"]
atrs = ["sing.orig","sing.log10","sing.actual","doub.orig","doub.log10","doub.actual"]

mpiFilters = ["01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10"]
ompFilters = ["11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.ompcrit.ompmutex.cust.0K10","11.plt.cust.0K10"]
everythingFilters = ["01.1K10","01.plt.0K10","11.1K10","11.plt.cust.0K10"]

custFilters1 = ["01.mpi.0K10","01.mpiall.0K10","01.mpicol.0K10","11.mpiall.cust.0K10","11.mpicol.cust.0K10","11.mpi.cust.0K10","01.1K10","01.plt.0K10","11.1K10","11.plt.cust.0K10"]
custFilters2 = ["11.mem.ompall.cust.0K10","11.mem.ompcrit.cust.0K10","11.mem.ompmutex.cust.0K10","11.ompcrit.ompmutex.cust.0K10","01.1K10","01.plt.0K10","11.1K10"]

def genLegend(i,j):
    s = "{\n\t"
    s = s+"	label = \"Legend\" ;\n\t"
    s = s+"shape=rectangle  ;\n\t"
    s = s+"color = black  ;\n\t"
    s = s+"\""+`i/threadNum`+"."+`i%threadNum`+"(Bug Free)\"  [shape=record ,style=dashed, color=blue] ;\n\t"
    s = s+"\""+`j/threadNum`+"."+`j%threadNum`+"(Bug Free)\"  [shape=record ,style=dashed, color=red] ;\n\t"
    s = s+"\"Common(Bug Free)\" [shape=record ,style=dashed, color=green4] ;\n\t"
    s = s+"\""+`i/threadNum`+"."+`i%threadNum`+"(Buggy)\"  [shape=record , color=blue] ;\n\t"
    s = s+"\""+`j/threadNum`+"."+`j%threadNum`+"(Buggy)\"  [shape=record , color=red] ;\n\t"
    s = s+"\"Common(Buggy)\" [shape=record , color=green4] ;\n\t"
    s = s+"\n\t\""+`i/threadNum`+"."+`i%threadNum`+"(Bug Free)\" -> \""+ `i/threadNum`+"."+`i%threadNum`+"(Buggy)\" [style=invis] ;\n\t"
    s = s+"\""+`j/threadNum`+"."+`j%threadNum`+"(Bug Free)\" -> \""+ `j/threadNum`+"."+`j%threadNum`+"(Buggy)\" [style=invis] ;\n\t"
    s = s+"\"Common(Bug Free)\" -> \"Common(Buggy)\" [style=invis] ;\n"
    s = s+"}"
    return s

def ltab2html(ltab,pre):
    lines = [x for x in open(ltab,"r").readlines()]
    s = "{\n\n\t"
    s = s + pre+"HtmlTable [\n\t\t"
    s = s + "shape=plaintext\n\t\t"
    s = s + "color=Black\n\t\t"
    s = s + "label=<\n\t\t\t"
    s = s + "<table border=\'0\' cellborder=\'1\'>\n\t\t\t\t "
    s = s + "<tr><td> Loop </td> <td> Body </td></tr>\n\t\t\t\t "
    for line in lines:
        s = s + "<tr><td> L"+line.partition(":")[0]+" </td> <td> "+line.partition(":")[2].strip()+" </td></tr>\n\t\t\t\t "
    s = s + "</table>\n\t>];\n}"
    return s

def genDiffNLR(_apath,_bpath,nm,showCommon,isBuggy):
    aa = _apath
    bb = _bpath
    fe = diff.lcs([x.strip() for x in open(aa,"r").readlines() if len(x) > 0],[x.strip() for x in open(bb,"r").readlines() if len(x) > 0])
    #print fe
    dt = nvis.edit2dot(fe,nm,showCommon,isBuggy)
    return dt

def readCLTable(file):
    print ">> Read table %s.."%file
    list = open(file,"r").read().split("\n")[3:]
    ret = {}
    for line in list:
		ll = line.split("|")
		if len(ll) == 3:
			value = ll[1].strip()
			ret[int(ll[0].strip())-1] = value
    print ">> Return table with len %d.."%len(ret)
    return ret

class topList:
    def __init__(self,exp,f,a,i,j,objTable,objTable_nb):
        app = exp.split(".")[0]
        bug = exp.split(".")[1]
        img = exp.split(".")[2]
        proc = exp.split(".")[3]
        thr = exp.split(".")[4]

        iobj = objTable[i].split(".")[1]+"."+objTable[i].split(".")[2]+"."+objTable[i].split(".")[3]
        jobj = objTable[j].split(".")[1]+"."+objTable[j].split(".")[2]+"."+objTable[j].split(".")[3]

        iobj_nb = objTable_nb[i].split(".")[1]+"."+objTable_nb[i].split(".")[2]+"."+objTable_nb[i].split(".")[3]
        jobj_nb = objTable_nb[j].split(".")[1]+"."+objTable_nb[j].split(".")[2]+"."+objTable_nb[j].split(".")[3]

        self.iid = "("+`i`+")"+iobj
        self.jid = "("+`j`+")"+jobj

        self.iid_nb = "("+`i`+"_nb)"+iobj_nb
        self.jid_nb = "("+`j`+"_nb)"+jobj_nb
        #find paths
        pathi = path+"/"+exp+"/prep/"+f+"/"+objTable[i]+".txt"
        pathj = path+"/"+exp+"/prep/"+f+"/"+objTable[j]+".txt"
        pathi_nb = path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/prep/"+f+"/"+objTable_nb[i]+".txt"
        pathj_nb = path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/prep/"+f+"/"+objTable_nb[j]+".txt"

        nm = `filters.index(f)`+"_"+iobj+"_"+jobj
        nm_nb = `filters.index(f)`+"_"+iobj_nb+"_"+jobj_nb

        ltab = ltab2html(path+"/"+exp+"/prep/"+f+"/ltab.txt","b")
        ltab_nb = ltab2html(path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/prep/"+f+"/ltab.txt","nb")
        diffNLR= genDiffNLR(pathi,pathj,nm,showCommon,1)
        diffNLR_nb= genDiffNLR(pathi_nb,pathj_nb,nm_nb,showCommon,0)

        finalDot= "digraph \""+exp+"\"{\n\trankdir= TP\n\t"
        finalDot= finalDot +"subgraph ltab"+ltab+"\n\t"
        finalDot= finalDot +"subgraph diffNLR"+diffNLR+"\n\t"
        finalDot= finalDot +"subgraph ltab_nb"+ltab_nb+"\n\t"
        finalDot= finalDot +"subgraph diffNLRNB"+diffNLR_nb+"\n\t"
        finalDot= finalDot +"subgraph leg"+genLegend(i,j)+"\n"
        finalDot= finalDot +"}"

        #create directory to store dots
        print "<><> WILL RUN:\n$> %s"%("mkdir -p "+rpath+"/"+exp+"/dots/ ;")
        process = subprocess.Popen("mkdir -p "+rpath+"/"+exp+"/dots/ ;", stdout=subprocess.PIPE,shell=True)
        si, err = process.communicate()
        print "<><> WILL OPEN and write DOT to:\n$> %s"%(rpath+"/"+exp+"/dots/"+nm+".dot")
        f = open(rpath+"/"+exp+"/dots/"+nm+".dot","w")
        f.write(finalDot)
        f.close()
        #print finalDot
        print "<><> WILL RUN:\n$> %s"%"dot -Tpdf "+rpath+"/"+exp+"/dots/"+nm+".dot -o "+rpath+"/"+exp+"/"+nm+".pdf"
        process = subprocess.Popen("dot -Tpdf "+rpath+"/"+exp+"/dots/"+nm+".dot -o "+rpath+"/"+exp+"/"+nm+".pdf", stdout=subprocess.PIPE,shell=True)
        si, err = process.communicate()
        print ">>>>> Storing Final path to %s"%(exp+"/"+nm+".pdf")
        self.diffNLRpath = exp+"/"+nm+".pdf"
        print ">>>>> Genearting diffNLR...\n\ti: %s\n\tj: %s\n\texp: %s filter: %s\n\toutFile: %s..."%(pathi,pathj,exp,filter,nm)

    def toString(self):
        #return self.iid+","+self.jid+","+self.diffNLRpath
        return self.iid+","+self.jid

class tableCell:
    def __init__(self,exp,f,a,option):
        app = exp.split(".")[0]
        bug = exp.split(".")[1]
        img = exp.split(".")[2]
        proc = exp.split(".")[3]
        thr = exp.split(".")[4]
        self.exp = exp
        self.filter = f # hold information about
        self.atr = a
        self.option = option
        self.objTable = readCLTable(path+"/"+exp+"/cl/"+f+"/"+a+".w.objTable.txt")
        self.objTable_nb = readCLTable(path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/cl/"+f+"/"+a+".w.objTable.txt")
        self.topProcList = self.getTopList("process")
        self.topThreadList = self.getTopList("thread")
    def getTopList(self,type):
        print ">> Get topPairs type: %s ..."%type
        ret = {}
        # TP list of top pairs(i,j)
        tp = topPairs(self.exp,self.filter,self.atr,type)
        print ">> Creating topList: %s ..."%type
        for rank in range(0,len(tp)):
            ret[rank] = topList(self.exp,self.filter,self.atr,tp[rank][0],tp[rank][1],self.objTable,self.objTable_nb)
        return ret

    def toStringList(self):
        l = []
        l.append(self.filter)
        l.append(self.atr)
        s = ""
        for k,v in sorted(self.topProcList.items()):
             s = s + `k+1`+":"+v.toString()+"\n"
        l.append(s)
        s = ""
        for k,v in sorted(self.topThreadList.items()):
             s = s + `k+1`+":"+v.toString()+"\n"
        l.append(s)
        return l

def topPairs(exp,f,a,type):
    # Read jsm exp with f and a
    # Read jsm noBug with f and a
    # Calculate top5 pairs to see their diffNLR
    diffs = {}
    app = exp.split(".")[0]
    bug = exp.split(".")[1]
    img = exp.split(".")[2]
    proc = exp.split(".")[3]
    thr = exp.split(".")[4]
    jsm1matdata = open(path+"/"+exp+"/cl/"+f+"/"+a+".w.jacmat.txt","r").read().split("\n")[:-1]
    jsm2matdata = open(path+"/"+app+".noBug."+img+"."+proc+"."+thr+"/cl/"+f+"/"+a+".w.jacmat.txt","r").read().split("\n")[:-1]
    assert(len(jsm1matdata) == len(jsm1matdata))
    matx = np.ones((len(jsm1matdata),len(jsm1matdata)))
    for i in range(0,len(jsm1matdata)):
        jsm1Row = [x for x in jsm1matdata[i].split(",") if len(x) > 0]
        jsm2Row = [x for x in jsm2matdata[i].split(",") if len(x) > 0]
        assert(len(jsm1Row) == len(jsm2Row))
        for j in range(0,len(jsm1Row)):
            #print "matx[%d][%d] > %.3f ** %.3f "%(i,j,float(jsm1Row[j]),float(jsm2Row[j]))
            matx[i][j] = abs(float(jsm1Row[j]) - float(jsm2Row[j]))
            if (j,i) not in diffs.keys():
                diffs[(i,j)] = matx[i][j]
    if type == "thread":
        ret = []
        cnt = 0
        print ">>>> Top pairs:"
        for k,v in sorted(diffs.items(), key= lambda x: x[1],reverse=True):
            if k[0] % threadNum != 0 and k[1] % threadNum != 0:
                ret.append(k)
                print "\t%s:%.3f"%(k,v)
                cnt = cnt + 1
            if cnt >= topn:
                break
        return ret
    else:
        ret = []
        cnt = 0
        print ">>>> Top pairs:"
        for k,v in sorted(diffs.items(), key= lambda x: x[1],reverse=True):
            if k[0] % threadNum == 0 and k[1] % threadNum == 0:
                ret.append(k)
                print "\t%s:%.3f"%(k,v)
                cnt = cnt + 1
            if cnt >= topn:
                break
        return ret




def topTable(exp):
    app = exp.split(".")[0]
    bug = exp.split(".")[0]
    img = exp.split(".")[0]
    proc = exp.split(".")[0]
    thr = exp.split(".")[0]
    fullTable = {}
    for f in ompFilters:
        for a in atrs:
            print "Creating Table Cell for %s filter: %s , atr: %s"%(exp,f,a)
            fullTable[(f,a)] = tableCell(exp,f,a,0)
    hdrs = ["Filter","Attributes","Top Process diffNLR Candidates","Top Thread diffNLR Candidates"]
    tab = []
    for k,v in fullTable.items():
        tab.append(v.toStringList())
    print tabulate(tab,headers=hdrs,tablefmt="fancy_grid")
    # for each filter
    #   for each attribute
    #       calculate topPairs to view their diffNLR



if len(sys.argv) != 2:
	print "USAGE:\n\t " +sys.argv[0]+" exp"
	sys.exit(-1)


topTable(sys.argv[1])
