#!/usr/bin/env python
# Author: Saeed Taheri, University of Utah, staheri@cs.utah.edu, 2018, All rights reserved
# Code: cmrx.py
# Description: Read texTrace and convert them to MRR (minimal reptetive representation)


import sys,subprocess
from sets import Set
import diffCore as diff
import newVis as nvis

aa = sys.argv[1]
bb = sys.argv[2]
fe = diff.lcs([x.strip() for x in open(aa,"r").readlines() if len(x) > 0],[x.strip() for x in open(bb,"r").readlines() if len(x) > 0])
print fe
nm = aa.split("/")[-1]+"__"+bb.split("/")[-1]
dt = nvis.edit2dot(fe,nm,1)
f = open(nm+".dot","w")
f.write(dt)
f.close()
process = subprocess.Popen("dot -Tpdf "+nm+".dot -o "+nm+".pdf", stdout=subprocess.PIPE,shell=True)
si, err = process.communicate()
