#ifndef ILCS_H
#define ILCS_H

/* comment out the following line if only GPU code is provided */
#define ILCS_USE_CPUS

/* comment out the following line if only CPU code is provided */
//#define ILCS_USE_GPUS

/* comment out the following line for single-node computation (no MPI) */
#define ILCS_USE_MPI

/* the ILCS_SLEEP_TIME controls, in seconds, how often the compute nodes communicate */
#define ILCS_SLEEP_TIME 4

/* the ILCS_NO_CHANGE_THRESHOLD controls after how many steps of no improvement the framework terminates */
#define ILCS_NO_CHANGE_THRESHOLD 5

/* ILCS CPU code interface */
size_t CPU_Init(int argc, char* argv[]);
void CPU_Exec(long seed, void const* champion, void* result);
void CPU_Output(void const* champion);

/* ILCS GPU code interface */
size_t GPU_Init(int argc, char *argv[]);
long GPU_Exec(long seed, long stride, void const* champion, void* result);
void GPU_Output(void const* champion);

#endif
