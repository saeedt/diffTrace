/*
Copyright (c) 2013, Texas State University-San Marcos. All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted for academic, research, experimental, or personal use provided
that the following conditions are met:

   * Redistributions of source code must retain the above copyright notice,
     this list of conditions and the following disclaimer.
   * Redistributions in binary form must reproduce the above copyright notice,
     this list of conditions and the following disclaimer in the documentation
     and/or other materials provided with the distribution.
   * Neither the name of Texas State University-San Marcos nor the names of its
     contributors may be used to endorse or promote products derived from this
     software without specific prior written permission.

For all other uses, please contact the Office for Commercialization and Industry
Relations at Texas State University-San Marcos <http://www.txstate.edu/ocir/>.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

Authors: Martin Burtscher and Hassan Rabeti
*/


/******************************************************************************/
/*** ILCS Parallelization Framework C Code ************************************/
/******************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <sys/time.h>
#include <omp.h>
#include "ilcs.h"
#ifdef ILCS_USE_MPI
#include <mpi.h>
#endif

#ifdef ILCS_USE_GPUS
int ilcs_get_num_gpus();
size_t ilcs_init_gpus(int argc, char *argv[], int gpus);
long ilcs_exec_gpu(int deviceID, long seed, long stride, void const* champion, void* result);
void ilcs_output_gpu(void const* champion);
long ilcs_count_gpu(int deviceID);
#endif

static volatile long cpu_climb_count = 0;  // stats only
static volatile long gpu_climb_count = 0;  // stats only

int main(int argc, char *argv[])
{
  int i, comm_sz, my_rank, threads;
  int step;
  size_t size;
#ifdef ILCS_USE_GPUS
  size_t gpusize;
#endif
  long lCPUs, gCPUs, lGPUs, gGPUs;
  volatile long CPUevals, GPUevals;
  long **best_result;
  struct timeval starttime, endtime;

  // Bug Injection purposes
  int buggyThread = 2;
  int buggyIter = 100;
  int buggyProc = 2;

  if (sizeof(long) < 8) {fprintf(stderr, "size of long is too short\n"); exit(-1);}
  if (ILCS_SLEEP_TIME < 1) {fprintf(stderr, "ILCS_SLEEP_TIME needs to be at least 1 s\n"); exit(-1);}
  if (ILCS_NO_CHANGE_THRESHOLD < 1) {fprintf(stderr, "ILCS_NO_CHANGE_THRESHOLD needs to be at least 1\n"); exit(-1);}

  comm_sz = 1;
  my_rank = 0;
#ifdef ILCS_USE_MPI
  MPI_Init(NULL, NULL);
  MPI_Comm_size(MPI_COMM_WORLD, &comm_sz);
  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);
#endif

  lCPUs = 0;
#ifdef ILCS_USE_CPUS
  lCPUs = omp_get_num_procs();
#endif

  lGPUs = 0;
#ifdef ILCS_USE_GPUS
  lGPUs = ilcs_get_num_gpus();
#endif

  gCPUs = lCPUs;
  gGPUs = lGPUs;
#ifdef ILCS_USE_MPI
  MPI_Reduce(&lCPUs, &gCPUs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce(&lGPUs, &gGPUs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
#endif

  if (my_rank == 0) {
    fprintf(stderr, "\nILCS Framework v0.27\n");
    fprintf(stderr, "configuration: %d processes, %d sec/step, %d steps to termination\n", comm_sz, ILCS_SLEEP_TIME, ILCS_NO_CHANGE_THRESHOLD);
    fprintf(stderr, "configuration: %ld CPUs/node, %ld CPUs total\n", lCPUs, gCPUs);
    fprintf(stderr, "configuration: %ld GPUs/node, %ld GPUs total\n", lGPUs, gGPUs);
  }

  size = 0;
#ifdef ILCS_USE_CPUS
  size = CPU_Init(argc, argv);
#endif
#ifdef ILCS_USE_GPUS
  gpusize = ilcs_init_gpus(argc, argv, lGPUs);
#ifdef ILCS_USE_CPUS
  if (gpusize != size) {fprintf(stderr, "CPU and GPU Init code must return the same size\n"); exit(-1);}
#endif
  size = gpusize;
#endif
  if (size < 1) {fprintf(stderr, "Init code must be called and must return a size greater than zero\n"); exit(-1);}

  while ((size % sizeof(long)) != 0) {
    size++;
  }

  threads = lCPUs + lGPUs;
  fprintf(stderr, "Process %d runs with %d Threads\n",my_rank,threads);
  best_result = (long **)malloc(threads * sizeof(long *));
  if (best_result == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}

  for (i = 0; i < threads; i++) {
    best_result[i] = (long *)malloc(size);
    if (best_result[i] == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
    best_result[i][0] = LONG_MAX;
  }

  long minimum = LONG_MAX;
  long lmin, gmin;
  volatile int cont = 1;
  volatile long *champ = NULL;
  CPUevals = 0;
  GPUevals = 0;

  MPI_Barrier(MPI_COMM_WORLD);  // for better timing
  gettimeofday(&starttime, NULL);
#pragma omp parallel num_threads(threads + 1) default(shared)
  {
    if (omp_get_num_threads() != threads + 1) {fprintf(stderr, "not enough threads launched\n"); exit(-1);}

    int rank = omp_get_thread_num();
    //-------------------------------------------------------
    if (rank > 0) {
      rank--;
      if (rank < lCPUs) {  // CPU threads
#ifdef ILCS_USE_CPUS
        long seed = LONG_MAX / comm_sz * my_rank + rank;
        long *res = (long *)malloc(size);
        if (res == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
        do {
          CPU_Exec(seed, (void*)champ, res);
#pragma omp atomic
          CPUevals++;
          if (best_result[rank][0] > res[0]) {
#pragma omp critical
            memcpy(best_result[rank], res, size);
          }
          seed += lCPUs;
        } while (cont);
#endif
#ifdef ILCS_USE_GPUS
      } else {  // GPU handler threads
        int deviceID = rank - lCPUs;
        long seed = LONG_MAX / comm_sz * my_rank - deviceID - 1;
        long *res = (long *)malloc(size);
        if (res == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
        do {
          long evaluations;
          evaluations = ilcs_exec_gpu(deviceID, seed, lGPUs, (void*)champ, res);
#pragma omp atomic
          GPUevals += evaluations;
          if (best_result[rank][0] > res[0]) {
#pragma omp critical
            memcpy(best_result[rank], res, size);
          }
          seed -= lGPUs * evaluations;
        } while (cont);
#pragma omp atomic
        gpu_climb_count += ilcs_count_gpu(deviceID);
#endif
      }
    } else {  // communication thread
      //-------------------------------------------------------
      int nochange = 0;
      step = 0;
      do {
        sleep((ILCS_SLEEP_TIME + 3) / 4);
        gettimeofday(&endtime, NULL);
        double runtime = endtime.tv_sec + endtime.tv_usec/1000000.0 - starttime.tv_sec - starttime.tv_usec/1000000.0;
        if (runtime > (step + 1) * ILCS_SLEEP_TIME) {
          step++;
          lmin = best_result[0][0];
          int best = 0;
          for (i = 1; i < threads; i++) {
            if (lmin > best_result[i][0]) {
              lmin = best_result[i][0];
              best = i;
            }
          }
          gmin = lmin;
#ifdef ILCS_USE_MPI
          MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
#endif
          nochange++;
          if (minimum > gmin) {
            minimum = gmin;
            nochange = 0;

            long *temp = (long *)malloc(size);
            if (temp == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
#ifdef ILCS_USE_MPI
            if (minimum == lmin) {
              lmin = my_rank;
            } else {
              lmin = LONG_MAX;
            }
            MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
            if (my_rank == gmin) {
              // OMP BUG HERE
//#pragma omp critical
              memcpy(temp, best_result[best], size);
            }
            MPI_Bcast(temp, size, MPI_BYTE, gmin, MPI_COMM_WORLD);
#else
#pragma omp critical
            memcpy(temp, best_result[best], size);
#endif
            champ = temp;
            if (my_rank == 0) {
              fprintf(stderr, "step: %d  ", step);  fflush(stderr);
#ifdef ILCS_USE_CPUS
//              CPU_Output((void*)champ);
#else
//              ilcs_output_gpu((void*)champ);
#endif
            }
          }
        }
      } while (nochange < ILCS_NO_CHANGE_THRESHOLD);
      cont = 0;  // signal to stop other threads
    }
  }

  lmin = best_result[0][0];
  int best = 0;
  for (i = 1; i < threads; i++) {
    if (lmin > best_result[i][0]) {
      lmin = best_result[i][0];
      best = i;
    }
  }
  gmin = lmin;
#ifdef ILCS_USE_MPI
  MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
#endif
  if (minimum > gmin) {
    minimum = gmin;
    long *temp = (long *)malloc(size);
    if (temp == NULL) {fprintf(stderr, "could not allocate memory\n"); exit(-1);}
#ifdef ILCS_USE_MPI
    if (minimum == lmin) {
      lmin = my_rank;
    } else {
      lmin = LONG_MAX;
    }
    MPI_Allreduce(&lmin, &gmin, 1, MPI_LONG, MPI_MIN, MPI_COMM_WORLD);
    if (my_rank == gmin) {
#pragma omp critical
      memcpy(temp, best_result[best], size);
    }
    MPI_Bcast(temp, size, MPI_BYTE, gmin, MPI_COMM_WORLD);
#else
#pragma omp critical
    memcpy(temp, best_result[best], size);
#endif
    champ = temp;
  }
  gettimeofday(&endtime, NULL);

  long CPUtotal = CPUevals;
  long GPUtotal = GPUevals;
#ifdef ILCS_USE_MPI
  MPI_Reduce((long*)&CPUevals, &CPUtotal, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce((long*)&GPUevals, &GPUtotal, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
#endif

  long CPUclimbs = cpu_climb_count;
  long GPUclimbs = gpu_climb_count;
#ifdef ILCS_USE_MPI
  MPI_Reduce((long*)&cpu_climb_count, &CPUclimbs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
  MPI_Reduce((long*)&gpu_climb_count, &GPUclimbs, 1, MPI_LONG, MPI_SUM, 0, MPI_COMM_WORLD);
#endif

  if (my_rank == 0) {
#ifdef ILCS_USE_CPUS
    CPU_Output((void*)champ);
#else
    ilcs_output_gpu((void*)champ);
#endif
    double runtime = endtime.tv_sec + endtime.tv_usec/1000000.0 - starttime.tv_sec - starttime.tv_usec/1000000.0;
    fprintf(stderr, "\nruntime: %.3lf sec\n", runtime);
    fprintf(stderr, "steps: %d\n", step);
    fprintf(stderr, "CPUtotal: %ld (%.1lf per sec)\n", CPUtotal, CPUtotal / runtime);
    fprintf(stderr, "GPUtotal: %ld (%.1lf per sec)\n", GPUtotal, GPUtotal / runtime);
    fprintf(stderr, "overall : %ld (%.1lf per sec)\n", CPUtotal + GPUtotal, (CPUtotal + GPUtotal) / runtime);
    fprintf(stderr, "CPU climb count: %ld\n", CPUclimbs);
    fprintf(stderr, "GPU climb count: %ld\n", GPUclimbs);
    fprintf(stderr, "overall climbs: %ld\n", CPUclimbs + GPUclimbs);
    fprintf(stderr, "overall climbs per second: %.6lf\n", (CPUclimbs + GPUclimbs) / runtime);
  }

#ifdef ILCS_USE_MPI
  MPI_Finalize();
#endif

  return 0;
}


/******************************************************************************/
/*** User TSP C Code **********************************************************/
/******************************************************************************/

#define MAXCITIES 1296

static int cities;
static float posx[MAXCITIES], posy[MAXCITIES];
static int dist[MAXCITIES * MAXCITIES];

struct TSP {
  long quality;  // lower is better
  unsigned short tour[MAXCITIES + 1];
};

size_t CPU_Init(int argc, char* argv[])
{
  register int i, j, ch, cnt;
  int i1;
  float i2, i3;
  register float dx, dy;
  register FILE *f;
  char str[256];

  if (argc != 2) {fprintf(stderr, "\narguments: input_file\n"); exit(-1);}

  f = fopen(argv[1], "r+t");
  if (f == NULL) {fprintf(stderr, "could not open file %s\n", argv[1]); exit(-1);}

  ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);
  ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);
  ch = getc(f);  while ((ch != EOF) && (ch != '\n')) ch = getc(f);

  ch = getc(f);  while ((ch != EOF) && (ch != ':')) ch = getc(f);
  fscanf(f, "%s\n", str);
  cities = atoi(str);
  if (cities == 0) {
    fprintf(stderr, "%d cities\n", cities);
    exit(-1);
  }
  if (cities >= MAXCITIES) {
    fprintf(stderr, "%d cities is too large\n", cities);
    exit(-1);
  }

  fscanf(f, "%s\n", str);
  if (strcmp(str, "NODE_COORD_SECTION") != 0) {
     printf("%s\n", str);
     printf("%d\n", cities);
    fprintf(stderr, "wrong file format\n");
    exit(-1);
  }

  cnt = 0;
  while (fscanf(f, "%d %f %f\n", &i1, &i2, &i3)) {
    posx[cnt] = i2;
    posy[cnt] = i3;
    cnt++;
    if (cnt > cities) {fprintf(stderr, "input too long\n"); exit(-1);}
    if (cnt != i1) {fprintf(stderr, "input line mismatch: expected %d instead of %d\n", cnt, i1); exit(-1);}
  }
  if (cnt != cities) {
    fprintf(stderr, "read %d instead of %d cities\n", cnt, cities);
    exit(-1);
  }
  fscanf(f, "%s", str);
  if (strcmp(str, "EOF") != 0) {
    fprintf(stderr, "didn't see 'EOF' at end of file\n");
    exit(-1);
  }
  fclose(f);

  for (i = 0; i < cities; i++) {
    for (j = i; j < cities; j++) {
      dx = posx[i] - posx[j];
      dy = posy[i] - posy[j];
      dist[j * cities + i] = dist[i * cities + j] = (int)(sqrtf(dx * dx + dy * dy) + 0.5);  // must be symmetric
    }
  }

  return sizeof(struct TSP);
}

void CPU_Exec(long seed, void const* champion, void* result)
{
  register unsigned short t;
  register int i, j, change, mini, minj, minchange;
  register int ti, tj, tip1, tjp1, diip1, nextti, from, to, len;
  register struct TSP* res = (struct TSP*)result;
  unsigned int rndstate;
  register long climbs = 0;  // stats only
  unsigned short tour[MAXCITIES + 1];

  for (i = 1; i < cities; i++) {
    tour[i] = i;
  }
  tour[cities] = 0;

  rndstate = (unsigned int)(seed ^ (seed >> 22) ^ (seed >> 43));
  for (i = 1; i < cities; i++) {
    j = rand_r(&rndstate) % (cities - 1) + 1;
    t = tour[i];
    tour[i] = tour[j];
    tour[j] = t;
  }

  do {
    minchange = 0;
    ti = 0;  // tour[0]
    nextti = tour[1];
    for (i = 2; i < cities; i++) {  // bias = 2
      tip1 = nextti * cities;
      diip1 = dist[ti + nextti];
      tj = nextti = tour[i];
      minchange += diip1;
      for (j = i + 1; j <= cities; j++) {  // bias = 1
        tjp1 = tour[j];
        change = dist[ti + tj] + dist[tip1 + tjp1] - dist[tj * cities + tjp1];
        tj = tjp1;
        if (minchange > change) {
          minchange = change;
          mini = i;
          minj = j;
        }
      }
      minchange -= diip1;
      ti = tip1;
    }

    climbs++;  // stats only

    if (minchange < 0) {
      // new tour is 0 to mini, minj downto mini+1, minj+1 to cities
      i = mini - 1;  // mini + 1
      j = minj - 1;  // minj
      while (i < j) {
        t = tour[j];
        tour[j] = tour[i];
        tour[i] = t;
        i++;
        j--;
      }
    }
  } while (minchange < 0);

  #pragma omp atomic
  cpu_climb_count += climbs;  // stats only

  len = 0;
  from = 0;
  for (i = 1; i < cities; i++) {
    to = res->tour[i] = tour[i];
    len += dist[from * cities + to];
    from = to;
  }
  len += dist[from];
  res->tour[cities] = tour[cities];
  res->quality = len;
}

void CPU_Output(void const* champion)
{
  register int i;
  register struct TSP* champ = (struct TSP*)champion;

  if (champ != NULL) {
    printf("len: %ld  tour:", champ->quality);
    for (i = 1; i <= cities; i++) {
      printf(" %d", (int)champ->tour[i]);
    }
    printf("\n\n");
  }
}
