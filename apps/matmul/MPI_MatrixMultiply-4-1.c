/*For EuroPVM 2009 ISP workshop */

/*
 ============================================================================
 Name        : MPI_MatrixMultiply-4.c
 ============================================================================
 */

/* THIS brings back the wildcard into matmult3 (non-deterministic) and uses Isend, Waitall, and Wait */
/* Interleavings are as in matmult.c */

#include <stdio.h>
#include <mpi.h>

#define MAX_AROWS 64
#define MAX_ACOLS 8
#define MAX_BROWS MAX_ACOLS
#define MAX_BCOLS 64
#define MAX_CROWS MAX_AROWS
#define MAX_CCOLS MAX_BCOLS
#define MAX_A_ENTRIES (MAX_AROWS * MAX_ACOLS)
#define MAX_B_ENTRIES (MAX_BROWS * MAX_BCOLS)
#define MAX_C_ENTRIES (MAX_AROWS * MAX_BCOLS)

int main(int argc,char *argv[]) {

  float a[MAX_A_ENTRIES], b[MAX_B_ENTRIES], c[MAX_C_ENTRIES];
  float buffer[MAX_ACOLS], ans[MAX_ACOLS];

  int myid, master, numprocs;
  int i, j, numsent, numrecd, sender;
  int anstype, row, arows, acols, brows, bcols, crows, ccols;

  MPI_Status status;

  MPI_Request onereq;  // for some of the Isends

  MPI_Request reqs[13];    // Max num proc allowed in Visual Studio ISP
  MPI_Status  statuses[13]; // Same as above

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
  MPI_Comm_rank(MPI_COMM_WORLD, &myid);

  master = 0;
  arows  = MAX_AROWS;
  acols  = MAX_ACOLS;
  brows  = MAX_BROWS;
  bcols  = MAX_BCOLS;
  crows  = arows;
  ccols  = bcols;

  if (myid == master) {
    /* read a, b */

    for (i = 0; i < MAX_AROWS; i++)
      for (j = 0; j < MAX_ACOLS; j++)
	a[i*MAX_ACOLS + j] = (float) (i*10 + j);

    for (i = 0; i < MAX_BROWS; i++)
      for (j = 0; j < MAX_BCOLS; j++)
	b[i*MAX_BCOLS + j] = (float) (i*10 + j);

    /* finished reading */
    numsent = 0;

    MPI_Bcast(b, brows*bcols - 1, MPI_FLOAT, master, MPI_COMM_WORLD);
    for (i = 0; i < numprocs-1; i++) {
      for (j = 0; j < acols; j++) {
        buffer[j] = a[i*acols+j];
      }
      MPI_Isend(buffer, acols, MPI_FLOAT, i+1, i+1, MPI_COMM_WORLD, &reqs[i]);
      numsent++;
    }

    MPI_Waitall(numsent, reqs, statuses); // Ignore the status codes for now

    numrecd = 0;

    for (i = 0; i < crows; i++) {
      MPI_Recv(ans, ccols, MPI_FLOAT, MPI_ANY_SOURCE, // Back to wildcards!!
         MPI_ANY_TAG, MPI_COMM_WORLD, &status);

      numrecd++;

      sender =  status.MPI_SOURCE;  // These can remain!
      anstype = status.MPI_TAG - 1; // These can remain!
      for (j = 0; j < ccols; j++) {
       c[anstype*ccols+j] = ans[j];
      }
      if (numsent < arows) {
        for (j = 0; j < acols; j++) {
            buffer[j] = a[numsent*acols+j];
        }

	if (numrecd > 1) MPI_Wait(&onereq, &status); // The first time around, nothing to wait on

	MPI_Isend(buffer, acols, MPI_FLOAT, sender, numsent+1, MPI_COMM_WORLD, &onereq);

	numsent++;
      }
      else { MPI_Send(buffer, 1, MPI_FLOAT, sender, 0, MPI_COMM_WORLD);
      }
    }

    if((numprocs-1) < arows) MPI_Wait(&onereq, &status);
      // This is the last wait needed for the last Isend above
      // This is needed ONLY if we had less processes than rows,
      // so that the above Isend actually took place...

    for (i = 0; i < MAX_CROWS; i++) {
      for (j = 0; j < MAX_CCOLS; j++) {
        printf("%f\t", c[i*MAX_CCOLS + j]);
        printf("\n");
    }}

  }
  else {
    MPI_Bcast(b, brows*bcols, MPI_FLOAT, master, MPI_COMM_WORLD);
    while (1) {
      MPI_Recv(buffer, acols, MPI_FLOAT, master, MPI_ANY_TAG,  MPI_COMM_WORLD, &status);
      if (status.MPI_TAG == 0) break;
      row = status.MPI_TAG - 1;
      for (i = 0; i < bcols; i++) {
        ans[i] = 0.0;
        for (j = 0; j < acols; j++) {
            ans[i] += buffer[j]*b[j*bcols+i];
        }
      }
      MPI_Send(ans, bcols, MPI_FLOAT, master, row+1, // row+1 Used as tag
             MPI_COMM_WORLD);
    }
  }
  MPI_Finalize();

  return 1;
}
