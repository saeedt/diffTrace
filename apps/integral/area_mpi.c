#include <stdlib.h>
#include <mpi.h>
#include "area_common.h"


int main(int argc, char **argv) {
    area_info_t params;
    int rank,size;
    params.arch=(char*)"mpi";
    MPI_Init( &argc, &argv );

    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    threadRank = rank;

    area_init(argc, argv, &params, rank == 0);

    area_begin(&params, rank == 0); 

    if(params.threads == 0 || size < params.threads)
        params.threads = size;

    // Local copies
    long long segments = params.segments;
    double start = params.start;

    long long rem = segments % params.threads;
    segments /= params.threads;
    start += rank * segments * params.delta;
    if( rank == params.threads - 1 )
    {
        segments += rem;
    }

    double area = 0;

    if( rank < params.threads )
    {
        long long i;
        for(i = 0; i < segments; ++i)
        {
            double x = start + ( i * params.delta );
            double y1 = params.op(x);
            double y2 = params.op(x + params.delta);
            area += (y1 + y2) * .5 * params.delta;
        }
    }
    double total_area;
    MPI_Reduce(&area, &total_area, 1, MPI_DOUBLE, MPI_SUM , 0, MPI_COMM_WORLD);
    //total_area = 5;
    double cputime = getCPUTime(); 
    MPI_Comm comm = MPI_COMM_WORLD;
    int gsize;
    if (rank == 0){
        MPI_Comm_size(comm, &gsize);
        cputimes = (double *)malloc(gsize*sizeof(double));
    }
	#ifdef GATHER_S_COUNT
	MPI_Gather( &cputime , 2, MPI_DOUBLE, cputimes, 1, MPI_DOUBLE, 0, comm );
	#elif GATHER_R_COUNT
	MPI_Gather( &cputime , 1, MPI_DOUBLE, cputimes, 2, MPI_DOUBLE, 0, comm );
	#elif GATHER_ROOT
	MPI_Gather( &cputime , 1, MPI_DOUBLE, cputimes, 1, MPI_DOUBLE, 1, comm );
	#else
	MPI_Gather( &cputime , 1, MPI_DOUBLE, cputimes, 1, MPI_DOUBLE, 0, comm );
	#endif
    
    area_end(&params, total_area, rank == 0);
    
    MPI_Finalize();

    area_finish(&params, rank == 0);

    return EXIT_SUCCESS;
}
