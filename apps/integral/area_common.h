#ifndef AREA_UNDER_CURVE_AREA_COMMON_H
#define AREA_UNDER_CURVE_AREA_COMMON_H

#include <sys/resource.h>
#include <stdbool.h>

//Global Variables
extern int threadRank;
extern double cputime,*cputimes;
extern struct rusage ru;

typedef struct
{
    int threads;
    unsigned long long segments;
    double start;
    double end;
    long double delta;
    const char* op_name;
    const char* op_eq;
    double (*op)(double);
    char* progname;
    size_t run;
    double start_time;
    char* arch;
} area_info_t;

bool lookup_op(const char* queyyry, area_info_t* params);
void usage_q(const char* progname, bool root);

void area_init(int argc, char* const argv[], area_info_t* params, bool root );
void area_finish(area_info_t* params, bool root);

void area_begin( area_info_t* params, bool root );
void area_end( const area_info_t* params, double area, bool root );

void usage(const char* progname);
double getCPUTime();

#define AREA_STRINGIFY(x) #x
#define AREA_TOSTRING(x) AREA_STRINGIFY(x)

#ifdef AREA_ARCH
const char* area_arch = AREA_TOSTRING(AREA_ARCH);
#else
extern const char* area_arch;
#endif

#ifdef AREA_CC
const char* area_cc = AREA_TOSTRING(AREA_CC);
#else
extern const char* area_cc;
#endif
#endif
