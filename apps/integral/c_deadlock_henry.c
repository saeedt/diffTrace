
/* deadlock.c */

/*
 * This program is an example of bad MPI.
 *
 * This program causes MPI_Send/MPI_Recv to deadlock (freeze up
 * with sender waiting for receiver and vice versa).
 */

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main (int argc, char** argv)
{ /* main */
    const int executable_command_line_argument        =      0;
    const int should_send_first_command_line_argument =      1;
    const int message_length_command_line_argument    =      2;
    const int number_of_command_line_arguments        =      3;
    const int maximum_buffer_length                   = 800000;
    const int minimum_message_length                  =      1;
    const int maximum_message_length                  = maximum_buffer_length;
    const int the_tag                                 =    999;
    const int program_failure_code                    =     -1;
    const int program_success_code                    =      0;
    const int false                                   =      0;
    const int true                                    =      1;

   /*
    * MPI_Recv requires the MPI_Status data structure, which includes
    * the following fields:
    *   mpi_status.MPI_SOURCE: which rank sent the message to this rank
    *   mpi_status.MPI_TAG: what the tag of the message was
    *   mpi_status.MPI_ERROR: the error code returned by MPI_Recv
    *
    * Calls to MPI_Recv and related MPI routines require that this
    * variable be used as an argument.
    *
    * However, for most such calls in most MPI programs in real life,
    * no one bothers to check the values of those fields, because the
    * values of MPI_SOURCE and MPI_TAG have to match what was passed
    * in to the MPI_Recv routine, and MPI_ERROR will matche the error
    * code returned by that call to the MPI routine.
    *
    * The exception would be if the requested source was MPI_ANY_SOURCE
    * and/or the requested tag was MPI_ANY_TAG.
    */
    MPI_Status mpi_status;

   /*
    * These arrays are "buffers" (arrays that temporarily contain a
    * particular piece of data).
    * Specifically, send_buffer and recv_buffer are used to hold
    * the message being sent (when this rank is sending) and the
    * message that was received (when this rank is receiving).
    */
    int send_buffer[maximum_buffer_length];
    int recv_buffer[maximum_buffer_length];

    int rank_to_communicate_with;
    int message_length;
    int should_send_first;
    int number_of_processes, my_rank;
    int mpi_error_code;

   /*
    * Start the MPI run.
    *
    * In ***EVERY*** MPI program, MPI_Init should be the
    * ***VERY FIRST EXECUTABLE STATEMENT***.
    */
    mpi_error_code = MPI_Init(&argc, &argv);

   /*
    * What is this process's rank? That is, who am I?
    */
    mpi_error_code = MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

   /*
    * How many processes are being used in this MPI run? That is,
    * how many of us are there?
    */
    mpi_error_code = MPI_Comm_size(MPI_COMM_WORLD, &number_of_processes);

   /*
    * This particular example program should use exactly 2 processes.
    */
    if (number_of_processes != 2) {

        fprintf(stderr, "ERROR: wrong number of processes %d.\n",
            number_of_processes);

       /*
        * Exit this MPI run cleanly, via MPI's self-killing routine.
        */
        mpi_error_code = MPI_Abort(MPI_COMM_WORLD, mpi_error_code);

    } /* if (number_of_processes != 2) */

   /*
    * Idiotproof: Are there enough command line arguments?
    */
    if (argc < number_of_command_line_arguments) {

        fprintf(stderr,
            "Usage: %s should_send_first[%d or %d] message_length\n",
            argv[executable_command_line_argument], false, true);

       /*
        * Exit this MPI run cleanly, via MPI's self-killing routine.
        */
        mpi_error_code = MPI_Abort(MPI_COMM_WORLD, mpi_error_code);

    } /* if (argc < number_of_command_line_arguments) */

   /*
    * Get the Boolean value for whether this rank should send first,
    * from the appropriate command line argument.
    */
    sscanf(argv[should_send_first_command_line_argument],
           "%d", &should_send_first);

   /*
    * Idiotproof the Boolean value for whether this rank should send first.
    */
    if ((should_send_first != false) &&
        (should_send_first != true)) {

        fprintf(stderr,
"ERROR: first command line argument (should send first %d)\n  should be either %d (false) or %d (true).\n",
            should_send_first, false, true);

       /*
        * Exit this MPI run cleanly, via MPI's self-killing routine.
        */
        mpi_error_code = MPI_Abort(MPI_COMM_WORLD, mpi_error_code);

    } /* if ((should_send_first != false) && ...) */

   /*
    * Get the length of the message being communicated, from the
    * appropriate command line argument.
    */
    sscanf(argv[message_length_command_line_argument], "%d", &message_length);

    if ((message_length < minimum_message_length) ||
        (message_length > maximum_message_length)) {

        fprintf(stderr,
            "ERROR: message length should be between %d and %d.\n",
            minimum_message_length, maximum_message_length);

       /*
        * Exit this MPI run cleanly, via MPI's self-killing routine.
        */
        mpi_error_code = MPI_Abort(MPI_COMM_WORLD, mpi_error_code);

    } /* if ((message_length < minimum_message_length) || ...) */

   /*
    * Determine which rank to communicate with.
    */
    rank_to_communicate_with = (my_rank + 1) % 2;

   /*
    * Communicate with the other rank.
    */
    if (should_send_first) {

        fprintf(stderr, "%d of %d: 1st, about to send %d ints\n",
            my_rank, number_of_processes, message_length);

       /*
        * Send this rank's message to the other rank.
        *
        * NOTE: Because this is a toy demonstration program, we
        *       don't care about the contents of the message.
        *       In a real program, we would care.
        */
        mpi_error_code =
            MPI_Send(send_buffer, message_length, MPI_INT,
                     rank_to_communicate_with, the_tag,
                     MPI_COMM_WORLD);

        fprintf(stderr, "%d of %d: 1st, done sending  %d ints\n",
            my_rank, number_of_processes, message_length);

        fprintf(stderr, "%d of %d: 2nd, about to receive %d ints\n",
            my_rank, number_of_processes, message_length);

       /*
        * Receive the other rank's message.
        */
        mpi_error_code =
            MPI_Recv(recv_buffer, maximum_buffer_length, MPI_INT,
                     rank_to_communicate_with, the_tag,
                     MPI_COMM_WORLD, &mpi_status);

        fprintf(stderr, "%d of %d: 2nd, done receiving   %d ints\n",
            my_rank, number_of_processes, message_length);

    } /* if (should_send_first) */
    else {

        fprintf(stderr, "%d of %d: 1st, about to receive %d ints\n",
            my_rank, number_of_processes, message_length);

       /*
        * Receive the other rank's message.
        *
        * NOTE: Because this is a toy demonstration program, we
        *       don't care about the contents of the message.
        *       In a real program, we would care.
        */
        mpi_error_code =
            MPI_Recv(recv_buffer, maximum_buffer_length, MPI_INT,
                     rank_to_communicate_with, the_tag,
                     MPI_COMM_WORLD, &mpi_status);

        fprintf(stderr, "%d of %d: 1st, done receiving   %d ints\n",
            my_rank, number_of_processes, message_length);

        fprintf(stderr, "%d of %d: 2nd, about to send %d ints\n",
            my_rank, number_of_processes, message_length);

       /*
        * Send this rank's message to the other rank.
        */
        mpi_error_code =
            MPI_Send(send_buffer, message_length, MPI_INT,
                     rank_to_communicate_with, the_tag,
                     MPI_COMM_WORLD);

        fprintf(stderr, "%d of %d: 2nd, done sending  %d ints\n",
            my_rank, number_of_processes, message_length);

    } /* if (should_send_first)...else */

   /*
    * End this MPI run.
    */
    mpi_error_code = MPI_Finalize();

    return program_success_code;

} /* main */
