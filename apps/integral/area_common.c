#ifndef AREA_UNDER_CURVE_AREA_COMMON_C
#define AREA_UNDER_CURVE_AREA_COMMON_C

#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <getopt.h>
#include <math.h>
#include <libgen.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <time.h>
#include <unistd.h>

#include "area_common.h"

int threadRank;
double cputime,*cputimes;
struct rusage ru;

void usage(const char* progname)
{
    usage_q(progname, false);
}

// Long Options
static const struct option long_options[] = {
        { "range", 1, NULL, 'r'},
        { "threads", 1, NULL, 't'},
        { "function", 1, NULL, 'f'},
        { "y", 1, NULL, 'f'},
        { "help", 0, NULL, 'h'},
        { 0, 0, 0, 0 }
};

void area_init(int argc, char* const argv[], area_info_t* params, bool root)
{
    // Defaults
    params->threads = 0;
    params->start = 0;
    params->end = 1;
    lookup_op("x", params);

    // Parse args
    int opt;
    char* comma;
    while(opt = getopt_long(argc, argv, "f:r:t:h", long_options, NULL), opt != -1 )
    {
        switch(opt)
        {
            case 'f':
                if (! lookup_op(optarg, params))
                    usage_q(argv[0], root);
                break;
            case 'r':
                comma = strchr(optarg, ',');
                if( comma == NULL )
                    usage_q(argv[0], root);
                params->start = atof(optarg);
                params->end = atof(comma + 1);
                break;
            case 't':
                params->threads = atoi(optarg);
                if(params->threads < 0 )
                    params->threads = 0;
                break;
            case 'h':
            case '?':
            default:
                usage_q(argv[0],root);
        }
    }

    char* path_temp = (char*)malloc(strlen(argv[0]) + 1);
    strcpy(path_temp,argv[0]);
    char* name_temp = (char*)basename(path_temp);
    params->progname = (char*)malloc(strlen(name_temp + 1));
    strcpy(params->progname, name_temp);
    free(path_temp);

    // Get segments off the end
    if( optind >= argc )
        usage_q(argv[0], root);

    params->segments = atoll(argv[optind]);

    // Sanity check
    if( params->segments < 1  || params->start >= params->end)
        usage_q(argv[0], root);
    params->delta = (params->end - params->start) / params->segments;
    if(root)
    {
        char hostname[1024];
        gethostname(hostname, 1024);
        
    	fprintf(stdout, "!~~~#**BEGIN RESULTS**#~~~!\n");
        fprintf(stdout, "%-20s: %s\n", "HOSTNAME", hostname);
        fprintf(stdout, "%-20s: %lld\n", "PROBLEM_SIZE", params->segments);
        fprintf(stdout, "%-20s: %.18g\n", "START", params->start);
        fprintf(stdout, "%-20s: %.18g\n", "END", params->end);
        fprintf(stdout, "%-20s: %.18Lg\n", "DELTA", params->delta);
        fprintf(stdout, "%-20s: %s\n", "FUNC", params->op_name);
        fprintf(stdout, "%-20s: %s\n", "PROGRAM", params->progname);
        fprintf(stdout, "%-20s: %s\n", "ARCH", params->arch);
       //fprintf(stdout, "%-20s: %s\n", "COMPILER", area_cc);
    }
    params->run = 0;
}


void area_finish(area_info_t* params, bool main)
{
    free(params->progname);
}

void area_begin( area_info_t* params, bool root )
{
    if(!root)
        return;
    struct timeval stime;
    gettimeofday(&stime,NULL);
    params->start_time = stime.tv_usec;
    params->start_time /= 1000000;
    params->start_time += stime.tv_sec;

    params->run += 1;
}

void area_end( const area_info_t* params, double area, bool root)
{
    if(!root)
        return;
   int i = 0;
   double cputime = 0; 
    for(i; i < params->threads; ++i){ 
    cputime += cputimes[i];
    }


    struct timeval stime;
    double end_time;
    gettimeofday(&stime,NULL);
    end_time = stime.tv_usec;
    end_time /= 1000000;
    end_time += stime.tv_sec;
    fprintf(stdout, "%-20s: %.8g\n", "CPUTIME",cputime);
    fprintf(stdout, "%-20s: %d\n", "THREADS", params->threads);
    fprintf(stdout, "%-20s: %.4g\n", "TIME", end_time - params->start_time);
    fprintf(stdout, "%-20s: %.4g\n", "AREA", area);
    fprintf(stdout, "!~~~#**END RESULTS**#~~~!\n");
}

struct operation_row
{
    const char* name;
    const char* equation;
    double (*func)(double);
};

double linear(double x)
{
    return x;
}

double squared(double x)
{
    return x * x;
}

double one_over(double x)
{
    return 1 / x;
}

double sinx(double x)
{
    return sin(x);
}

double gauss(double x)
{
    const double A1 = 8000,               a1 = 2;
    const double A3 =  100,  x03 =   1,   a3 =  2;
    const double A4 =   80,  x04 =   1,   a4 =  2;
    const double A5 =   60,  x05 = .01,   a5 =  2;
    const double A6 =   40,  x06 = .01,   a6 =  2;
    const double A7 =   20,  x07 = .01,   a7 =  2;

     return A1 * sin( x / a1 )
         +  A3 * exp( -pow( x - x03, 2.0 / a3 ) )
         +  A4 * exp( -pow( x - x04, 2.0 / a4 ) )
         +  A5 * exp( -pow( x - x05, 2.0 / a5 ) )
         +  A6 * exp( -pow( x - x06, 2.0 / a6 ) )
         +  A7 * exp( -pow( x - x07, 2.0 / a7 ) );
}

static const struct operation_row ops_table[] = {
    { "x", "x", linear },
    { "x^2", "x / 2", squared },
    { "1/x", "1 / x", one_over },
    { "sin", "sin x", sinx },
    { "gauss", "a 2d gaussian computation", gauss }
};

static const size_t ops_table_size = sizeof(ops_table) / sizeof(struct operation_row);

bool lookup_op(const char* query, area_info_t* params)
{
    size_t i;
    for(i = 0; i < ops_table_size && strcmp(ops_table[i].name, query) != 0 ; ++i);
    if ( i < ops_table_size ) 
    {
        params->op_name = ops_table[i].name;
        params->op_eq = ops_table[i].equation;
        params->op = ops_table[i].func;
    }
    return i < ops_table_size;
}

void usage_q(const char* progname, bool root)
{
    if( root )
    {
        fprintf(stderr, "Usage: %s [--function FUNCTION] [ --threads NUM_THREADS ] [ --range X1,X2 ] SEGMENTS\n", progname);
        fprintf(stderr, "Usage: %s --help\n\n", progname);
        fprintf(stderr, "AVAILABLE FUNCTIONS:\n");
        size_t i;
        for( i = 0; i < ops_table_size; ++i)
        {
            fprintf(stderr, "\t\"%s\"\t- y = %s\n", ops_table[i].name, ops_table[i].equation);
        }
    }
    exit(EXIT_FAILURE);
}

double getCPUTime()
{
    getrusage(RUSAGE_SELF,&ru);
    struct timeval utim = ru.ru_utime;
    struct timeval stim = ru.ru_stime;
    double cputime = (double) utim.tv_sec + (double) utim.tv_usec / 1000000.0;
    cputime += (double) stim.tv_sec + (double) stim.tv_usec / 1000000.0;
    return cputime;
}

#endif

