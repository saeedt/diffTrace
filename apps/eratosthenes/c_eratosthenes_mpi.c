#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <mpi.h>

#define NOT_PRIME 0
#define PRIME 1
#define SET_PRIME 2 

int find_minimum_candidate_prime(unsigned char values[], int value_count, int low_value);
void remove_multiples_gmp(unsigned char values[], int value_count, int low_value, 
                          int global_minimum_prime); 
void display_primes(unsigned char values[], int value_count, int low_value, int rank); 

int main(int argc, char *argv[]) {
	int rank, size;
	MPI_Errhandler errhandler;
	double start_time, end_time;
	const int N = 200000000;
	int P; 
	const int DEBUG = 0; 
	
	unsigned char *values = NULL; 
	int value_count, low_value, high_value, x, repeat = 1, start; 
	int minimum_candidate_prime, global_minimum_prime, gmp_squared; 
	int prime_count = 0, global_prime_count = 0; 

	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	
	//MPI_Barrier(MPI_COMM_WORLD); 
	start_time = MPI_Wtime(); 
	
	P = size;
	value_count = N / P; 
	
	values = (unsigned char *)malloc(sizeof(unsigned char) * value_count); 
	if (values == NULL) {
		printf("malloc() failed\n");
		exit(1); 
	}
	
	low_value = N / P * rank; 
	high_value = (N / P * (rank + 1)) - 1; 
	
	for (x = 0; x < value_count; x++)
		values[x] = PRIME; 

	if (rank == 0) { // xxx kludge 0 and 1 on rank 0 are marked out since we start at 2
		values[0] = NOT_PRIME;  
		values[1] = NOT_PRIME; 
	}
	
	while (repeat) {
		minimum_candidate_prime = find_minimum_candidate_prime(values, value_count, low_value); 
		MPI_Allreduce(&minimum_candidate_prime, &global_minimum_prime, 1, MPI_INT, MPI_MIN, 
		              MPI_COMM_WORLD); 		
		gmp_squared = global_minimum_prime * global_minimum_prime; 

		// Remove the multiples, start point is determined by which range K^2 falls in.
		// 
		if (gmp_squared < low_value) {
			x = 0;
			start = -1; 

			while ((start == -1) && (x < value_count)) {
				if (((x + low_value) % global_minimum_prime) == 0)
					start = x;  
				x++; 
			}
			remove_multiples_gmp(values, value_count, start, global_minimum_prime); 
		}
			
		if ((gmp_squared >= low_value) && (gmp_squared <= high_value)) {
			start = gmp_squared - low_value; 
			remove_multiples_gmp(values, value_count, start, global_minimum_prime); 
		}
			
		//	if ((global_minimum_prime * global_minimum_prime) > high_value)
		//		do nothing;
		
		// Mark the prime, who does it is determined by which range K falls in.
		//
		if ((global_minimum_prime >= low_value) && (global_minimum_prime <= high_value))
			values[global_minimum_prime - low_value] = SET_PRIME; 
		
		if (gmp_squared > N)
			repeat = 0; 
	}

	MPI_Barrier(MPI_COMM_WORLD); 

	if (DEBUG) {
		display_primes(values, value_count, low_value, rank); 
		fflush(stdout); 
	}
	
	for (x = 0; x < value_count; x++)
		if (values[x] == SET_PRIME || values[x] == PRIME)
			prime_count++; 
	
	/*
	#ifdef REDUCE_COUNT
	MPI_Reduce(&prime_count, &global_prime_count, 10, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	#endif
	#ifndef REDUCE_COUNT
	MPI_Reduce(&prime_count, &global_prime_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	#endif
	
	#ifdef REDUCE_ROOT
	MPI_Reduce(&prime_count, &global_prime_count, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD); 
	#endif
	#ifndef REDUCE_ROOT
	MPI_Reduce(&prime_count, &global_prime_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD); 
	#endif
	*/
	#ifdef REDUCE_ROOT
        MPI_Reduce(&prime_count, &global_prime_count, 1, MPI_INT, MPI_SUM, 1, MPI_COMM_WORLD);
        #elif REDUCE_COUNT
        MPI_Reduce(&prime_count, &global_prime_count, 10, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
        #else
	MPI_Reduce(&prime_count, &global_prime_count, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
	#endif
	
	
	end_time = MPI_Wtime(); 
	MPI_Finalize(); 
		
	if (rank == 0) {
		printf("For N = %d and P = %d the global_prime_count = %d in %f seconds\n", 
		        N, P, global_prime_count, end_time - start_time);  	
		fflush(stdout); 
	}
		
	exit(0); 
}

int find_minimum_candidate_prime(unsigned char values[], int value_count, int low_value) {
	int x = 0;
	int minimum_candidate_prime = 0; 
	
	while ((values[x] == NOT_PRIME || values[x] == SET_PRIME) && (x < value_count)) 
		x++;
		
	if (x < value_count)
		minimum_candidate_prime = x + low_value; 
	else
		minimum_candidate_prime = INT_MAX; 

	return(minimum_candidate_prime); 
}

void remove_multiples_gmp(unsigned char values[], int value_count, int start, 
                          int global_minimum_prime) { 
	int x; 
	
	for (x = start; x < value_count; x = x + global_minimum_prime)
		values[x] = 0; 

	return;
}

void display_primes(unsigned char values[], int value_count, int low_value, int rank) {
	int x = 0;
	
	for (x = 0; x < value_count; x++)
		if (values[x] == SET_PRIME || values[x] == PRIME) {
			printf("rank = %d, x = %d + %d = %d\n", rank, low_value, x, x + low_value); 
			fflush(stdout); 
		}
		
	return; 
}
