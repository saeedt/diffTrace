
#include <stdio.h>
#include <math.h>

int main(int argc, const char * argv[])
{
    /* Define temporary variables */
    double result = 0;
    int i;

    /* Calculate the square root of value */
    for(i = 1;  i <= 50  ; ++i){
    	result += sqrt(i);
    }
    /* Display the result of the calculation */
    printf("The Square Root sum is %f\n", result);

    return 0;
}
