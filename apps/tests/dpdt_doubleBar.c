#include <mpi.h>
#include <stdio.h>
#include <math.h>

int main(int argc, char** argv) {
  // Initialize the MPI environment
  MPI_Init(NULL, NULL);

  // Get the number of processes
  int world_size;
  MPI_Comm_size(MPI_COMM_WORLD, &world_size);

  // Get the rank of the process
  int world_rank;
  MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);

  // Get the name of the processor
  char processor_name[MPI_MAX_PROCESSOR_NAME];
  int name_len;
  MPI_Get_processor_name(processor_name, &name_len);

  // Print off a hello world message
  printf("Hello world from processor %s, rank %d"
	 " out of %d processors\n",
	 processor_name, world_rank, world_size);


  // EVEN RANKS do A
  if (world_rank % 2 == 0){
    printf("BB_EVEN_RANKS: POW. rank %d\n", world_rank);
    int A[1024];
    A[0]=1024;
    for (int i = 1 ; i <1024 ; i++){
      A[i] = pow(i,(A[i-1]%6)); 
    }
  }
  // ODD RANKS do B
  /* else{
    printf("BB_ODD_RANKS: SQRT. rank %d\n", world_rank);
    double A[1024];
    A[0]=1024.0;
    for(int i = 1 ; i <1024 ; i++){
      A[i] = sqrt(i*(A[i-1]*0.01));
    }
    }*/
  // Then a Barrier
  MPI_Barrier(MPI_COMM_WORLD);
  
  // ODD RANKS do B
  if (world_rank % 2 == 1){
    printf("AB_ODD_RANKS: SQRT. rank %d\n", world_rank);
    double A[1024];
    A[0]=1024.0;
    for(int i = 1 ; i <1024 ; i++){
      A[i] = sqrt(i*(A[i-1]*0.6));
    }
  }
  // ODD RANKS do B
  /*else{
    printf("AB_ODD_RANKS: POW. rank %d\n", world_rank);
    int A[1024];
    A[0]=1024;
    for(int i = 1 ; i <1024 ; i++){
      A[i] = pow(i,(A[i-1]%6));
    }
    }*/
  // Finalize the MPI environment.
  MPI_Finalize();
}
